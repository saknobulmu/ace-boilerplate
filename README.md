# Atlassian Add-on using Express + some sugar
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![Build Status](https://travis-ci.org/mtmendonca/ace-boilerplate.svg?branch=master)](https://travis-ci.org/mtmendonca/ace-boilerplate)

## Tech
- [Docker] - Run the stack in containers!
- [docker-compose] - Basic Docker orchestrator
- [Postgresql] - Database
- [Redis] - Cache
- [ExpressJS] - Node.js web framework
- [NestJS] - Node.js server-side framework
- [ACE] - Atlassian Express library
- [ApolloServer] - Graphql server library
- [Ngrok] - http tunneling service
- [TypeScript] - Typescript

## Quick setup
### Dependencies
- Nodejs v12.x: Use [nvm](https://github.com/creationix/nvm) if you can, it's handy for switching between node versions
- A Jira Cloud development instance: Click [here](https://developer.atlassian.com/blog/2016/04/cloud-ecosystem-dev-env/) to get one.
Assuming you used nvm, activate v12 and install Yarn globally:
```sh
$ nvm install 12
$ nvm use
$ npm install -g yarn
```
Create a copy of `.env.sample` as `.env`  
Create a copy of `api/credentials.json.sample` as `credentials.json`  
Edit credentials.json. To get an API Token follow [these instructions](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)

### Start the docker-compose stack
```sh
$ ./start
```
The script above will 
* Install node dependencies on the `api` and `app` directories
* Deploy Postgresql and Redis containers
* Start the server and web applications on containers
* Start an nginx reverse proxy that redirects `/app` requests to the `web` container, and everything else to the `api` container


Use ACE's `AC_OPTS` environment variable on `.env` to control ACE's behaviour.
With the current value of `AC_OPTS=force-reg,force-dereg` an Ngrok tunnel will be created and used as your application's enpoint during development.
Use `AC_OPTS=no-auth` to start the stack locally, skipping authentication.

Use `NGROK_AUTHTOKEN` environment variable on `.env` to specify an Ngrok auth token if you have one.

## Production deployment
When deploying to production make sure you replace `atlassian-connect.json` with a version of the file that matches your production environment.
### Docker
Build the production-ready container using Dockerfile.production

## Architecture
This boilerplate creates a dependency injection container named `ioc` and adds it to express' `req`, used in routes and graphql resolvers. This container has methods that return the bootstrapped dependencies placed under:
* `api/db/migrations` migrations using Knex.js
* `api/src/main.ts` bootstrap the application with NestJS and ExpressJS
* `api/src/main.module.ts` module that ties all other modules together
* `api/src/common` common components that can be shared across all modules
* `api/src/config` configuration components
* `api/src/middlewares` middlewares that can be shared across all modules
* `api/src/modules` all NestJS modules, see [NestJS modules documentation](https://docs.nestjs.com/modules) for more information
* `api/src/utils` module that ties all other modules together

ACE's middleware also includes a `context` object in `req` which contains [jira's context data](https://bitbucket.org/atlassian/atlassian-connect-express). In essence, the following variables are added to express' `req` object:
```js
{
  context: ACEContext,
  ace: ?Object,
}
```

`api/entrypoint.sh` runs a database migration prior to launching the stack.
DB Migrations are created using [Knex.js](http://knexjs.org/).

## Jira REST API Connector
The `api/src/modules/jira/connector/restapi.connector.ts` gives you `get`, `post`, `put` and `delete` methods that proxy `ace`'s httpClient methods.

## Yarn scripts
### /api
|command|purpose|
|---|---|
|start:dev|Starts the stack locally according to the `AC_OPTS` config in `.env` |
|start:debug| same as `dev` but with node's `--inspection` enabled and listening on port 9229|
|start:prod|Starts production build |
|migrate|Runs the latest migrations|
|create-migration `<migration-name>`|Creates an empty database migration on `/db/migrations`|
|build|Creates production build in `api/dist/`|

### /app
Default create-react-app scripts

[docker]: https://www.docker.com
[docker-compose]: https://docs.docker.com/compose/install/
[metabase]: https://www.metabase.com
[postgresql]: https://www.postgresql.org/
[redis]: https://redis.io/
[expressjs]: https://expressjs.com
[nestjs]: https://nestjs.com
[ace]: https://bitbucket.org/atlassian/atlassian-connect-express
[inversifyjs]: https://github.com/inversify/InversifyJS
[apolloserver]: https://www.apollographql.com/docs/apollo-server/
[ngrok]: https://ngrok.com/
[typescript]: https://www.typescriptlang.org/
