#!/bin/sh
set -e

if [ ! -z "$NGROK_AUTHTOKEN" ]
then
    ngrok authtoken $NGROK_AUTHTOKEN
fi

echo "inspect_db_size: -1" >> /root/.ngrok2/ngrok.yml
echo "log: false" >> /root/.ngrok2/ngrok.yml

if [ ! -z "$NGROK_REGION" ]
then
    echo "region: $NGROK_REGION" >> /root/.ngrok2/ngrok.yml
fi

yarn start:prod
