exports.up = knex =>
  knex.schema.createTable('todos', table => {
    table.timestamp('created_at', { useTz: true });
    table.timestamp('updated_at', { useTz: true });
    table.uuid('id').primary();
    table.text('title').notNullable();
    table.boolean('done').notNullable().defaultTo(false);
  });

exports.down = knex => knex.schema.dropTableIfExists('todos');
