#!/bin/sh
set -e

if [ ! -z "$NGROK_AUTHTOKEN" ]
then
    ngrok authtoken $NGROK_AUTHTOKEN
fi

echo "inspect_db_size: -1" >> /root/.ngrok2/ngrok.yml
echo "log: false" >> /root/.ngrok2/ngrok.yml

if [ ! -z "$NGROK_REGION" ]
then
    echo "region: $NGROK_REGION" >> /root/.ngrok2/ngrok.yml
fi

yarn migrate

if [ $DEBUG == "true" ]
then
    echo "Starting in DEBUG mode! 🐞"
    yarn start:debug
else
    yarn start:dev
fi
