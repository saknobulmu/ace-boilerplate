import { Module, DynamicModule } from '@nestjs/common';
import { GraphQLModule as NestGraphQLModule } from '@nestjs/graphql';
import { SequelizeModule } from '@nestjs/sequelize';
import { AddOn } from 'atlassian-connect-express';
import { JiraModule } from './modules/jira/jira.module';
import { CoreModule } from './modules/core/core.module';
import { GraphQLModule } from './modules/graphql/graphql.module';
import { graphQLConfig } from './config/graphql.config';
import { dbConfig } from './config/db.config';

@Module({})
export class MainModule {
    static register(aceAddon: AddOn): DynamicModule {
        return {
            module: MainModule,
            imports: [
                JiraModule.register(aceAddon),
                CoreModule,
                GraphQLModule,
                NestGraphQLModule.forRootAsync(graphQLConfig()),
                SequelizeModule.forRoot(dbConfig()),
            ],
        };
    }
}
