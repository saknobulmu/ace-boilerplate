import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { decode } from 'jsonwebtoken';
import { isEmpty } from 'lodash';
import { ACERequest } from '../modules/jira/jira.types';

@Injectable()
export class ValidateUserMiddleware implements NestMiddleware {
    use(req: ACERequest, res: Response, next: NextFunction) {
        if (!req?.context?.userAccountId || this.isAnonymousOrJSDUser(req)) {
            console.log(
                `Anonymous or JSD User not authorised, HostBaseURL: ${req?.context?.hostBaseUrl}, Token: ${req?.context?.token}`,
            );
            return res.status(401);
        }
        return next();
    }

    isAnonymousOrJSDUser = (req: ACERequest) => {
        const tokenDecoded = decode(req.context.token);
        const sub: any = tokenDecoded && tokenDecoded.sub;
        const jiraServiceDeskUserPrefix = 'qm:';
        return isEmpty(sub) || sub.startsWith(jiraServiceDeskUserPrefix);
    };
}
