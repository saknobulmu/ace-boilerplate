import rateLimit from 'express-rate-limit';
import parseDomain from 'parse-domain';

export const webhookRequestLimiter = rateLimit({
    windowMs: 60 * 1000,
    max: 40,
    keyGenerator: (req) => {
        let subdomain: string;
        if (req.body.version) {
            subdomain = parseDomain(req.body.version.self).subdomain;
        } else if (req.body.project) {
            subdomain = parseDomain(req.body.project.self).subdomain;
        }
        let key: string;
        if (!subdomain) {
            key = `${req.headers['x-forwarded-for']}|${req.body.webhookEvent}`;
        } else {
            key = `${subdomain}|${req.body.webhookEvent}`;
        }
        return key;
    },
    handler: (req, res) => {
        console.log(`🟠 User from ${req.headers['x-forwarded-for']} reached request limit in Webhook`);
        console.log('BLOCKED REQ ', JSON.stringify({ method: req.method, query: req.baseUrl, body: req.body }));
        res.status(429).send('too many requests');
    },
});
export const graphqlRequestLimiter = rateLimit({
    windowMs: 60 * 1000,
    max: 500,
    keyGenerator: (req) => req.headers['x-forwarded-for'] as string,
    handler: (req, res) => {
        console.log(`🟠 User from ${req.headers['x-forwarded-for']} reached request limit in GraphQL`);
        console.log('BLOCKED REQ ', JSON.stringify({ method: req.method, query: req.baseUrl, body: req.body }));
        res.status(429).send('too many requests');
    },
});
export const generalRequestLimiter = rateLimit({
    windowMs: 60 * 1000,
    max: 500,
    keyGenerator: (req) => req.headers['x-forwarded-for'] as string,
    handler: (req, res) => {
        console.log(`🟠 User from ${req.headers['x-forwarded-for']} reached request limit in General`);
        console.log('BLOCKED REQ ', JSON.stringify({ method: req.method, query: req.baseUrl, body: req.body }));
        res.status(429).send('too many requests');
    },
});
