import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { ACERequest } from '../modules/jira/jira.types';
import { getUrlSubdomainFromContext } from '../utils/url';

@Injectable()
export class InstanceInfoMiddleware implements NestMiddleware {
    async use(req: ACERequest, _res: Response, next: NextFunction) {
        const { clientKey } = req.context;
        const instanceName = getUrlSubdomainFromContext(req);

        req.instanceName = instanceName;
        req.clientKey = clientKey;
        return next();
    }
}
