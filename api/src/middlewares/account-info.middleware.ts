import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { ACERequest } from '../modules/jira/jira.types';

@Injectable()
export class AccountInfoMiddleware implements NestMiddleware {
    use(req: ACERequest, _res: Response, next: NextFunction) {
        req.userAccountId = req.context.userAccountId;
        return next();
    }
}
