import { Inject, Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { AddOn } from 'atlassian-connect-express';
import { ACE_ADDON } from '../modules/jira/jira.constants';

@Injectable()
export class AuthenticateMiddleware implements NestMiddleware {
    private readonly logger = new Logger(AuthenticateMiddleware.name);

    constructor(@Inject(ACE_ADDON) private aceAddon: AddOn) {}

    use(req: Request, res: Response, next: NextFunction) {
        this.logger.debug('🟡 Authenticating...');
        this.aceAddon.authenticate()(req, res, next);
    }
}
