import bodyParser from 'body-parser';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import errorHandler from 'errorhandler';
import morgan from 'morgan';
import helmet from 'helmet';
import nocache from 'nocache';
import cors from 'cors';
import express, { Request } from 'express';
import { AddOn } from 'atlassian-connect-express';
import { xssSanitizer } from '../utils/xss.sanitizer';

export const applyExpressMiddlewares = (appExpress: express.Application, aceAddon: AddOn) => {
    const devEnv = appExpress.get('env') === 'development';

    if (devEnv) {
        appExpress.use(morgan('dev'));
    } else {
        morgan.token('body', ({ body }: Request) => JSON.stringify(body || {}));
        appExpress.use(
            morgan(
                ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":body"',
            ),
        );
    }

    appExpress.use(
        cors({
            origin: '*',
            methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
            preflightContinue: false,
            optionsSuccessStatus: 204,
        }),
    );
    appExpress.use((req, res, next) => {
        req.body = xssSanitizer(req.body);
        next();
    });

    // Atlassian security policy requirements
    // http://go.atlassian.com/security-requirements-for-cloud-apps
    // HSTS must be enabled with a minimum age of at least one year
    appExpress.use(
        helmet({
            contentSecurityPolicy: false,
            expectCt: false,
            dnsPrefetchControl: false,
            frameguard: {
                action: 'ALLOW-FROM',
                domain: 'atlassian.net',
            },
            hidePoweredBy: {
                setTo: 'Green Ether',
            },
            hpkp: false,
            hsts: {
                maxAge: 31536000,
                includeSubDomains: true,
                preload: true,
            },
            ieNoOpen: true,
            noSniff: true,
            xssFilter: true,
            referrerPolicy: {
                policy: 'no-referrer',
            },
        }),
    );

    // Include request parsers
    appExpress.use(bodyParser.json({ limit: '1mb' }));
    appExpress.use(bodyParser.urlencoded({ extended: false, limit: '1mb' }));
    appExpress.use(cookieParser());

    // Gzip responses when appropriate
    appExpress.use(compression());

    // Include atlassian-connect-express middleware
    appExpress.use(aceAddon.middleware());

    // Atlassian security policy requirements
    // http://go.atlassian.com/security-requirements-for-cloud-apps
    appExpress.use(nocache());

    // Show nicer errors in dev mode
    if (devEnv) appExpress.use(errorHandler());
};
