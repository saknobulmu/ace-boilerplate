import { Inject, Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { AddOn } from 'atlassian-connect-express';
import { ACE_ADDON } from '../modules/jira/jira.constants';

@Injectable()
export class CheckValidTokenMiddleware implements NestMiddleware {
    private readonly logger = new Logger(CheckValidTokenMiddleware.name);

    constructor(@Inject(ACE_ADDON) private aceAddon: AddOn) {}

    use(req: Request, res: Response, next: NextFunction) {
        this.logger.debug('🟡 Checking valid token...');
        const middlewareNextFunction = this.aceAddon.checkValidToken();
        if (typeof middlewareNextFunction === 'function') {
            middlewareNextFunction(req, res, next);
        }
    }
}
