import { ACERequest } from '../modules/jira/jira.types';

const PROPS_TO_IGNORE_STRINGIFY = ['socket', 'incoming', 'connection', 'client', 'res'];

// this function ignores some properties to avoid circular reference when stringifying a JSON object
const filterStringifyProperties = (key: string, value: any) => {
    if (PROPS_TO_IGNORE_STRINGIFY.includes(key)) {
        return key;
    }
    return value;
};

export const getUrlSubdomain = (url: string) => {
    const parsedHost = new URL(url).host;
    const splittedHost = parsedHost.split('.');
    return splittedHost && splittedHost.length > 0 ? splittedHost[0] : '';
};

export const getUrlSubdomainFromContext = (req: ACERequest) => {
    if (!req.context) {
        console.error('Request with corrupted context');
        return null;
    }

    return getUrlSubdomain(req.context.hostBaseUrl);
};

export const getUrlSubdomainFromBody = (req: ACERequest) => {
    if (!req.body || !req.body.baseUrl || !Object.keys(req.body).length) {
        console.error(`Request with corrupted body: ${JSON.stringify(req, filterStringifyProperties)}`);
        return null;
    }

    return getUrlSubdomain(req.body.baseUrl);
};
