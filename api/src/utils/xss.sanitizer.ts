import xss from 'xss';

export const xssSanitizer = (value: any) => {
    if (typeof value === 'string') {
        return xss(value);
    }
    if (value && Array.isArray(value)) {
        return value.map((el) => xssSanitizer(el));
    }
    if (value && typeof value === 'object') {
        const newObject = {};
        Object.keys(value).forEach((key) => {
            newObject[key] = xssSanitizer(value[key]);
        });
        return newObject;
    }
    return value;
};
