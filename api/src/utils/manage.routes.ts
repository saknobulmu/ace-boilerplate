import express from 'express';
import { AddOn } from 'atlassian-connect-express';
import { ACEDescriptor } from '../modules/jira/jira.types';

const removeMiddlewares = (route: any, index: number, routes: any, webhooksUrls: Array<string>): number => {
    if (route.route && route.route.path) {
        if (webhooksUrls.includes(route.route.path)) {
            return index;
        }
    }
    return -1;
};

export const removeDefaultWebhooksRoutes = (appExpress: express.Application, aceAddon: AddOn) => {
    let webhooksUrls = [];
    const aceDescriptor = aceAddon.descriptor as unknown as ACEDescriptor;
    const addonWebhooks = aceDescriptor.modules.webhooks;
    if (addonWebhooks) {
        webhooksUrls = addonWebhooks.map((webhook) => webhook.url);
        const routes = appExpress._router.stack;
        const spliceIndexes: number[] = [];
        routes.forEach((route: any, index: number, routes: any) => {
            const spliceIndex = removeMiddlewares(route, index, routes, webhooksUrls);
            if (spliceIndex > -1) {
                spliceIndexes.push(spliceIndex);
            }
        });

        for (let i = spliceIndexes.length - 1; i >= 0; i--) {
            routes.splice(spliceIndexes[i], 1);
        }
    }
};
