import { Injectable } from '@nestjs/common';
import { BaseInput, ReqOrOpts } from '../jira.types';
import { User } from '../jira.rest.types';
import { RestApiConnector } from '../connector/restapi.connector';

interface GetCurrentUserInput extends BaseInput {
    queryParameters?: {
        expand?: 'groups' | 'applicationRoles';
    };
}

@Injectable()
export class JiraUserService {
    constructor(private readonly restApiConnector: RestApiConnector) {}

    getCurrentUser(reqOrOpts: ReqOrOpts, input?: GetCurrentUserInput) {
        return this.restApiConnector.asUserHttpClient(reqOrOpts).get<User>('/rest/api/3/myself', input);
    }
}
