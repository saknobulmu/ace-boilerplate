import { Injectable } from '@nestjs/common';
import { BaseInput, ReqOrOpts } from '../jira.types';
import { User } from '../jira.rest.types';
import { RestApiConnector } from '../connector/restapi.connector';

@Injectable()
export class JiraIssueService {
    constructor(private readonly restApiConnector: RestApiConnector) {}

    updateIssueSummary(reqOrOpts: ReqOrOpts, issueIdOrKey: string, summary: string) {
        const input: BaseInput = {
            pathParameters: { issueIdOrKey },
            body: {
                fields: {
                    summary,
                },
            },
        };
        return this.restApiConnector.httpClient(reqOrOpts).put<User>('/rest/api/3/issue/{issueIdOrKey}', input);
    }
}
