import { Injectable, Inject } from '@nestjs/common';
import 'reflect-metadata';
import { AddOn, HostClient } from 'atlassian-connect-express';
import { ACE_ADDON } from '../jira.constants';
import { ReqOrOpts, HttpClient } from '../jira.types';
import { requestFn, unpaginatedRequestFn, isReq, Methods } from './restapi.request';

@Injectable()
export class RestApiConnector {
    constructor(@Inject(ACE_ADDON) private readonly aceAddon: AddOn) {}

    httpClient(reqOrOpts: ReqOrOpts): HttpClient {
        return this.createAddonHttpClient(reqOrOpts);
    }

    asUserHttpClient(reqOrOpts: ReqOrOpts): HttpClient {
        if (isReq(reqOrOpts)) {
            return this.createAddonHttpClient(reqOrOpts, reqOrOpts.context.userAccountId);
        }
        return this.createAddonHttpClient(reqOrOpts, reqOrOpts.userAccountId);
    }

    private createAddonHttpClient(reqOrOpts: ReqOrOpts, userAccountId?: string): HttpClient {
        let http = this.buildHttpClient(reqOrOpts);
        if (userAccountId) {
            http = http.asUserByAccountId(userAccountId);
        }

        return {
            getUnpaginated: unpaginatedRequestFn(http, Methods.get),
            get: requestFn(http, Methods.get),
            post: requestFn(http, Methods.post),
            put: requestFn(http, Methods.put),
            del: requestFn(http, Methods.delete),
        };
    }

    private buildHttpClient(reqOrOpts: ReqOrOpts): HostClient {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return this.aceAddon.httpClient(reqOrOpts);
    }
}
