import { stringify as qsStringify } from 'querystring';
import 'reflect-metadata';
import matchAll from 'string.prototype.matchall';
import { isEmpty, chunk } from 'lodash';
import { IncomingMessage } from 'http';
import { Request } from 'request';
import { HostClient } from 'atlassian-connect-express';
import { v4 as uuidv4 } from 'uuid';
import { ReqOrOpts, ACERequest, BaseInput, ResultIdentifier, JiraQueryType } from '../jira.types';

export enum Methods {
    get = 'get',
    post = 'post',
    put = 'put',
    delete = 'delete',
}

interface PromiseResponse extends IncomingMessage {
    request?: Request;
}

const PATH_PARAMETERS = 'pathParameters';
const QUERY_PARAMETERS = 'queryParameters';
const BODY = 'body';

const BATCH_SIZE = 30;

const makeRequest = <T>(httpHostClient: HostClient, method: Methods, builtPath: string, input?: BaseInput) => {
    return new Promise<T>((resolve, reject) => {
        httpHostClient[method](
            {
                json: true,
                url: builtPath,
                body: input ? input[BODY] : undefined,
            },
            (error: Error, res: PromiseResponse, body: unknown) => {
                if (error) {
                    return reject(error);
                }
                const { statusCode, statusMessage, headers, request } = res;
                if (statusCode >= 400) {
                    const host = request?.host;
                    console.error(
                        `StatusCode: ${statusCode}, StatusMessage: ${statusMessage}, Host: ${host}, Path: ${builtPath}`,
                    );
                    reject(new Error(JSON.stringify({ statusCode, statusMessage, body })));
                }
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore - needed because we need to do another request to the new location
                if (statusCode === 302 || statusCode === 303) resolve(headers.location);

                if (typeof body === 'string') {
                    return resolve(JSON.parse(body) as T);
                }

                return resolve(body as T);
            },
        );
    });
};

const addPathParameters = (path: string, pathParameters: Record<string, string>) => {
    let pathWithParameters = path;
    const matches = [...matchAll(path, /{([\w]+)}/gi)];

    matches.forEach(([match, value]) => {
        pathWithParameters = pathWithParameters.replace(match, pathParameters[value]);
    });

    return pathWithParameters;
};

const buildPath = (path: string, input?: BaseInput) => {
    const pathWithParameters =
        input && PATH_PARAMETERS in input ? addPathParameters(path, input[PATH_PARAMETERS]) : path;

    const pathWithQuerystring =
        input && QUERY_PARAMETERS in input
            ? `${pathWithParameters}?${qsStringify(input[QUERY_PARAMETERS])}`
            : pathWithParameters;

    return pathWithQuerystring;
};

const getEachPaginationRecords = async <T>(
    builtPath: string,
    resultIdentifier: ResultIdentifier,
    httpHostClient: HostClient,
    method: Methods,
    startAt: number,
    maxResults: number,
    input?: BaseInput,
): Promise<{ records: Array<T>; total: number }> => {
    const paginatedPath = `${builtPath}${
        builtPath.includes('?') ? '&' : '?'
    }startAt=${startAt}&maxResults=${maxResults}`;
    const body = await makeRequest<any>(httpHostClient, method, paginatedPath, input);
    const { total, [resultIdentifier]: records } = body;
    return { records, total };
};

const paginationThrottle = async <T>(
    builtPath: string,
    resultIdentifier: ResultIdentifier,
    httpHostClient: HostClient,
    method: Methods,
    startAtList: Array<number>,
    maxResults: number,
    input?: BaseInput,
): Promise<Array<T>> => {
    let allResults = [];
    if (!isEmpty(startAtList)) {
        const startAtChunks = chunk(startAtList, BATCH_SIZE);

        for (const startAtChunk of startAtChunks) {
            const results = await Promise.all(
                startAtChunk.map(async (startAtValue) =>
                    getEachPaginationRecords(
                        builtPath,
                        resultIdentifier,
                        httpHostClient,
                        method,
                        startAtValue,
                        maxResults,
                        input,
                    ),
                ),
            );
            const resultsFlat = results.flatMap((resultItem) => resultItem.records);
            allResults = [...allResults, ...resultsFlat];
        }
    }

    return allResults;
};

const getResultWithoutPagination = async <T>(
    builtPath: string,
    resultIdentifier: ResultIdentifier,
    httpHostClient: HostClient,
    method: Methods,
    maxResults: number,
    input?: BaseInput,
): Promise<Array<T>> => {
    const startTimeMs = new Date().getTime();
    let hasMore = true;
    let currentStartAt = 0;
    let result: Array<any> = [];

    const { total, records } = await getEachPaginationRecords<T>(
        builtPath,
        resultIdentifier,
        httpHostClient,
        method,
        currentStartAt,
        maxResults,
        input,
    );

    const queryId = uuidv4();
    if (total > 1000) {
        console.log(`Pagination total records: ${total}, QueryId: ${queryId}, Path: ${builtPath}`);
    }

    hasMore = total > currentStartAt + maxResults;
    result = [...records];

    const startAtList = [];
    while (hasMore) {
        currentStartAt += maxResults;
        startAtList.push(currentStartAt);
        hasMore = total > currentStartAt + maxResults;
    }

    const paginationResult = await paginationThrottle(
        builtPath,
        resultIdentifier,
        httpHostClient,
        method,
        startAtList,
        maxResults,
        input,
    );
    result = [...result, ...paginationResult];

    const endTimeMs = new Date().getTime();
    const executionTimeMs = (endTimeMs - startTimeMs) / 1000;
    if (executionTimeMs > 10) {
        console.log(
            `Pagination execution time: ${executionTimeMs} seconds, QueryId: ${queryId}, Path: ${builtPath}, total: ${total}`,
        );
    }

    return result;
};

const isReq = (reqOrOpts: ReqOrOpts): reqOrOpts is ACERequest => {
    return (reqOrOpts as ACERequest).context !== undefined;
};

const requestFn = (httpHostClient: HostClient, method: Methods) => {
    return <T>(path: string, input?: BaseInput) => {
        const builtPath = buildPath(path, input);
        return makeRequest<T>(httpHostClient, method, builtPath, input);
    };
};

const unpaginatedRequestFn = (httpHostClient: HostClient, method: Methods) => {
    return <T>(path: string, jiraQueryType: JiraQueryType, input?: BaseInput) => {
        const builtPath = buildPath(path, input);

        let resultIdentifier: ResultIdentifier = 'values';
        let maxResults = 10;
        if (jiraQueryType === JiraQueryType.ISSUE) {
            resultIdentifier = 'issues';
            maxResults = 100;
        } else if (jiraQueryType === JiraQueryType.LABEL) {
            maxResults = 1000;
        } else if (jiraQueryType === JiraQueryType.USER) {
            resultIdentifier = 'users';
            maxResults = 50;
        } else if (jiraQueryType === JiraQueryType.FIELD) {
            maxResults = 50;
        } else if (jiraQueryType === JiraQueryType.PROJECT) {
            maxResults = 50;
        }

        return getResultWithoutPagination<T>(builtPath, resultIdentifier, httpHostClient, method, maxResults, input);
    };
};

export { requestFn, unpaginatedRequestFn, isReq };
