/**
 * 🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨
 * This is a generated file, any changes made to it must be carried over to the
 * new version after we update following the instructions in this package's README.md file
 */

type ModelObject = Record<string, unknown>;

/**
 *
 * @export
 */
export const COLLECTION_FORMATS = {
    csv: ',',
    ssv: ' ',
    tsv: '\t',
    pipes: '|',
};

/**
 *
 * @export
 * @interface FetchArgs
 */
export interface FetchArgs {
    url: string;
    options: any;
}

/**
 *
 * @export
 * @class RequiredError
 * @extends {Error}
 */
export class RequiredError extends Error {
    name: 'RequiredError';

    constructor(public field: string, msg?: string) {
        super(msg);
    }
}

/**
 *
 * @export
 * @interface ActorInputBean
 */
export interface ActorInputBean {
    /**
     * The account IDs of the users to add as default actors. This parameter accepts a comma-separated list. For example, `\"user\":[\"5b10a2844c20165700ede21g\", \"5b109f2e9729b51b54dc274d\"]`.
     * @type {Array<string>}
     * @memberof ActorInputBean
     */
    user?: Array<string>;
    /**
     * The name of the group to add as a default actor. This parameter accepts a comma-separated list. For example, `\"group\":[\"project-admin\", \"jira-developers\"]`.
     * @type {Array<string>}
     * @memberof ActorInputBean
     */
    group?: Array<string>;
}
/**
 *
 * @export
 * @interface ActorsMap
 */
export interface ActorsMap {
    /**
     * The user account ID of the user to add.
     * @type {Array<string>}
     * @memberof ActorsMap
     */
    user?: Array<string>;
    /**
     * The name of the group to add.
     * @type {Array<string>}
     * @memberof ActorsMap
     */
    group?: Array<string>;
}
/**
 *
 * @export
 * @interface AddFieldBean
 */
export interface AddFieldBean {
    /**
     * The ID of the field to add.
     * @type {string}
     * @memberof AddFieldBean
     */
    fieldId: string;
}
/**
 *
 * @export
 * @interface AddGroupBean
 */
export interface AddGroupBean {
    [key: string]: any;
}
/**
 * The application the linked item is in.
 * @export
 * @interface Application
 */
export interface Application {
    [key: string]: any;
}
/**
 * Details of an application property.
 * @export
 * @interface ApplicationProperty
 */
export interface ApplicationProperty {
    /**
     * The ID of the application property. The ID and key are the same.
     * @type {string}
     * @memberof ApplicationProperty
     */
    id?: string;
    /**
     * The key of the application property. The ID and key are the same.
     * @type {string}
     * @memberof ApplicationProperty
     */
    key?: string;
    /**
     * The new value.
     * @type {string}
     * @memberof ApplicationProperty
     */
    value?: string;
    /**
     * The name of the application property.
     * @type {string}
     * @memberof ApplicationProperty
     */
    name?: string;
    /**
     * The description of the application property.
     * @type {string}
     * @memberof ApplicationProperty
     */
    desc?: string;
    /**
     * The data type of the application property.
     * @type {string}
     * @memberof ApplicationProperty
     */
    type?: string;
    /**
     * The default value of the application property.
     * @type {string}
     * @memberof ApplicationProperty
     */
    defaultValue?: string;
    /**
     *
     * @type {string}
     * @memberof ApplicationProperty
     */
    example?: string;
    /**
     * The allowed values, if applicable.
     * @type {Array<string>}
     * @memberof ApplicationProperty
     */
    allowedValues?: Array<string>;
}
/**
 * Details of an application role.
 * @export
 * @interface ApplicationRole
 */
export interface ApplicationRole {
    /**
     * The key of the application role.
     * @type {string}
     * @memberof ApplicationRole
     */
    key?: string;
    /**
     * The groups associated with the application role.
     * @type {Array<string>}
     * @memberof ApplicationRole
     */
    groups?: Array<string>;
    /**
     * The display name of the application role.
     * @type {string}
     * @memberof ApplicationRole
     */
    name?: string;
    /**
     * The groups that are granted default access for this application role.
     * @type {Array<string>}
     * @memberof ApplicationRole
     */
    defaultGroups?: Array<string>;
    /**
     * Determines whether this application role should be selected by default on user creation.
     * @type {boolean}
     * @memberof ApplicationRole
     */
    selectedByDefault?: boolean;
    /**
     * Deprecated.
     * @type {boolean}
     * @memberof ApplicationRole
     */
    defined?: boolean;
    /**
     * The maximum count of users on your license.
     * @type {number}
     * @memberof ApplicationRole
     */
    numberOfSeats?: number;
    /**
     * The count of users remaining on your license.
     * @type {number}
     * @memberof ApplicationRole
     */
    remainingSeats?: number;
    /**
     * The number of users counting against your license.
     * @type {number}
     * @memberof ApplicationRole
     */
    userCount?: number;
    /**
     * The [type of users](https://confluence.atlassian.com/x/lRW3Ng) being counted against your license.
     * @type {string}
     * @memberof ApplicationRole
     */
    userCountDescription?: string;
    /**
     *
     * @type {boolean}
     * @memberof ApplicationRole
     */
    hasUnlimitedSeats?: boolean;
    /**
     * Indicates if the application role belongs to Jira platform (`jira-core`).
     * @type {boolean}
     * @memberof ApplicationRole
     */
    platform?: boolean;
}
/**
 * Details of an item associated with the changed record.
 * @export
 * @interface AssociatedItemBean
 */
export interface AssociatedItemBean {
    /**
     * The ID of the associated record.
     * @type {string}
     * @memberof AssociatedItemBean
     */
    id?: string;
    /**
     * The name of the associated record.
     * @type {string}
     * @memberof AssociatedItemBean
     */
    name?: string;
    /**
     * The type of the associated record.
     * @type {string}
     * @memberof AssociatedItemBean
     */
    typeName?: string;
    /**
     * The ID of the associated parent record.
     * @type {string}
     * @memberof AssociatedItemBean
     */
    parentId?: string;
    /**
     * The name of the associated parent record.
     * @type {string}
     * @memberof AssociatedItemBean
     */
    parentName?: string;
}
/**
 * Details about an attachment.
 * @export
 * @interface Attachment
 */
export interface Attachment {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface AttachmentArchiveEntry
 */
export interface AttachmentArchiveEntry {
    /**
     *
     * @type {string}
     * @memberof AttachmentArchiveEntry
     */
    mediaType?: string;
    /**
     *
     * @type {number}
     * @memberof AttachmentArchiveEntry
     */
    entryIndex?: number;
    /**
     *
     * @type {string}
     * @memberof AttachmentArchiveEntry
     */
    abbreviatedName?: string;
    /**
     *
     * @type {string}
     * @memberof AttachmentArchiveEntry
     */
    name?: string;
    /**
     *
     * @type {number}
     * @memberof AttachmentArchiveEntry
     */
    size?: number;
}
/**
 *
 * @export
 * @interface AttachmentArchiveImpl
 */
export interface AttachmentArchiveImpl {
    /**
     * The list of the items included in the archive.
     * @type {Array<AttachmentArchiveEntry>}
     * @memberof AttachmentArchiveImpl
     */
    entries?: Array<AttachmentArchiveEntry>;
    /**
     * The number of items in the archive.
     * @type {number}
     * @memberof AttachmentArchiveImpl
     */
    totalEntryCount?: number;
}
/**
 * Metadata for an item in an attachment archive.
 * @export
 * @interface AttachmentArchiveItemReadable
 */
export interface AttachmentArchiveItemReadable {
    /**
     * The path of the archive item.
     * @type {string}
     * @memberof AttachmentArchiveItemReadable
     */
    path?: string;
    /**
     * The position of the item within the archive.
     * @type {number}
     * @memberof AttachmentArchiveItemReadable
     */
    index?: number;
    /**
     * The size of the archive item.
     * @type {string}
     * @memberof AttachmentArchiveItemReadable
     */
    size?: string;
    /**
     * The MIME type of the archive item.
     * @type {string}
     * @memberof AttachmentArchiveItemReadable
     */
    mediaType?: string;
    /**
     * The label for the archive item.
     * @type {string}
     * @memberof AttachmentArchiveItemReadable
     */
    label?: string;
}
/**
 * Metadata for an archive (for example a zip) and its contents.
 * @export
 * @interface AttachmentArchiveMetadataReadable
 */
export interface AttachmentArchiveMetadataReadable {
    /**
     * The ID of the attachment.
     * @type {number}
     * @memberof AttachmentArchiveMetadataReadable
     */
    id?: number;
    /**
     * The name of the archive file.
     * @type {string}
     * @memberof AttachmentArchiveMetadataReadable
     */
    name?: string;
    /**
     * The list of the items included in the archive.
     * @type {Array<AttachmentArchiveItemReadable>}
     * @memberof AttachmentArchiveMetadataReadable
     */
    entries?: Array<AttachmentArchiveItemReadable>;
    /**
     * The number of items included in the archive.
     * @type {number}
     * @memberof AttachmentArchiveMetadataReadable
     */
    totalEntryCount?: number;
    /**
     * The MIME type of the attachment.
     * @type {string}
     * @memberof AttachmentArchiveMetadataReadable
     */
    mediaType?: string;
}
/**
 * Metadata for an issue attachment.
 * @export
 * @interface AttachmentMetadata
 */
export interface AttachmentMetadata {
    /**
     * The ID of the attachment.
     * @type {number}
     * @memberof AttachmentMetadata
     */
    id?: number;
    /**
     * The URL of the attachment metadata details.
     * @type {string}
     * @memberof AttachmentMetadata
     */
    self?: string;
    /**
     * The name of the attachment file.
     * @type {string}
     * @memberof AttachmentMetadata
     */
    filename?: string;
    /**
     * Details of the user who attached the file.
     * @type {User}
     * @memberof AttachmentMetadata
     */
    author?: User;
    /**
     * The datetime the attachment was created.
     * @type {Date}
     * @memberof AttachmentMetadata
     */
    created?: Date;
    /**
     * The size of the attachment.
     * @type {number}
     * @memberof AttachmentMetadata
     */
    size?: number;
    /**
     * The MIME type of the attachment.
     * @type {string}
     * @memberof AttachmentMetadata
     */
    mimeType?: string;
    /**
     * Additional properties of the attachment.
     * @type {{ [key: string]: ModelObject; }}
     * @memberof AttachmentMetadata
     */
    properties?: { [key: string]: ModelObject };
    /**
     * The URL of the attachment.
     * @type {string}
     * @memberof AttachmentMetadata
     */
    content?: string;
    /**
     * The URL of a thumbnail representing the attachment.
     * @type {string}
     * @memberof AttachmentMetadata
     */
    thumbnail?: string;
}
/**
 * Details of the instance's attachment settings.
 * @export
 * @interface AttachmentSettings
 */
export interface AttachmentSettings {
    /**
     * Whether the ability to add attachments is enabled.
     * @type {boolean}
     * @memberof AttachmentSettings
     */
    enabled?: boolean;
    /**
     * The maximum size of attachments permitted, in bytes.
     * @type {number}
     * @memberof AttachmentSettings
     */
    uploadLimit?: number;
}
/**
 * An audit record.
 * @export
 * @interface AuditRecordBean
 */
export interface AuditRecordBean {
    /**
     * The ID of the audit record.
     * @type {number}
     * @memberof AuditRecordBean
     */
    id?: number;
    /**
     * The summary of the audit record.
     * @type {string}
     * @memberof AuditRecordBean
     */
    summary?: string;
    /**
     * The URL of the computer where the creation of the audit record was initiated.
     * @type {string}
     * @memberof AuditRecordBean
     */
    remoteAddress?: string;
    /**
     * Deprecated, use `authorAccountId` instead. The key of the user who created the audit record.
     * @type {string}
     * @memberof AuditRecordBean
     */
    authorKey?: string;
    /**
     * The date and time on which the audit record was created.
     * @type {Date}
     * @memberof AuditRecordBean
     */
    created?: Date;
    /**
     * The category of the audit record. For a list of these categories, see the help article [Auditing in Jira applications](https://confluence.atlassian.com/x/noXKM).
     * @type {string}
     * @memberof AuditRecordBean
     */
    category?: string;
    /**
     * The event the audit record originated from.
     * @type {string}
     * @memberof AuditRecordBean
     */
    eventSource?: string;
    /**
     * The description of the audit record.
     * @type {string}
     * @memberof AuditRecordBean
     */
    description?: string;
    /**
     *
     * @type {AssociatedItemBean}
     * @memberof AuditRecordBean
     */
    objectItem?: AssociatedItemBean;
    /**
     * The list of values changed in the record event.
     * @type {Array<ChangedValueBean>}
     * @memberof AuditRecordBean
     */
    changedValues?: Array<ChangedValueBean>;
    /**
     * The list of items associated with the changed record.
     * @type {Array<AssociatedItemBean>}
     * @memberof AuditRecordBean
     */
    associatedItems?: Array<AssociatedItemBean>;
}
/**
 * Container for a list of audit records.
 * @export
 * @interface AuditRecords
 */
export interface AuditRecords {
    /**
     * The number of audit items skipped before the first item in this list.
     * @type {number}
     * @memberof AuditRecords
     */
    offset?: number;
    /**
     * The requested or default limit on the number of audit items to be returned.
     * @type {number}
     * @memberof AuditRecords
     */
    limit?: number;
    /**
     * The total number of audit items returned.
     * @type {number}
     * @memberof AuditRecords
     */
    total?: number;
    /**
     * The list of audit items.
     * @type {Array<AuditRecordBean>}
     * @memberof AuditRecords
     */
    records?: Array<AuditRecordBean>;
}
/**
 * A field auto-complete suggestion.
 * @export
 * @interface AutoCompleteSuggestion
 */
export interface AutoCompleteSuggestion {
    /**
     * The value of a suggested item.
     * @type {string}
     * @memberof AutoCompleteSuggestion
     */
    value?: string;
    /**
     * The display name of a suggested item. If `fieldValue` or `predicateValue` are provided, the matching text is highlighted with the HTML bold tag.
     * @type {string}
     * @memberof AutoCompleteSuggestion
     */
    displayName?: string;
}
/**
 * The results from a JQL query.
 * @export
 * @interface AutoCompleteSuggestions
 */
export interface AutoCompleteSuggestions {
    /**
     * The list of suggested item.
     * @type {Array<AutoCompleteSuggestion>}
     * @memberof AutoCompleteSuggestions
     */
    results?: Array<AutoCompleteSuggestion>;
}
/**
 * Details of an avatar.
 * @export
 * @interface Avatar
 */
export interface Avatar {
    /**
     * The ID of the avatar.
     * @type {string}
     * @memberof Avatar
     */
    id: string;
    /**
     * The owner of the avatar. For a system avatar the owner is null (and nothing is returned). For non-system avatars this is the appropriate identifier, such as the ID for a project or the account ID for a user.
     * @type {string}
     * @memberof Avatar
     */
    owner?: string;
    /**
     * Whether the avatar is a system avatar.
     * @type {boolean}
     * @memberof Avatar
     */
    isSystemAvatar?: boolean;
    /**
     * Whether the avatar is used in Jira. For example, shown as a project's avatar.
     * @type {boolean}
     * @memberof Avatar
     */
    isSelected?: boolean;
    /**
     * Whether the avatar can be deleted.
     * @type {boolean}
     * @memberof Avatar
     */
    isDeletable?: boolean;
    /**
     * The file name of the avatar icon. Returned for system avatars.
     * @type {string}
     * @memberof Avatar
     */
    fileName?: string;
    /**
     * The list of avatar icon URLs.
     * @type {{ [key: string]: string; }}
     * @memberof Avatar
     */
    urls?: { [key: string]: string };
}
/**
 *
 * @export
 * @interface AvatarUrls
 */
export interface AvatarUrls {}
/**
 *
 * @export
 * @interface AvatarUrlsBean
 */
export interface AvatarUrlsBean {
    /**
     * The URL of the item's 16x16 pixel avatar.
     * @type {string}
     * @memberof AvatarUrlsBean
     */
    _16x16?: string;
    /**
     * The URL of the item's 24x24 pixel avatar.
     * @type {string}
     * @memberof AvatarUrlsBean
     */
    _24x24?: string;
    /**
     * The URL of the item's 32x32 pixel avatar.
     * @type {string}
     * @memberof AvatarUrlsBean
     */
    _32x32?: string;
    /**
     * The URL of the item's 48x48 pixel avatar.
     * @type {string}
     * @memberof AvatarUrlsBean
     */
    _48x48?: string;
}
/**
 * Details about system and custom avatars.
 * @export
 * @interface Avatars
 */
export interface Avatars {
    /**
     * System avatars list.
     * @type {Array<Avatar>}
     * @memberof Avatars
     */
    system?: Array<Avatar>;
    /**
     * Custom avatars list.
     * @type {Array<Avatar>}
     * @memberof Avatars
     */
    custom?: Array<Avatar>;
}
/**
 * Details of options to create for a custom field.
 * @export
 * @interface BulkCreateCustomFieldOptionRequest
 */
export interface BulkCreateCustomFieldOptionRequest {
    /**
     * Details of options to create.
     * @type {Array<CustomFieldOptionValue>}
     * @memberof BulkCreateCustomFieldOptionRequest
     */
    options?: Array<CustomFieldOptionValue>;
}
/**
 * Bulk issue property update request details.
 * @export
 * @interface BulkIssuePropertyUpdateRequest
 */
export interface BulkIssuePropertyUpdateRequest {
    /**
     * The value of the property. The value must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.
     * @type {ModelObject}
     * @memberof BulkIssuePropertyUpdateRequest
     */
    value?: ModelObject;
    /**
     * The bulk operation filter.
     * @type {IssueFilterForBulkPropertySet}
     * @memberof BulkIssuePropertyUpdateRequest
     */
    filter?: IssueFilterForBulkPropertySet;
    /**
     * The Jira expression to calculate the value of the property. The value of the expression must be an object that can be converted to JSON, such as a number, boolean, string, list, or map. The context variables available to the expression are issue and user. Issues for which the expression returns a value whose JSON representation is longer than 32768 characters are ignored.
     * @type {string}
     * @memberof BulkIssuePropertyUpdateRequest
     */
    expression?: string;
}

export enum BulkIssuePropertyUpdateRequestStatus {
    RUNNING = 'RUNNING',
    COMPLETE = 'COMPLETE',
    FAILED = 'FAILED',
}

export interface BulkIssuePropertyUpdateRequestResultErrors {
    errorMessages: string[];
    reasons: string[];
}
export interface BulkIssuePropertyUpdateRequestResult {
    updatedTotal?: number;
    errors?: BulkIssuePropertyUpdateRequestResultErrors;
}
export interface BulkIssuePropertyUpdateResponse {
    id: string;
    self: string;
    status: BulkIssuePropertyUpdateRequestStatus;
    message: string;
    result: BulkIssuePropertyUpdateRequestResult;
}

/**
 *
 * @export
 * @interface BulkOperationErrorResult
 */
export interface BulkOperationErrorResult {
    /**
     *
     * @type {number}
     * @memberof BulkOperationErrorResult
     */
    status?: number;
    /**
     *
     * @type {ErrorCollection}
     * @memberof BulkOperationErrorResult
     */
    elementErrors?: ErrorCollection;
    /**
     *
     * @type {number}
     * @memberof BulkOperationErrorResult
     */
    failedElementNumber?: number;
}
/**
 * Details of global and project permissions granted to the user.
 * @export
 * @interface BulkPermissionGrants
 */
export interface BulkPermissionGrants {
    /**
     * List of project permissions and the projects and issues those permissions provide access to.
     * @type {Array<BulkProjectPermissionGrants>}
     * @memberof BulkPermissionGrants
     */
    projectPermissions: Array<BulkProjectPermissionGrants>;
    /**
     * List of permissions granted to the user.
     * @type {Array<string>}
     * @memberof BulkPermissionGrants
     */
    globalPermissions: Array<string>;
}
/**
 * Details of global permissions to look up and project permissions with associated projects and issues to look up.
 * @export
 * @interface BulkPermissionsRequestBean
 */
export interface BulkPermissionsRequestBean {
    /**
     * Project permissions with associated projects and issues to look up.
     * @type {Array<BulkProjectPermissions>}
     * @memberof BulkPermissionsRequestBean
     */
    projectPermissions?: Array<BulkProjectPermissions>;
    /**
     * Global permissions to look up.
     * @type {Array<string>}
     * @memberof BulkPermissionsRequestBean
     */
    globalPermissions?: Array<string>;
    /**
     * The account ID of a user.
     * @type {string}
     * @memberof BulkPermissionsRequestBean
     */
    accountId?: string;
}
/**
 * List of project permissions and the projects and issues those permissions grant access to.
 * @export
 * @interface BulkProjectPermissionGrants
 */
export interface BulkProjectPermissionGrants {
    /**
     * A project permission,
     * @type {string}
     * @memberof BulkProjectPermissionGrants
     */
    permission: string;
    /**
     * IDs of the issues the user has the permission for.
     * @type {Array<number>}
     * @memberof BulkProjectPermissionGrants
     */
    issues: Array<number>;
    /**
     * IDs of the projects the user has the permission for.
     * @type {Array<number>}
     * @memberof BulkProjectPermissionGrants
     */
    projects: Array<number>;
}
/**
 * Details of project permissions and associated issues and projects to look up.
 * @export
 * @interface BulkProjectPermissions
 */
export interface BulkProjectPermissions {
    /**
     * List of issue IDs.
     * @type {Array<number>}
     * @memberof BulkProjectPermissions
     */
    issues?: Array<number>;
    /**
     * List of project IDs.
     * @type {Array<number>}
     * @memberof BulkProjectPermissions
     */
    projects?: Array<number>;
    /**
     * List of project permissions.
     * @type {Array<string>}
     * @memberof BulkProjectPermissions
     */
    permissions: Array<string>;
}
/**
 * A change item.
 * @export
 * @interface ChangeDetails
 */
export interface ChangeDetails {
    /**
     * The name of the field changed.
     * @type {string}
     * @memberof ChangeDetails
     */
    field?: string;
    /**
     * The type of the field changed.
     * @type {string}
     * @memberof ChangeDetails
     */
    fieldtype?: string;
    /**
     * The ID of the field changed.
     * @type {string}
     * @memberof ChangeDetails
     */
    fieldId?: string;
    /**
     * The details of the original value.
     * @type {string}
     * @memberof ChangeDetails
     */
    from?: string;
    /**
     * The details of the original value as a string.
     * @type {string}
     * @memberof ChangeDetails
     */
    fromString?: string;
    /**
     * The details of the new value.
     * @type {string}
     * @memberof ChangeDetails
     */
    to?: string;
    /**
     * The details of the new value as a string.
     * @type {string}
     * @memberof ChangeDetails
     */
    toString?: string;
}
/**
 * Details of names changed in the record event.
 * @export
 * @interface ChangedValueBean
 */
export interface ChangedValueBean {
    /**
     * The name of the field changed.
     * @type {string}
     * @memberof ChangedValueBean
     */
    fieldName?: string;
    /**
     * The value of the field before the change.
     * @type {string}
     * @memberof ChangedValueBean
     */
    changedFrom?: string;
    /**
     * The value of the field after the change.
     * @type {string}
     * @memberof ChangedValueBean
     */
    changedTo?: string;
}
/**
 * Details of a changed worklog.
 * @export
 * @interface ChangedWorklog
 */
export interface ChangedWorklog {
    /**
     * The ID of the worklog.
     * @type {number}
     * @memberof ChangedWorklog
     */
    worklogId?: number;
    /**
     * The datetime of the change.
     * @type {number}
     * @memberof ChangedWorklog
     */
    updatedTime?: number;
    /**
     * Details of properties associated with the change.
     * @type {Array<EntityProperty>}
     * @memberof ChangedWorklog
     */
    properties?: Array<EntityProperty>;
}
/**
 * List of changed worklogs.
 * @export
 * @interface ChangedWorklogs
 */
export interface ChangedWorklogs {
    /**
     * Changed worklog list.
     * @type {Array<ChangedWorklog>}
     * @memberof ChangedWorklogs
     */
    values?: Array<ChangedWorklog>;
    /**
     * The datetime of the first worklog item in the list.
     * @type {number}
     * @memberof ChangedWorklogs
     */
    since?: number;
    /**
     * The datetime of the last worklog item in the list.
     * @type {number}
     * @memberof ChangedWorklogs
     */
    until?: number;
    /**
     * The URL of this changed worklogs list.
     * @type {string}
     * @memberof ChangedWorklogs
     */
    self?: string;
    /**
     * The URL of the next list of changed worklogs.
     * @type {string}
     * @memberof ChangedWorklogs
     */
    nextPage?: string;
    /**
     *
     * @type {boolean}
     * @memberof ChangedWorklogs
     */
    lastPage?: boolean;
}
/**
 * A changelog.
 * @export
 * @interface Changelog
 */
export interface Changelog {
    /**
     * The ID of the changelog.
     * @type {string}
     * @memberof Changelog
     */
    id?: string;
    /**
     * The user who made the change.
     * @type {UserDetails}
     * @memberof Changelog
     */
    author?: UserDetails;
    /**
     * The date on which the change took place.
     * @type {Date}
     * @memberof Changelog
     */
    created?: Date;
    /**
     * The list of items changed.
     * @type {Array<ChangeDetails>}
     * @memberof Changelog
     */
    items?: Array<ChangeDetails>;
    /**
     * The history metadata associated with the changed.
     * @type {HistoryMetadata}
     * @memberof Changelog
     */
    historyMetadata?: HistoryMetadata;
}
/**
 * Details of an issue navigator column item.
 * @export
 * @interface ColumnItem
 */
export interface ColumnItem {
    /**
     * The issue navigator column label.
     * @type {string}
     * @memberof ColumnItem
     */
    label?: string;
    /**
     * The issue navigator column value.
     * @type {string}
     * @memberof ColumnItem
     */
    value?: string;
}
/**
 * A comment.
 * @export
 * @interface Comment
 */
export interface Comment {
    [key: string]: any;
}
/**
 * Details about a project component.
 * @export
 * @interface Component
 */
export interface Component {
    /**
     * The URL of the component.
     * @type {string}
     * @memberof Component
     */
    self?: string;
    /**
     * The unique identifier for the component.
     * @type {string}
     * @memberof Component
     */
    id?: string;
    /**
     * The unique name for the component in the project. Required when creating a component. Optional when updating a component. The maximum length is 255 characters.
     * @type {string}
     * @memberof Component
     */
    name?: string;
    /**
     * The description for the component. Optional when creating or updating a component.
     * @type {string}
     * @memberof Component
     */
    description?: string;
    /**
     * The user details for the component's lead user.
     * @type {User}
     * @memberof Component
     */
    lead?: User;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof Component
     */
    leadUserName?: string;
    /**
     * The accountId of the component's lead user. The accountId uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*.
     * @type {string}
     * @memberof Component
     */
    leadAccountId?: string;
    /**
     * The nominal user type used to determine the assignee for issues created with this component. See `realAssigneeType` for details on how the type of the user, and hence the user, assigned to issues is determined. Can take the following values:   *  `PROJECT_LEAD` the assignee to any issues created with this component is nominally the lead for the project the component is in.  *  `COMPONENT_LEAD` the assignee to any issues created with this component is nominally the lead for the component.  *  `UNASSIGNED` an assignee is not set for issues created with this component.  *  `PROJECT_DEFAULT` the assignee to any issues created with this component is nominally the default assignee for the project that the component is in.  Default value: `PROJECT_DEFAULT`.   Optional when creating or updating a component.
     * @type {string}
     * @memberof Component
     */
    assigneeType?: Component.AssigneeTypeEnum;
    /**
     * The details of the user associated with `assigneeType`, if any. See `realAssignee` for details of the user assigned to issues created with this component.
     * @type {User}
     * @memberof Component
     */
    assignee?: User;
    /**
     * The type of the assignee that is assigned to issues created with this component, when an assignee cannot be set from the `assigneeType`. For example, `assigneeType` is set to `COMPONENT_LEAD` but no component lead is set. This property is set to one of the following values:   *  `PROJECT_LEAD` when `assigneeType` is `PROJECT_LEAD` and the project lead has permission to be assigned issues in the project that the component is in.  *  `COMPONENT_LEAD` when `assignee`Type is `COMPONENT_LEAD` and the component lead has permission to be assigned issues in the project that the component is in.  *  `UNASSIGNED` when `assigneeType` is `UNASSIGNED` and Jira is configured to allow unassigned issues.  *  `PROJECT_DEFAULT` when none of the preceding cases are true.
     * @type {string}
     * @memberof Component
     */
    realAssigneeType?: Component.RealAssigneeTypeEnum;
    /**
     * The user assigned to issues created with this component, when `assigneeType` does not identify a valid assignee.
     * @type {User}
     * @memberof Component
     */
    realAssignee?: User;
    /**
     * Whether a user is associated with `assigneeType`. For example, if the `assigneeType` is set to `COMPONENT_LEAD` but the component lead is not set, then `false` is returned.
     * @type {boolean}
     * @memberof Component
     */
    isAssigneeTypeValid?: boolean;
    /**
     * The key of the project the component is assigned to. Required when creating a component. Can't be updated.
     * @type {string}
     * @memberof Component
     */
    project?: string;
    /**
     * The ID of the project the component is assigned to.
     * @type {number}
     * @memberof Component
     */
    projectId?: number;
}

/**
 * @export
 * @namespace Component
 */
export namespace Component {
    /**
     * @export
     * @enum {string}
     */
    export enum AssigneeTypeEnum {
        PROJECTDEFAULT = <any>'PROJECT_DEFAULT',
        COMPONENTLEAD = <any>'COMPONENT_LEAD',
        PROJECTLEAD = <any>'PROJECT_LEAD',
        UNASSIGNED = <any>'UNASSIGNED',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum RealAssigneeTypeEnum {
        PROJECTDEFAULT = <any>'PROJECT_DEFAULT',
        COMPONENTLEAD = <any>'COMPONENT_LEAD',
        PROJECTLEAD = <any>'PROJECT_LEAD',
        UNASSIGNED = <any>'UNASSIGNED',
    }
}
/**
 * Count of issues assigned to a component.
 * @export
 * @interface ComponentIssuesCount
 */
export interface ComponentIssuesCount {
    /**
     * The URL for this count of issues for a component.
     * @type {string}
     * @memberof ComponentIssuesCount
     */
    self?: string;
    /**
     * The count of issues assigned to a component.
     * @type {number}
     * @memberof ComponentIssuesCount
     */
    issueCount?: number;
}
/**
 * Details about a component with a count of the issues it contains.
 * @export
 * @interface ComponentWithIssueCount
 */
export interface ComponentWithIssueCount {
    /**
     * Count of issues for the component.
     * @type {number}
     * @memberof ComponentWithIssueCount
     */
    issueCount?: number;
    /**
     * The description for the component.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    description?: string;
    /**
     * The URL for this count of the issues contained in the component.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    self?: string;
    /**
     * The key of the project to which the component is assigned.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    project?: string;
    /**
     * Not used.
     * @type {number}
     * @memberof ComponentWithIssueCount
     */
    projectId?: number;
    /**
     * The details of the user associated with `assigneeType`, if any. See `realAssignee` for details of the user assigned to issues created with this component.
     * @type {User}
     * @memberof ComponentWithIssueCount
     */
    assignee?: User;
    /**
     * The user details for the component's lead user.
     * @type {User}
     * @memberof ComponentWithIssueCount
     */
    lead?: User;
    /**
     * The nominal user type used to determine the assignee for issues created with this component. See `realAssigneeType` for details on how the type of the user, and hence the user, assigned to issues is determined. Takes the following values:   *  `PROJECT_LEAD` the assignee to any issues created with this component is nominally the lead for the project the component is in.  *  `COMPONENT_LEAD` the assignee to any issues created with this component is nominally the lead for the component.  *  `UNASSIGNED` an assignee is not set for issues created with this component.  *  `PROJECT_DEFAULT` the assignee to any issues created with this component is nominally the default assignee for the project that the component is in.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    assigneeType?: ComponentWithIssueCount.AssigneeTypeEnum;
    /**
     * The user assigned to issues created with this component, when `assigneeType` does not identify a valid assignee.
     * @type {User}
     * @memberof ComponentWithIssueCount
     */
    realAssignee?: User;
    /**
     * The type of the assignee that is assigned to issues created with this component, when an assignee cannot be set from the `assigneeType`. For example, `assigneeType` is set to `COMPONENT_LEAD` but no component lead is set. This property is set to one of the following values:   *  `PROJECT_LEAD` when `assigneeType` is `PROJECT_LEAD` and the project lead has permission to be assigned issues in the project that the component is in.  *  `COMPONENT_LEAD` when `assignee`Type is `COMPONENT_LEAD` and the component lead has permission to be assigned issues in the project that the component is in.  *  `UNASSIGNED` when `assigneeType` is `UNASSIGNED` and Jira is configured to allow unassigned issues.  *  `PROJECT_DEFAULT` when none of the preceding cases are true.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    realAssigneeType?: ComponentWithIssueCount.RealAssigneeTypeEnum;
    /**
     * Whether a user is associated with `assigneeType`. For example, if the `assigneeType` is set to `COMPONENT_LEAD` but the component lead is not set, then `false` is returned.
     * @type {boolean}
     * @memberof ComponentWithIssueCount
     */
    isAssigneeTypeValid?: boolean;
    /**
     * The name for the component.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    name?: string;
    /**
     * The unique identifier for the component.
     * @type {string}
     * @memberof ComponentWithIssueCount
     */
    id?: string;
}

/**
 * @export
 * @namespace ComponentWithIssueCount
 */
export namespace ComponentWithIssueCount {
    /**
     * @export
     * @enum {string}
     */
    export enum AssigneeTypeEnum {
        PROJECTDEFAULT = <any>'PROJECT_DEFAULT',
        COMPONENTLEAD = <any>'COMPONENT_LEAD',
        PROJECTLEAD = <any>'PROJECT_LEAD',
        UNASSIGNED = <any>'UNASSIGNED',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum RealAssigneeTypeEnum {
        PROJECTDEFAULT = <any>'PROJECT_DEFAULT',
        COMPONENTLEAD = <any>'COMPONENT_LEAD',
        PROJECTLEAD = <any>'PROJECT_LEAD',
        UNASSIGNED = <any>'UNASSIGNED',
    }
}
/**
 * A JQL query clause that consists of nested clauses. For example, `(labels in (urgent, blocker) OR lastCommentedBy = currentUser()). Note that, where nesting is not defined, the parser nests JQL clauses based on the operator precedence. For example, \"A OR B AND C\" is parsed as \"(A OR B) AND C\". See Setting the precedence of operators for more information about precedence in JQL queries.`
 * @export
 * @interface CompoundClause
 */
export interface CompoundClause {
    /**
     * The list of nested clauses.
     * @type {Array<JqlQueryClause>}
     * @memberof CompoundClause
     */
    clauses: Array<JqlQueryClause>;
    /**
     * The operator between the clauses.
     * @type {string}
     * @memberof CompoundClause
     */
    operator: CompoundClause.OperatorEnum;
}

/**
 * @export
 * @namespace CompoundClause
 */
export namespace CompoundClause {
    /**
     * @export
     * @enum {string}
     */
    export enum OperatorEnum {
        And = <any>'and',
        Or = <any>'or',
        Not = <any>'not',
    }
}
/**
 * Details about the configuration of Jira.
 * @export
 * @interface JiraConfiguration
 */
export interface JiraConfiguration {
    /**
     * Whether the ability for users to vote on issues is enabled. See [Configuring Jira application options](https://confluence.atlassian.com/x/uYXKM) for details.
     * @type {boolean}
     * @memberof Configuration
     */
    votingEnabled?: boolean;
    /**
     * Whether the ability for users to watch issues is enabled. See [Configuring Jira application options](https://confluence.atlassian.com/x/uYXKM) for details.
     * @type {boolean}
     * @memberof Configuration
     */
    watchingEnabled?: boolean;
    /**
     * Whether the ability to create unassigned issues is enabled. See [Configuring Jira application options](https://confluence.atlassian.com/x/uYXKM) for details.
     * @type {boolean}
     * @memberof Configuration
     */
    unassignedIssuesAllowed?: boolean;
    /**
     * Whether the ability to create subtasks for issues is enabled.
     * @type {boolean}
     * @memberof Configuration
     */
    subTasksEnabled?: boolean;
    /**
     * Whether the ability to link issues is enabled.
     * @type {boolean}
     * @memberof Configuration
     */
    issueLinkingEnabled?: boolean;
    /**
     * Whether the ability to track time is enabled. This property is deprecated.
     * @type {boolean}
     * @memberof Configuration
     */
    timeTrackingEnabled?: boolean;
    /**
     * Whether the ability to add attachments to issues is enabled.
     * @type {boolean}
     * @memberof Configuration
     */
    attachmentsEnabled?: boolean;
    /**
     * The configuration of time tracking.
     * @type {TimeTrackingConfiguration}
     * @memberof Configuration
     */
    timeTrackingConfiguration?: TimeTrackingConfiguration;
}
/**
 * A [Connect module](https://developer.atlassian.com/cloud/jira/platform/about-jira-modules/) in the same format as in the [app descriptor](https://developer.atlassian.com/cloud/jira/platform/app-descriptor/).
 * @export
 * @interface ConnectModule
 */
export interface ConnectModule {}
/**
 *
 * @export
 * @interface ConnectModules
 */
export interface ConnectModules {
    /**
     * A list of app modules in the same format as the `modules` property in the [app descriptor](https://developer.atlassian.com/cloud/jira/platform/app-descriptor/).
     * @type {Array<ConnectModule>}
     * @memberof ConnectModules
     */
    modules: Array<ConnectModule>;
}
/**
 * A workflow transition rule.
 * @export
 * @interface ConnectWorkflowTransitionRule
 */
export interface ConnectWorkflowTransitionRule {
    /**
     * The ID of the transition rule.
     * @type {string}
     * @memberof ConnectWorkflowTransitionRule
     */
    id: string;
    /**
     * The key of the rule, as defined in the Connect app descriptor.
     * @type {string}
     * @memberof ConnectWorkflowTransitionRule
     */
    key: string;
    /**
     *
     * @type {RuleConfiguration}
     * @memberof ConnectWorkflowTransitionRule
     */
    configuration?: RuleConfiguration;
    /**
     *
     * @type {WorkflowTransition}
     * @memberof ConnectWorkflowTransitionRule
     */
    transition?: WorkflowTransition;
}
/**
 * Container for a list of registered webhooks. Webhook details are returned in the same order as the request.
 * @export
 * @interface ContainerForRegisteredWebhooks
 */
export interface ContainerForRegisteredWebhooks {
    /**
     * A list of registered webhooks.
     * @type {Array<RegisteredWebhook>}
     * @memberof ContainerForRegisteredWebhooks
     */
    webhookRegistrationResult?: Array<RegisteredWebhook>;
}
/**
 * Container for a list of webhook IDs.
 * @export
 * @interface ContainerForWebhookIDs
 */
export interface ContainerForWebhookIDs {
    /**
     * A list of webhook IDs.
     * @type {Array<number>}
     * @memberof ContainerForWebhookIDs
     */
    webhookIds: Array<number>;
}
/**
 * A container for a list of workflow schemes together with the projects they are associated with.
 * @export
 * @interface ContainerOfWorkflowSchemeAssociations
 */
export interface ContainerOfWorkflowSchemeAssociations {
    /**
     * A list of workflow schemes together with projects they are associated with.
     * @type {Array<WorkflowSchemeAssociations>}
     * @memberof ContainerOfWorkflowSchemeAssociations
     */
    values: Array<WorkflowSchemeAssociations>;
}
/**
 * A context.
 * @export
 * @interface Context
 */
export interface Context {
    /**
     * The ID of the context.
     * @type {number}
     * @memberof Context
     */
    id?: number;
    /**
     * The name of the context.
     * @type {string}
     * @memberof Context
     */
    name?: string;
    /**
     * The scope of the context.
     * @type {Scope}
     * @memberof Context
     */
    scope?: Scope;
}
/**
 * The converted JQL queries.
 * @export
 * @interface ConvertedJQLQueries
 */
export interface ConvertedJQLQueries {
    /**
     * The list of converted query strings with account IDs in place of user identifiers.
     * @type {Array<string>}
     * @memberof ConvertedJQLQueries
     */
    queryStrings?: Array<string>;
    /**
     * List of queries containing user information that could not be mapped to an existing user
     * @type {Array<JQLQueryWithUnknownUsers>}
     * @memberof ConvertedJQLQueries
     */
    queriesWithUnknownUsers?: Array<JQLQueryWithUnknownUsers>;
}
/**
 *
 * @export
 * @interface CreateUpdateRoleRequestBean
 */
export interface CreateUpdateRoleRequestBean {
    /**
     * The name of the project role. Must be unique. Cannot begin or end with whitespace. The maximum length is 255 characters. Required when creating a project role. Optional when partially updating a project role.
     * @type {string}
     * @memberof CreateUpdateRoleRequestBean
     */
    name?: string;
    /**
     * A description of the project role. Required when fully updating a project role. Optional when creating or partially updating a project role.
     * @type {string}
     * @memberof CreateUpdateRoleRequestBean
     */
    description?: string;
}
/**
 * Details about a created issue or subtask.
 * @export
 * @interface CreatedIssue
 */
export interface CreatedIssue {
    /**
     * The ID of the created issue or subtask.
     * @type {string}
     * @memberof CreatedIssue
     */
    id?: string;
    /**
     * The key of the created issue or subtask.
     * @type {string}
     * @memberof CreatedIssue
     */
    key?: string;
    /**
     * The URL of the created issue or subtask.
     * @type {string}
     * @memberof CreatedIssue
     */
    self?: string;
    /**
     * The response code and messages related to any requested transition.
     * @type {NestedResponse}
     * @memberof CreatedIssue
     */
    transition?: NestedResponse;
}
/**
 * Details about the issues created and the errors for requests that failed.
 * @export
 * @interface CreatedIssues
 */
export interface CreatedIssues {
    /**
     * Details of the issues created.
     * @type {Array<CreatedIssue>}
     * @memberof CreatedIssues
     */
    issues?: Array<CreatedIssue>;
    /**
     * Error details for failed issue creation requests.
     * @type {Array<BulkOperationErrorResult>}
     * @memberof CreatedIssues
     */
    errors?: Array<BulkOperationErrorResult>;
}
/**
 *
 * @export
 * @interface CustomFieldDefinitionJsonBean
 */
export interface CustomFieldDefinitionJsonBean {
    /**
     * The name of the custom field, which is displayed in Jira. This is not the unique identifier.
     * @type {string}
     * @memberof CustomFieldDefinitionJsonBean
     */
    name: string;
    /**
     * The description of the custom field, which is displayed in Jira.
     * @type {string}
     * @memberof CustomFieldDefinitionJsonBean
     */
    description?: string;
    /**
     * The type of the custom field. For example, *com.atlassian.jira.plugin.system.customfieldtypes:grouppicker*.   *  `cascadingselect`: Allows multiple values to be selected using two select lists  *  `datepicker`: Stores a date using a picker control  *  `datetime`: Stores a date with a time component  *  `float`: Stores and validates a numeric (floating point) input  *  `grouppicker`: Stores a user group using a picker control  *  `importid`: A read-only field that stores the previous ID of the issue from the system that it was imported from  *  `labels`: Stores labels  *  `multicheckboxes`: Stores multiple values using checkboxes  *  `multigrouppicker`: Stores multiple user groups using a picker control  *  `multiselect`: Stores multiple values using a select list  *  `multiuserpicker`: Stores multiple users using a picker control  *  `multiversion`: Stores multiple versions from the versions available in a project using a picker control  *  `project`: Stores a project from a list of projects that the user is permitted to view  *  `radiobuttons`: Stores a value using radio buttons  *  `readonlyfield`: Stores a read-only text value, which can only be populated via the API  *  `select`: Stores a value from a configurable list of options  *  `textarea`: Stores a long text string using a multiline text area  *  `textfield`: Stores a text string using a single-line text box  *  `url`: Stores a URL  *  `userpicker`: Stores a user using a picker control  *  `version`: Stores a version using a picker control
     * @type {string}
     * @memberof CustomFieldDefinitionJsonBean
     */
    type: CustomFieldDefinitionJsonBean.TypeEnum;
    /**
     * The searcher defines the way the field is searched in Jira. For example, *com.atlassian.jira.plugin.system.customfieldtypes:grouppickersearcher*.   The search UI (basic search and JQL search) will display different operations and values for the field, based on the field searcher. You must specify a searcher that is valid for the field type, as listed below (abbreviated values shown):   *  `cascadingselect`: `cascadingselectsearcher`  *  `datepicker`: `daterange`  *  `datetime`: `datetimerange`  *  `float`: `exactnumber` or `numberrange`  *  `grouppicker`: `grouppickersearcher`  *  `importid`: `exactnumber` or `numberrange`  *  `labels`: `labelsearcher`  *  `multicheckboxes`: `multiselectsearcher`  *  `multigrouppicker`: `multiselectsearcher`  *  `multiselect`: `multiselectsearcher`  *  `multiuserpicker`: `userpickergroupsearcher`  *  `multiversion`: `versionsearcher`  *  `project`: `projectsearcher`  *  `radiobuttons`: `multiselectsearcher`  *  `readonlyfield`: `textsearcher`  *  `select`: `multiselectsearcher`  *  `textarea`: `textsearcher`  *  `textfield`: `textsearcher`  *  `url`: `exacttextsearcher`  *  `userpicker`: `userpickergroupsearcher`  *  `version`: `versionsearcher`
     * @type {string}
     * @memberof CustomFieldDefinitionJsonBean
     */
    searcherKey: CustomFieldDefinitionJsonBean.SearcherKeyEnum;
}

/**
 * @export
 * @namespace CustomFieldDefinitionJsonBean
 */
export namespace CustomFieldDefinitionJsonBean {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Cascadingselect = <any>'com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect',
        Datepicker = <any>'com.atlassian.jira.plugin.system.customfieldtypes:datepicker',
        Datetime = <any>'com.atlassian.jira.plugin.system.customfieldtypes:datetime',
        Float = <any>'com.atlassian.jira.plugin.system.customfieldtypes:float',
        Grouppicker = <any>'com.atlassian.jira.plugin.system.customfieldtypes:grouppicker',
        Importid = <any>'com.atlassian.jira.plugin.system.customfieldtypes:importid',
        Labels = <any>'com.atlassian.jira.plugin.system.customfieldtypes:labels',
        Multicheckboxes = <any>'com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes',
        Multigrouppicker = <any>'com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker',
        Multiselect = <any>'com.atlassian.jira.plugin.system.customfieldtypes:multiselect',
        Multiuserpicker = <any>'com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker',
        Multiversion = <any>'com.atlassian.jira.plugin.system.customfieldtypes:multiversion',
        Project = <any>'com.atlassian.jira.plugin.system.customfieldtypes:project',
        Radiobuttons = <any>'com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons',
        Readonlyfield = <any>'com.atlassian.jira.plugin.system.customfieldtypes:readonlyfield',
        Select = <any>'com.atlassian.jira.plugin.system.customfieldtypes:select',
        Textarea = <any>'com.atlassian.jira.plugin.system.customfieldtypes:textarea',
        Textfield = <any>'com.atlassian.jira.plugin.system.customfieldtypes:textfield',
        Url = <any>'com.atlassian.jira.plugin.system.customfieldtypes:url',
        Userpicker = <any>'com.atlassian.jira.plugin.system.customfieldtypes:userpicker',
        Version = <any>'com.atlassian.jira.plugin.system.customfieldtypes:version',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum SearcherKeyEnum {
        Cascadingselectsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:cascadingselectsearcher',
        Daterange = <any>'com.atlassian.jira.plugin.system.customfieldtypes:daterange',
        Datetimerange = <any>'com.atlassian.jira.plugin.system.customfieldtypes:datetimerange',
        Exactnumber = <any>'com.atlassian.jira.plugin.system.customfieldtypes:exactnumber',
        Exacttextsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:exacttextsearcher',
        Grouppickersearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:grouppickersearcher',
        Labelsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:labelsearcher',
        Multiselectsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:multiselectsearcher',
        Numberrange = <any>'com.atlassian.jira.plugin.system.customfieldtypes:numberrange',
        Projectsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:projectsearcher',
        Textsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:textsearcher',
        Userpickergroupsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher',
        Versionsearcher = <any>'com.atlassian.jira.plugin.system.customfieldtypes:versionsearcher',
    }
}
/**
 * Details of a custom option for a field.
 * @export
 * @interface CustomFieldOption
 */
export interface CustomFieldOption {
    /**
     * The URL of these custom field option details.
     * @type {string}
     * @memberof CustomFieldOption
     */
    self?: string;
    /**
     * The value of the custom field option.
     * @type {string}
     * @memberof CustomFieldOption
     */
    value?: string;
}
/**
 * Details of a custom field option and its cascading options.
 * @export
 * @interface CustomFieldOptionDetails
 */
export interface CustomFieldOptionDetails {
    /**
     * The ID of the custom field option.
     * @type {number}
     * @memberof CustomFieldOptionDetails
     */
    id?: number;
    /**
     * The value of the custom field option.
     * @type {string}
     * @memberof CustomFieldOptionDetails
     */
    value?: string;
    /**
     * The cascading options.
     * @type {Array<string>}
     * @memberof CustomFieldOptionDetails
     */
    cascadingOptions?: Array<string>;
}
/**
 * Value of a custom field option and the values of its cascading options.
 * @export
 * @interface CustomFieldOptionValue
 */
export interface CustomFieldOptionValue {
    /**
     * The value of the custom field option.
     * @type {string}
     * @memberof CustomFieldOptionValue
     */
    value: string;
    /**
     * The cascading options.
     * @type {Array<string>}
     * @memberof CustomFieldOptionValue
     */
    cascadingOptions?: Array<string>;
}
/**
 * Details about the replacement for a deleted version.
 * @export
 * @interface CustomFieldReplacement
 */
export interface CustomFieldReplacement {
    /**
     * The ID of the custom field in which to replace the version number.
     * @type {number}
     * @memberof CustomFieldReplacement
     */
    customFieldId?: number;
    /**
     * The version number to use as a replacement for the deleted version.
     * @type {number}
     * @memberof CustomFieldReplacement
     */
    moveTo?: number;
}
/**
 * Details of a dashboard.
 * @export
 * @interface Dashboard
 */
export interface Dashboard {
    /**
     *
     * @type {string}
     * @memberof Dashboard
     */
    description?: string;
    /**
     * The ID of the dashboard.
     * @type {string}
     * @memberof Dashboard
     */
    id?: string;
    /**
     * Whether the dashboard is selected as a favorite by the user.
     * @type {boolean}
     * @memberof Dashboard
     */
    isFavourite?: boolean;
    /**
     * The name of the dashboard.
     * @type {string}
     * @memberof Dashboard
     */
    name?: string;
    /**
     * The owner of the dashboard.
     * @type {UserBean}
     * @memberof Dashboard
     */
    owner?: UserBean;
    /**
     * The number of users who have this dashboard as a favorite.
     * @type {number}
     * @memberof Dashboard
     */
    popularity?: number;
    /**
     * The rank of this dashboard.
     * @type {number}
     * @memberof Dashboard
     */
    rank?: number;
    /**
     * The URL of these dashboard details.
     * @type {string}
     * @memberof Dashboard
     */
    self?: string;
    /**
     * The details of any share permissions for the dashboard.
     * @type {Array<SharePermission>}
     * @memberof Dashboard
     */
    sharePermissions?: Array<SharePermission>;
    /**
     * The URL of the dashboard.
     * @type {string}
     * @memberof Dashboard
     */
    view?: string;
}
/**
 * Details of a dashboard.
 * @export
 * @interface DashboardDetails
 */
export interface DashboardDetails {
    /**
     * The name of the dashboard.
     * @type {string}
     * @memberof DashboardDetails
     */
    name: string;
    /**
     * The description of the dashboard.
     * @type {string}
     * @memberof DashboardDetails
     */
    description?: string;
    /**
     * The details of any share permissions for the dashboard.
     * @type {Array<SharePermission>}
     * @memberof DashboardDetails
     */
    sharePermissions: Array<SharePermission>;
}
/**
 * Details of the scope of the default sharing for new filters and dashboards.
 * @export
 * @interface DefaultShareScope
 */
export interface DefaultShareScope {
    /**
     * The scope of the default sharing for new filters and dashboards:   *  `AUTHENTICATED` Shared with all logged-in users.  *  `GLOBAL` Shared with all logged-in users. This shows as `AUTHENTICATED` in the response.  *  `PRIVATE` Not shared with any users.
     * @type {string}
     * @memberof DefaultShareScope
     */
    scope: DefaultShareScope.ScopeEnum;
}

/**
 * @export
 * @namespace DefaultShareScope
 */
export namespace DefaultShareScope {
    /**
     * @export
     * @enum {string}
     */
    export enum ScopeEnum {
        GLOBAL = <any>'GLOBAL',
        AUTHENTICATED = <any>'AUTHENTICATED',
        PRIVATE = <any>'PRIVATE',
    }
}
/**
 * Details about the default workflow.
 * @export
 * @interface DefaultWorkflow
 */
export interface DefaultWorkflow {
    /**
     * The name of the workflow to set as the default workflow.
     * @type {string}
     * @memberof DefaultWorkflow
     */
    workflow: string;
    /**
     * Whether a draft workflow scheme is created or updated when updating an active workflow scheme. The draft is updated with the new default workflow. Defaults to `false`.
     * @type {boolean}
     * @memberof DefaultWorkflow
     */
    updateDraftIfNeeded?: boolean;
}
/**
 *
 * @export
 * @interface DeleteAndReplaceVersionBean
 */
export interface DeleteAndReplaceVersionBean {
    /**
     * The ID of the version to update `fixVersion` to when the field contains the deleted version.
     * @type {number}
     * @memberof DeleteAndReplaceVersionBean
     */
    moveFixIssuesTo?: number;
    /**
     * The ID of the version to update `affectedVersion` to when the field contains the deleted version.
     * @type {number}
     * @memberof DeleteAndReplaceVersionBean
     */
    moveAffectedIssuesTo?: number;
    /**
     * An array of custom field IDs (`customFieldId`) and version IDs (`moveTo`) to update when the fields contain the deleted version.
     * @type {Array<CustomFieldReplacement>}
     * @memberof DeleteAndReplaceVersionBean
     */
    customFieldReplacementList?: Array<CustomFieldReplacement>;
}
/**
 * Details about a workflow.
 * @export
 * @interface DeprecatedWorkflow
 */
export interface DeprecatedWorkflow {
    /**
     * The name of the workflow.
     * @type {string}
     * @memberof DeprecatedWorkflow
     */
    name?: string;
    /**
     * The description of the workflow.
     * @type {string}
     * @memberof DeprecatedWorkflow
     */
    description?: string;
    /**
     * The datetime the workflow was last modified.
     * @type {string}
     * @memberof DeprecatedWorkflow
     */
    lastModifiedDate?: string;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof DeprecatedWorkflow
     */
    lastModifiedUser?: string;
    /**
     * The account ID of the user that last modified the workflow.
     * @type {string}
     * @memberof DeprecatedWorkflow
     */
    lastModifiedUserAccountId?: string;
    /**
     * The number of steps included in the workflow.
     * @type {number}
     * @memberof DeprecatedWorkflow
     */
    steps?: number;
    /**
     * The scope where this workflow applies
     * @type {Scope}
     * @memberof DeprecatedWorkflow
     */
    scope?: Scope;
    /**
     *
     * @type {boolean}
     * @memberof DeprecatedWorkflow
     */
    _default?: boolean;
}
/**
 * An entity property, for more information see [Entity properties](https://developer.atlassian.com/cloud/jira/platform/jira-entity-properties/).
 * @export
 * @interface EntityProperty
 */
export interface EntityProperty {
    /**
     * The key of the property. Required on create and update.
     * @type {string}
     * @memberof EntityProperty
     */
    key?: string;
    /**
     * The value of the property. Required on create and update.
     * @type {ModelObject}
     * @memberof EntityProperty
     */
    value?: ModelObject;
}
/**
 * Error messages from an operation.
 * @export
 * @interface ErrorCollection
 */
export interface ErrorCollection {
    /**
     * The list of error messages produced by this operation. For example, \"input parameter 'key' must be provided\"
     * @type {Array<string>}
     * @memberof ErrorCollection
     */
    errorMessages?: Array<string>;
    /**
     * The list of errors by parameter returned by the operation. For example,\"projectKey\": \"Project keys must start with an uppercase letter, followed by one or more uppercase alphanumeric characters.\"
     * @type {{ [key: string]: string; }}
     * @memberof ErrorCollection
     */
    errors?: { [key: string]: string };
    /**
     *
     * @type {number}
     * @memberof ErrorCollection
     */
    status?: number;
}
/**
 *
 * @export
 * @interface ErrorMessage
 */
export interface ErrorMessage {
    /**
     * The error message.
     * @type {string}
     * @memberof ErrorMessage
     */
    message: string;
}
/**
 * Details about a notification associated with an event.
 * @export
 * @interface EventNotification
 */
export interface EventNotification {
    /**
     * Expand options that include additional event notification details in the response.
     * @type {string}
     * @memberof EventNotification
     */
    expand?: string;
    /**
     * The ID of the notification.
     * @type {number}
     * @memberof EventNotification
     */
    id?: number;
    /**
     * Identifies the recipients of the notification.
     * @type {string}
     * @memberof EventNotification
     */
    notificationType?: EventNotification.NotificationTypeEnum;
    /**
     * The value of the `notificationType`:   *  `User` The `parameter` is the user account ID.  *  `Group` The `parameter` is the group name.  *  `ProjectRole` The `parameter` is the project role ID.  *  `UserCustomField` The `parameter` is the ID of the custom field.  *  `GroupCustomField` The `parameter` is the ID of the custom field.
     * @type {string}
     * @memberof EventNotification
     */
    parameter?: string;
    /**
     * The specified group.
     * @type {GroupName}
     * @memberof EventNotification
     */
    group?: GroupName;
    /**
     * The custom user or group field.
     * @type {FieldDetails}
     * @memberof EventNotification
     */
    field?: FieldDetails;
    /**
     * The email address.
     * @type {string}
     * @memberof EventNotification
     */
    emailAddress?: string;
    /**
     * The specified project role.
     * @type {ProjectRole}
     * @memberof EventNotification
     */
    projectRole?: ProjectRole;
    /**
     * The specified user.
     * @type {UserDetails}
     * @memberof EventNotification
     */
    user?: UserDetails;
}

/**
 * @export
 * @namespace EventNotification
 */
export namespace EventNotification {
    /**
     * @export
     * @enum {string}
     */
    export enum NotificationTypeEnum {
        CurrentAssignee = <any>'CurrentAssignee',
        Reporter = <any>'Reporter',
        CurrentUser = <any>'CurrentUser',
        ProjectLead = <any>'ProjectLead',
        ComponentLead = <any>'ComponentLead',
        User = <any>'User',
        Group = <any>'Group',
        ProjectRole = <any>'ProjectRole',
        EmailAddress = <any>'EmailAddress',
        AllWatchers = <any>'AllWatchers',
        UserCustomField = <any>'UserCustomField',
        GroupCustomField = <any>'GroupCustomField',
    }
}
/**
 * Details about a failed webhook.
 * @export
 * @interface FailedWebhook
 */
export interface FailedWebhook {
    /**
     * The webhook ID, as sent in the `X-Atlassian-Webhook-Identifier` header with the webhook.
     * @type {string}
     * @memberof FailedWebhook
     */
    id: string;
    /**
     * The webhook body.
     * @type {string}
     * @memberof FailedWebhook
     */
    body?: string;
    /**
     * The original webhook destination.
     * @type {string}
     * @memberof FailedWebhook
     */
    url: string;
    /**
     * The time the webhook was added to the list of failed webhooks (that is, the time of the last failed retry).
     * @type {number}
     * @memberof FailedWebhook
     */
    failureTime: number;
}
/**
 * A page of failed webhooks.
 * @export
 * @interface FailedWebhooks
 */
export interface FailedWebhooks {
    /**
     * The list of webhooks.
     * @type {Array<FailedWebhook>}
     * @memberof FailedWebhooks
     */
    values: Array<FailedWebhook>;
    /**
     * The maximum number of items on the page. If the list of values is shorter than this number, then there are no more pages.
     * @type {number}
     * @memberof FailedWebhooks
     */
    maxResults: number;
    /**
     * The URL to the next page of results. Present only if the request returned at least one result.The next page may be empty at the time of receiving the response, but new failed webhooks may appear in time. You can save the URL to the next page and query for new results periodically (for example, every hour).
     * @type {string}
     * @memberof FailedWebhooks
     */
    next?: string;
}
/**
 * Details of a field.
 * @export
 * @interface Field
 */
export interface Field {
    /**
     * The ID of the field.
     * @type {string}
     * @memberof Field
     */
    id: string;
    /**
     * The name of the field.
     * @type {string}
     * @memberof Field
     */
    name: string;
    /**
     *
     * @type {JsonTypeBean}
     * @memberof Field
     */
    schema?: JsonTypeBean;
    /**
     * The description of the field.
     * @type {string}
     * @memberof Field
     */
    description?: string;
    /**
     * The key of the field.
     * @type {string}
     * @memberof Field
     */
    key?: string;
    /**
     * Whether the field is locked.
     * @type {boolean}
     * @memberof Field
     */
    isLocked?: boolean;
    /**
     * Number of screens where the field is used.
     * @type {number}
     * @memberof Field
     */
    screensCount?: number;
    /**
     * Number of contexts where the field is used.
     * @type {number}
     * @memberof Field
     */
    contextsCount?: number;
    /**
     *
     * @type {FieldLastUsed}
     * @memberof Field
     */
    lastUsed?: FieldLastUsed;
}
/**
 * A clause that asserts whether a field was changed. For example, `status CHANGED AFTER startOfMonth(-1M)`.See [CHANGED](https://confluence.atlassian.com/x/dgiiLQ#Advancedsearching-operatorsreference-CHANGEDCHANGED) for more information about the CHANGED operator.
 * @export
 * @interface FieldChangedClause
 */
export interface FieldChangedClause {
    /**
     *
     * @type {JqlQueryField}
     * @memberof FieldChangedClause
     */
    field: JqlQueryField;
    /**
     * The operator applied to the field.
     * @type {string}
     * @memberof FieldChangedClause
     */
    operator: FieldChangedClause.OperatorEnum;
    /**
     * The list of time predicates.
     * @type {Array<JqlQueryClauseTimePredicate>}
     * @memberof FieldChangedClause
     */
    predicates: Array<JqlQueryClauseTimePredicate>;
}

/**
 * @export
 * @namespace FieldChangedClause
 */
export namespace FieldChangedClause {
    /**
     * @export
     * @enum {string}
     */
    export enum OperatorEnum {
        Changed = <any>'changed',
    }
}
/**
 * Details of a field configuration.
 * @export
 * @interface FieldConfiguration
 */
export interface FieldConfiguration {
    /**
     * The ID of the field configuration.
     * @type {number}
     * @memberof FieldConfiguration
     */
    id: number;
    /**
     * The name of the field configuration.
     * @type {string}
     * @memberof FieldConfiguration
     */
    name: string;
    /**
     * The description of the field configuration.
     * @type {string}
     * @memberof FieldConfiguration
     */
    description: string;
    /**
     * Whether the field configuration is the default.
     * @type {boolean}
     * @memberof FieldConfiguration
     */
    isDefault?: boolean;
}
/**
 * The field configuration for an issue type.
 * @export
 * @interface FieldConfigurationIssueTypeItem
 */
export interface FieldConfigurationIssueTypeItem {
    /**
     * The ID of the field configuration scheme.
     * @type {string}
     * @memberof FieldConfigurationIssueTypeItem
     */
    fieldConfigurationSchemeId: string;
    /**
     * The ID of the issue type or *default*. When set to *default* this field configuration issue type item applies to all issue types without a field configuration.
     * @type {string}
     * @memberof FieldConfigurationIssueTypeItem
     */
    issueTypeId: string;
    /**
     * The ID of the field configuration.
     * @type {string}
     * @memberof FieldConfigurationIssueTypeItem
     */
    fieldConfigurationId: string;
}
/**
 * A field within a field configuration.
 * @export
 * @interface FieldConfigurationItem
 */
export interface FieldConfigurationItem {
    /**
     * The ID of the field within the field configuration.
     * @type {string}
     * @memberof FieldConfigurationItem
     */
    id: string;
    /**
     * The description of the field within the field configuration.
     * @type {string}
     * @memberof FieldConfigurationItem
     */
    description?: string;
    /**
     * Whether the field is hidden in the field configuration.
     * @type {boolean}
     * @memberof FieldConfigurationItem
     */
    isHidden?: boolean;
    /**
     * Whether the field is required in the field configuration.
     * @type {boolean}
     * @memberof FieldConfigurationItem
     */
    isRequired?: boolean;
}
/**
 * Details of a field configuration scheme.
 * @export
 * @interface FieldConfigurationScheme
 */
export interface FieldConfigurationScheme {
    /**
     * The ID of the field configuration scheme.
     * @type {string}
     * @memberof FieldConfigurationScheme
     */
    id: string;
    /**
     * The name of the field configuration scheme.
     * @type {string}
     * @memberof FieldConfigurationScheme
     */
    name: string;
    /**
     * The description of the field configuration scheme.
     * @type {string}
     * @memberof FieldConfigurationScheme
     */
    description?: string;
}
/**
 * Associated field configuration scheme and project.
 * @export
 * @interface FieldConfigurationSchemeProjectAssociation
 */
export interface FieldConfigurationSchemeProjectAssociation {
    /**
     * The ID of the field configuration scheme. If the field configuration scheme ID is `null`, the operation assigns the default field configuration scheme.
     * @type {string}
     * @memberof FieldConfigurationSchemeProjectAssociation
     */
    fieldConfigurationSchemeId?: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof FieldConfigurationSchemeProjectAssociation
     */
    projectId: string;
}
/**
 * Project list with assigned field configuration schema.
 * @export
 * @interface FieldConfigurationSchemeProjects
 */
export interface FieldConfigurationSchemeProjects {
    /**
     *
     * @type {FieldConfigurationScheme}
     * @memberof FieldConfigurationSchemeProjects
     */
    fieldConfigurationScheme?: FieldConfigurationScheme;
    /**
     * The IDs of projects using the field configuration scheme.
     * @type {Array<string>}
     * @memberof FieldConfigurationSchemeProjects
     */
    projectIds: Array<string>;
}
/**
 * Details about a field.
 * @export
 * @interface FieldDetails
 */
export interface FieldDetails {
    /**
     * The ID of the field.
     * @type {string}
     * @memberof FieldDetails
     */
    id?: string;
    /**
     * The key of the field.
     * @type {string}
     * @memberof FieldDetails
     */
    key?: string;
    /**
     * The name of the field.
     * @type {string}
     * @memberof FieldDetails
     */
    name?: string;
    /**
     * Whether the field is a custom field.
     * @type {boolean}
     * @memberof FieldDetails
     */
    custom?: boolean;
    /**
     * Whether the content of the field can be used to order lists.
     * @type {boolean}
     * @memberof FieldDetails
     */
    orderable?: boolean;
    /**
     * Whether the field can be used as a column on the issue navigator.
     * @type {boolean}
     * @memberof FieldDetails
     */
    navigable?: boolean;
    /**
     * Whether the content of the field can be searched.
     * @type {boolean}
     * @memberof FieldDetails
     */
    searchable?: boolean;
    /**
     * The names that can be used to reference the field in an advanced search. For more information, see [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ).
     * @type {Array<string>}
     * @memberof FieldDetails
     */
    clauseNames?: Array<string>;
    /**
     * The scope of the field.
     * @type {Scope}
     * @memberof FieldDetails
     */
    scope?: Scope;
    /**
     * The data schema for the field.
     * @type {JsonTypeBean}
     * @memberof FieldDetails
     */
    schema?: JsonTypeBean;
}
/**
 * Information about the most recent use of a field.
 * @export
 * @interface FieldLastUsed
 */
export interface FieldLastUsed {
    /**
     * Last used value type:   *  *TRACKED*: field is tracked and a last used date is available.  *  *NOT\\_TRACKED*: field is not tracked, last used date is not available.  *  *NO\\_INFORMATION*: field is tracked, but no last used date is available.
     * @type {string}
     * @memberof FieldLastUsed
     */
    type?: FieldLastUsed.TypeEnum;
    /**
     * The date when the value of the field last changed.
     * @type {Date}
     * @memberof FieldLastUsed
     */
    value?: Date;
}

/**
 * @export
 * @namespace FieldLastUsed
 */
export namespace FieldLastUsed {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        TRACKED = <any>'TRACKED',
        NOTTRACKED = <any>'NOT_TRACKED',
        NOINFORMATION = <any>'NO_INFORMATION',
    }
}
/**
 * The metadata describing an issue field.
 * @export
 * @interface FieldMetadata
 */
export interface FieldMetadata {
    /**
     * Whether the field is required.
     * @type {boolean}
     * @memberof FieldMetadata
     */
    required: boolean;
    /**
     * The data type of the field.
     * @type {JsonTypeBean}
     * @memberof FieldMetadata
     */
    schema?: JsonTypeBean;
    /**
     * The name of the field.
     * @type {string}
     * @memberof FieldMetadata
     */
    name: string;
    /**
     * The key of the field.
     * @type {string}
     * @memberof FieldMetadata
     */
    key: string;
    /**
     * The URL that can be used to automatically complete the field.
     * @type {string}
     * @memberof FieldMetadata
     */
    autoCompleteUrl?: string;
    /**
     * Whether the field has a default value.
     * @type {boolean}
     * @memberof FieldMetadata
     */
    hasDefaultValue?: boolean;
    /**
     * The list of operations that can be performed on the field.
     * @type {Array<string>}
     * @memberof FieldMetadata
     */
    operations: Array<string>;
    /**
     * The list of values allowed in the field.
     * @type {Array<ModelObject>}
     * @memberof FieldMetadata
     */
    allowedValues?: Array<ModelObject>;
    /**
     * The default value of the field.
     * @type {ModelObject}
     * @memberof FieldMetadata
     */
    defaultValue?: ModelObject;
}
/**
 * Details of a field that can be used in advanced searches.
 * @export
 * @interface FieldReferenceData
 */
export interface FieldReferenceData {
    /**
     * The field identifier.
     * @type {string}
     * @memberof FieldReferenceData
     */
    value?: string;
    /**
     * The display name of the field.
     * @type {string}
     * @memberof FieldReferenceData
     */
    displayName?: string;
    /**
     * Whether the field can be used in a query's `ORDER BY` clause.
     * @type {string}
     * @memberof FieldReferenceData
     */
    orderable?: FieldReferenceData.OrderableEnum;
    /**
     * Whether the content of this field can be searched.
     * @type {string}
     * @memberof FieldReferenceData
     */
    searchable?: FieldReferenceData.SearchableEnum;
    /**
     * Whether the field provide auto-complete suggestions.
     * @type {string}
     * @memberof FieldReferenceData
     */
    auto?: FieldReferenceData.AutoEnum;
    /**
     * If the item is a custom field, the ID of the custom field.
     * @type {string}
     * @memberof FieldReferenceData
     */
    cfid?: string;
    /**
     * The valid search operators for the field.
     * @type {Array<string>}
     * @memberof FieldReferenceData
     */
    operators?: Array<string>;
    /**
     * The data types of items in the field.
     * @type {Array<string>}
     * @memberof FieldReferenceData
     */
    types?: Array<string>;
}

/**
 * @export
 * @namespace FieldReferenceData
 */
export namespace FieldReferenceData {
    /**
     * @export
     * @enum {string}
     */
    export enum OrderableEnum {
        True = <any>'true',
        False = <any>'false',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum SearchableEnum {
        True = <any>'true',
        False = <any>'false',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum AutoEnum {
        True = <any>'true',
        False = <any>'false',
    }
}
/**
 * Details of an operation to perform on a field.
 * @export
 * @interface FieldUpdateOperation
 */
export interface FieldUpdateOperation {
    /**
     * A map containing the name of a field and the value to add to it.
     * @type {ModelObject}
     * @memberof FieldUpdateOperation
     */
    add?: ModelObject;
    /**
     * A map containing the name of a field and the value to set in it.
     * @type {ModelObject}
     * @memberof FieldUpdateOperation
     */
    set?: ModelObject;
    /**
     * A map containing the name of a field and the value to removed from it.
     * @type {ModelObject}
     * @memberof FieldUpdateOperation
     */
    remove?: ModelObject;
    /**
     * A map containing the name of a field and the value to edit in it.
     * @type {ModelObject}
     * @memberof FieldUpdateOperation
     */
    edit?: ModelObject;
}
/**
 * A clause that asserts the current value of a field. For example, `summary ~ test`.
 * @export
 * @interface FieldValueClause
 */
export interface FieldValueClause {
    /**
     *
     * @type {JqlQueryField}
     * @memberof FieldValueClause
     */
    field: JqlQueryField;
    /**
     * The operator between the field and operand.
     * @type {string}
     * @memberof FieldValueClause
     */
    operator: FieldValueClause.OperatorEnum;
    /**
     *
     * @type {JqlQueryClauseOperand}
     * @memberof FieldValueClause
     */
    operand: JqlQueryClauseOperand;
}

/**
 * @export
 * @namespace FieldValueClause
 */
export namespace FieldValueClause {
    /**
     * @export
     * @enum {string}
     */
    export enum OperatorEnum {
        Equal = <any>'=',
        NotEqual = <any>'!=',
        GreaterThan = <any>'>',
        LessThan = <any>'<',
        GreaterThanOrEqualTo = <any>'>=',
        LessThanOrEqualTo = <any>'<=',
        In = <any>'in',
        NotIn = <any>'not in',
        Tilde = <any>'~',
        TildeEqualTo = <any>'~=',
        Is = <any>'is',
        IsNot = <any>'is not',
    }
}
/**
 * A clause that asserts a previous value of a field. For example, `status WAS \"Resolved\" BY currentUser() BEFORE \"2019/02/02\"`. See [WAS](https://confluence.atlassian.com/x/dgiiLQ#Advancedsearching-operatorsreference-WASWAS) for more information about the WAS operator.
 * @export
 * @interface FieldWasClause
 */
export interface FieldWasClause {
    /**
     *
     * @type {JqlQueryField}
     * @memberof FieldWasClause
     */
    field: JqlQueryField;
    /**
     * The operator between the field and operand.
     * @type {string}
     * @memberof FieldWasClause
     */
    operator: FieldWasClause.OperatorEnum;
    /**
     *
     * @type {JqlQueryClauseOperand}
     * @memberof FieldWasClause
     */
    operand: JqlQueryClauseOperand;
    /**
     * The list of time predicates.
     * @type {Array<JqlQueryClauseTimePredicate>}
     * @memberof FieldWasClause
     */
    predicates: Array<JqlQueryClauseTimePredicate>;
}

/**
 * @export
 * @namespace FieldWasClause
 */
export namespace FieldWasClause {
    /**
     * @export
     * @enum {string}
     */
    export enum OperatorEnum {
        Was = <any>'was',
        WasIn = <any>'was in',
        WasNotIn = <any>'was not in',
        WasNot = <any>'was not',
    }
}
/**
 * Key fields from the linked issue.
 * @export
 * @interface Fields
 */
export interface Fields {
    /**
     * The summary description of the linked issue.
     * @type {string}
     * @memberof Fields
     */
    summary?: string;
    /**
     * The status of the linked issue.
     * @type {StatusDetails}
     * @memberof Fields
     */
    status?: StatusDetails;
    /**
     * The priority of the linked issue.
     * @type {Priority}
     * @memberof Fields
     */
    priority?: Priority;
    /**
     * The assignee of the linked issue.
     * @type {UserDetails}
     * @memberof Fields
     */
    assignee?: UserDetails;
    /**
     * The time tracking of the linked issue.
     * @type {TimeTrackingDetails}
     * @memberof Fields
     */
    timetracking?: TimeTrackingDetails;
    /**
     *
     * @type {IssueTypeDetails}
     * @memberof Fields
     */
    issuetype?: IssueTypeDetails;
    /**
     * The type of the linked issue.
     * @type {IssueTypeDetails}
     * @memberof Fields
     */
    issueType?: IssueTypeDetails;
}
/**
 * Details about a filter.
 * @export
 * @interface Filter
 */
export interface Filter {
    /**
     * The URL of the filter.
     * @type {string}
     * @memberof Filter
     */
    self?: string;
    /**
     * The unique identifier for the filter.
     * @type {string}
     * @memberof Filter
     */
    id?: string;
    /**
     * The name of the filter. Must be unique.
     * @type {string}
     * @memberof Filter
     */
    name: string;
    /**
     * A description of the filter.
     * @type {string}
     * @memberof Filter
     */
    description?: string;
    /**
     * The user who owns the filter. This is defaulted to the creator of the filter, however Jira administrators can change the owner of a shared filter in the admin settings.
     * @type {User}
     * @memberof Filter
     */
    owner?: User;
    /**
     * The JQL query for the filter. For example, *project = SSP AND issuetype = Bug*.
     * @type {string}
     * @memberof Filter
     */
    jql?: string;
    /**
     * A URL to view the filter results in Jira, using the ID of the filter. For example, *https://your-domain.atlassian.net/issues/?filter=10100*.
     * @type {string}
     * @memberof Filter
     */
    viewUrl?: string;
    /**
     * A URL to view the filter results in Jira, using the [Search for issues using JQL](#api-rest-api-3-filter-search-get) operation with the filter's JQL string to return the filter results. For example, *https://your-domain.atlassian.net/rest/api/3/search?jql=project+%3D+SSP+AND+issuetype+%3D+Bug*.
     * @type {string}
     * @memberof Filter
     */
    searchUrl?: string;
    /**
     * Whether the filter is selected as a favorite.
     * @type {boolean}
     * @memberof Filter
     */
    favourite?: boolean;
    /**
     * The count of how many users have selected this filter as a favorite, including the filter owner.
     * @type {number}
     * @memberof Filter
     */
    favouritedCount?: number;
    /**
     * The groups and projects that the filter is shared with.
     * @type {Array<SharePermission>}
     * @memberof Filter
     */
    sharePermissions?: Array<SharePermission>;
    /**
     * A paginated list of the users that the filter is shared with. This includes users that are members of the groups or can browse the projects that the filter is shared with.
     * @type {UserList}
     * @memberof Filter
     */
    sharedUsers?: UserList;
    /**
     * A paginated list of the users that are subscribed to the filter.
     * @type {FilterSubscriptionsList}
     * @memberof Filter
     */
    subscriptions?: FilterSubscriptionsList;
}
/**
 * Details of a filter.
 * @export
 * @interface FilterDetails
 */
export interface FilterDetails {
    /**
     * The URL of the filter.
     * @type {string}
     * @memberof FilterDetails
     */
    self?: string;
    /**
     * The unique identifier for the filter.
     * @type {string}
     * @memberof FilterDetails
     */
    id?: string;
    /**
     * The name of the filter. Must be unique.
     * @type {string}
     * @memberof FilterDetails
     */
    name: string;
    /**
     * A description of the filter.
     * @type {string}
     * @memberof FilterDetails
     */
    description?: string;
    /**
     * The user who owns the filter. This is defaulted to the creator of the filter, however Jira administrators can change the owner of a shared filter in the admin settings.
     * @type {User}
     * @memberof FilterDetails
     */
    owner?: User;
    /**
     * The JQL query for the filter. For example, *project = SSP AND issuetype = Bug*.
     * @type {string}
     * @memberof FilterDetails
     */
    jql?: string;
    /**
     * A URL to view the filter results in Jira, using the ID of the filter. For example, *https://your-domain.atlassian.net/issues/?filter=10100*.
     * @type {string}
     * @memberof FilterDetails
     */
    viewUrl?: string;
    /**
     * A URL to view the filter results in Jira, using the [Search for issues using JQL](#api-rest-api-3-filter-search-get) operation with the filter's JQL string to return the filter results. For example, *https://your-domain.atlassian.net/rest/api/3/search?jql=project+%3D+SSP+AND+issuetype+%3D+Bug*.
     * @type {string}
     * @memberof FilterDetails
     */
    searchUrl?: string;
    /**
     * Whether the filter is selected as a favorite by any users, not including the filter owner.
     * @type {boolean}
     * @memberof FilterDetails
     */
    favourite?: boolean;
    /**
     * The count of how many users have selected this filter as a favorite, including the filter owner.
     * @type {number}
     * @memberof FilterDetails
     */
    favouritedCount?: number;
    /**
     * The groups and projects that the filter is shared with. This can be specified when updating a filter, but not when creating a filter.
     * @type {Array<SharePermission>}
     * @memberof FilterDetails
     */
    sharePermissions?: Array<SharePermission>;
    /**
     * The users that are subscribed to the filter.
     * @type {Array<FilterSubscription>}
     * @memberof FilterDetails
     */
    subscriptions?: Array<FilterSubscription>;
}
/**
 * Details of a user or group subscribing to a filter.
 * @export
 * @interface FilterSubscription
 */
export interface FilterSubscription {
    /**
     * The ID of the filter subscription.
     * @type {number}
     * @memberof FilterSubscription
     */
    id?: number;
    /**
     * The user subscribing to filter.
     * @type {User}
     * @memberof FilterSubscription
     */
    user?: User;
    /**
     * The group subscribing to filter.
     * @type {GroupName}
     * @memberof FilterSubscription
     */
    group?: GroupName;
}
/**
 * A paginated list of subscriptions to a filter.
 * @export
 * @interface FilterSubscriptionsList
 */
export interface FilterSubscriptionsList {
    /**
     * The number of items on the page.
     * @type {number}
     * @memberof FilterSubscriptionsList
     */
    size?: number;
    /**
     * The list of items.
     * @type {Array<FilterSubscription>}
     * @memberof FilterSubscriptionsList
     */
    items?: Array<FilterSubscription>;
    /**
     * The maximum number of results that could be on the page.
     * @type {number}
     * @memberof FilterSubscriptionsList
     */
    maxResults?: number;
    /**
     * The index of the first item returned on the page.
     * @type {number}
     * @memberof FilterSubscriptionsList
     */
    startIndex?: number;
    /**
     * The index of the last item returned on the page.
     * @type {number}
     * @memberof FilterSubscriptionsList
     */
    endIndex?: number;
}
/**
 * A group found in a search.
 * @export
 * @interface FoundGroup
 */
export interface FoundGroup {
    /**
     * The name of the group.
     * @type {string}
     * @memberof FoundGroup
     */
    name?: string;
    /**
     * The group name with the matched query string highlighted with the HTML bold tag.
     * @type {string}
     * @memberof FoundGroup
     */
    html?: string;
    /**
     *
     * @type {Array<GroupLabel>}
     * @memberof FoundGroup
     */
    labels?: Array<GroupLabel>;
    /**
     * The ID of the group, if available, which uniquely identifies the group across all Atlassian products. For example, *952d12c3-5b5b-4d04-bb32-44d383afc4b2*.
     * @type {string}
     * @memberof FoundGroup
     */
    groupId?: string;
}
/**
 * The list of groups found in a search, including header text (Showing X of Y matching groups) and total of matched groups.
 * @export
 * @interface FoundGroups
 */
export interface FoundGroups {
    /**
     * Header text indicating the number of groups in the response and the total number of groups found in the search.
     * @type {string}
     * @memberof FoundGroups
     */
    header?: string;
    /**
     * The total number of groups found in the search.
     * @type {number}
     * @memberof FoundGroups
     */
    total?: number;
    /**
     *
     * @type {Array<FoundGroup>}
     * @memberof FoundGroups
     */
    groups?: Array<FoundGroup>;
}
/**
 * The list of users found in a search, including header text (Showing X of Y matching users) and total of matched users.
 * @export
 * @interface FoundUsers
 */
export interface FoundUsers {
    /**
     *
     * @type {Array<UserPickerUser>}
     * @memberof FoundUsers
     */
    users?: Array<UserPickerUser>;
    /**
     * The total number of users found in the search.
     * @type {number}
     * @memberof FoundUsers
     */
    total?: number;
    /**
     * Header text indicating the number of users in the response and the total number of users found in the search.
     * @type {string}
     * @memberof FoundUsers
     */
    header?: string;
}
/**
 * List of users and groups found in a search.
 * @export
 * @interface FoundUsersAndGroups
 */
export interface FoundUsersAndGroups {
    /**
     *
     * @type {FoundUsers}
     * @memberof FoundUsersAndGroups
     */
    users?: FoundUsers;
    /**
     *
     * @type {FoundGroups}
     * @memberof FoundUsersAndGroups
     */
    groups?: FoundGroups;
}
/**
 * An operand that is a function. See [Advanced searching - functions reference](https://confluence.atlassian.com/x/dwiiLQ) for more information about JQL functions.
 * @export
 * @interface FunctionOperand
 */
export interface FunctionOperand {
    /**
     * The name of the function.
     * @type {string}
     * @memberof FunctionOperand
     */
    _function: string;
    /**
     * The list of function arguments.
     * @type {Array<string>}
     * @memberof FunctionOperand
     */
    arguments?: Array<string>;
}
/**
 * Details of functions that can be used in advanced searches.
 * @export
 * @interface FunctionReferenceData
 */
export interface FunctionReferenceData {
    /**
     * The function identifier.
     * @type {string}
     * @memberof FunctionReferenceData
     */
    value?: string;
    /**
     * The display name of the function.
     * @type {string}
     * @memberof FunctionReferenceData
     */
    displayName?: string;
    /**
     * Whether the function can take a list of arguments.
     * @type {string}
     * @memberof FunctionReferenceData
     */
    isList?: FunctionReferenceData.IsListEnum;
    /**
     * The data types returned by the function.
     * @type {Array<string>}
     * @memberof FunctionReferenceData
     */
    types?: Array<string>;
}

/**
 * @export
 * @namespace FunctionReferenceData
 */
export namespace FunctionReferenceData {
    /**
     * @export
     * @enum {string}
     */
    export enum IsListEnum {
        True = <any>'true',
        False = <any>'false',
    }
}
/**
 *
 * @export
 * @interface GlobalScopeBean
 */
export interface GlobalScopeBean {
    /**
     * Defines the behavior of the option in the global context.If notSelectable is set, the option cannot be set as the field's value. This is useful for archiving an option that has previously been selected but shouldn't be used anymore.If defaultValue is set, the option is selected by default.
     * @type {Array<string>}
     * @memberof GlobalScopeBean
     */
    attributes?: Array<GlobalScopeBean.AttributesEnum>;
}

/**
 * @export
 * @namespace GlobalScopeBean
 */
export namespace GlobalScopeBean {
    /**
     * @export
     * @enum {string}
     */
    export enum AttributesEnum {
        NotSelectable = <any>'notSelectable',
        DefaultValue = <any>'defaultValue',
    }
}
/**
 *
 * @export
 * @interface Group
 */
export interface Group {
    /**
     * The name of group.
     * @type {string}
     * @memberof Group
     */
    name?: string;
    /**
     * The URL for these group details.
     * @type {string}
     * @memberof Group
     */
    self?: string;
    /**
     * A paginated list of the users that are members of the group. A maximum of 50 users is returned in the list, to access additional users append `[start-index:end-index]` to the expand request. For example, to access the next 50 users, use`?expand=users[51:100]`.
     * @type {PagedListUserDetailsApplicationUser}
     * @memberof Group
     */
    users?: PagedListUserDetailsApplicationUser;
    /**
     * Expand options that include additional group details in the response.
     * @type {string}
     * @memberof Group
     */
    expand?: string;
}
/**
 * Details about a group.
 * @export
 * @interface GroupDetails
 */
export interface GroupDetails {
    /**
     * The name of the group.
     * @type {string}
     * @memberof GroupDetails
     */
    name?: string;
    /**
     * The ID of the group, if available, which uniquely identifies the group across all Atlassian products. For example, *952d12c3-5b5b-4d04-bb32-44d383afc4b2*.
     * @type {string}
     * @memberof GroupDetails
     */
    groupId?: string;
}
/**
 * A group label.
 * @export
 * @interface GroupLabel
 */
export interface GroupLabel {
    /**
     * The group label name.
     * @type {string}
     * @memberof GroupLabel
     */
    text?: string;
    /**
     * The title of the group label.
     * @type {string}
     * @memberof GroupLabel
     */
    title?: string;
    /**
     * The type of the group label.
     * @type {string}
     * @memberof GroupLabel
     */
    type?: GroupLabel.TypeEnum;
}

/**
 * @export
 * @namespace GroupLabel
 */
export namespace GroupLabel {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        ADMIN = <any>'ADMIN',
        SINGLE = <any>'SINGLE',
        MULTIPLE = <any>'MULTIPLE',
    }
}
/**
 * Details about a group name.
 * @export
 * @interface GroupName
 */
export interface GroupName {
    /**
     * The name of group.
     * @type {string}
     * @memberof GroupName
     */
    name?: string;
    /**
     * The URL for these group details.
     * @type {string}
     * @memberof GroupName
     */
    self?: string;
}
/**
 * Jira instance health check results. Deprecated and no longer returned.
 * @export
 * @interface HealthCheckResult
 */
export interface HealthCheckResult {
    /**
     * The name of the Jira health check item.
     * @type {string}
     * @memberof HealthCheckResult
     */
    name?: string;
    /**
     * The description of the Jira health check item.
     * @type {string}
     * @memberof HealthCheckResult
     */
    description?: string;
    /**
     * Whether the Jira health check item passed or failed.
     * @type {boolean}
     * @memberof HealthCheckResult
     */
    passed?: boolean;
}
/**
 * Project Issue Type Hierarchy
 * @export
 * @interface Hierarchy
 */
export interface Hierarchy {
    /**
     *
     * @type {Array<HierarchyLevel>}
     * @memberof Hierarchy
     */
    level?: Array<HierarchyLevel>;
}
/**
 *
 * @export
 * @interface HierarchyLevel
 */
export interface HierarchyLevel {
    /**
     *
     * @type {number}
     * @memberof HierarchyLevel
     */
    id?: number;
    /**
     *
     * @type {string}
     * @memberof HierarchyLevel
     */
    name?: string;
    /**
     *
     * @type {number}
     * @memberof HierarchyLevel
     */
    aboveLevelId?: number;
    /**
     *
     * @type {number}
     * @memberof HierarchyLevel
     */
    belowLevelId?: number;
    /**
     *
     * @type {number}
     * @memberof HierarchyLevel
     */
    projectConfigurationId?: number;
    /**
     *
     * @type {Array<number>}
     * @memberof HierarchyLevel
     */
    issueTypeIds?: Array<number>;
    /**
     *
     * @type {string}
     * @memberof HierarchyLevel
     */
    externalUuid?: string;
}
/**
 * Details of issue history metadata.
 * @export
 * @interface HistoryMetadata
 */
export interface HistoryMetadata {
    [key: string]: any;
}
/**
 * Details of user or system associated with a issue history metadata item.
 * @export
 * @interface HistoryMetadataParticipant
 */
export interface HistoryMetadataParticipant {
    [key: string]: any;
}
/**
 * An icon. If no icon is defined:   *  for a status icon, no status icon displays in Jira.  *  for the remote object icon, the default link icon displays in Jira.
 * @export
 * @interface Icon
 */
export interface Icon {
    [key: string]: any;
}
/**
 * An icon.
 * @export
 * @interface IconBean
 */
export interface IconBean {
    /**
     * The URL of a 16x16 pixel icon.
     * @type {string}
     * @memberof IconBean
     */
    url16x16?: string;
    /**
     * The title of the icon, for use as a tooltip on the icon.
     * @type {string}
     * @memberof IconBean
     */
    title?: string;
    /**
     * The URL of the tooltip, used only for a status icon.
     * @type {string}
     * @memberof IconBean
     */
    link?: string;
}
/**
 *
 * @export
 * @interface IdBean
 */
export interface IdBean {
    /**
     * The ID of the permission scheme to associate with the project. Use the [Get all permission schemes](#api-rest-api-3-permissionscheme-get) resource to get a list of permission scheme IDs.
     * @type {number}
     * @memberof IdBean
     */
    id: number;
}
/**
 *
 * @export
 * @interface IdOrKeyBean
 */
export interface IdOrKeyBean {
    /**
     * The ID of the referenced item.
     * @type {number}
     * @memberof IdOrKeyBean
     */
    id?: number;
    /**
     * The key of the referenced item.
     * @type {string}
     * @memberof IdOrKeyBean
     */
    key?: string;
}
/**
 *
 * @export
 * @interface IncludedFields
 */
export interface IncludedFields {
    /**
     *
     * @type {Array<string>}
     * @memberof IncludedFields
     */
    actuallyIncluded?: Array<string>;
    /**
     *
     * @type {Array<string>}
     * @memberof IncludedFields
     */
    excluded?: Array<string>;
    /**
     *
     * @type {Array<string>}
     * @memberof IncludedFields
     */
    included?: Array<string>;
}

export interface JiraProjectBean {
    id: number;
    key: string;
    name: string;
    projectTypeKey: string;
    avatarUrls: {
        '48x48': string;
    };
}

/**
 *
 * @export
 * @interface IssueBean
 */
export interface IssueBean {
    /**
     * Expand options that include additional issue details in the response.
     * @type {string}
     * @memberof IssueBean
     */
    expand?: string;
    /**
     * The ID of the issue.
     * @type {string}
     * @memberof IssueBean
     */
    id?: string;
    /**
     * The URL of the issue details.
     * @type {string}
     * @memberof IssueBean
     */
    self?: string;
    /**
     * The key of the issue.
     * @type {string}
     * @memberof IssueBean
     */
    key?: string;
    /**
     * The rendered value of each field present on the issue.
     * @type {{ [key: string]: ModelObject; }}
     * @memberof IssueBean
     */
    renderedFields?: { [key: string]: ModelObject };
    /**
     * Details of the issue properties identified in the request.
     * @type {{ [key: string]: ModelObject; }}
     * @memberof IssueBean
     */
    properties?: { [key: string]: ModelObject };
    /**
     * The ID and name of each field present on the issue.
     * @type {{ [key: string]: string; }}
     * @memberof IssueBean
     */
    names?: { [key: string]: string };
    /**
     * The schema describing each field present on the issue.
     * @type {{ [key: string]: JsonTypeBean; }}
     * @memberof IssueBean
     */
    schema?: { [key: string]: JsonTypeBean };
    /**
     * The transitions that can be performed on the issue.
     * @type {Array<IssueTransition>}
     * @memberof IssueBean
     */
    transitions?: Array<IssueTransition>;
    /**
     * The operations that can be performed on the issue.
     * @type {Operations}
     * @memberof IssueBean
     */
    operations?: Operations;
    /**
     * The metadata for the fields on the issue that can be amended.
     * @type {IssueUpdateMetadata}
     * @memberof IssueBean
     */
    editmeta?: IssueUpdateMetadata;
    /**
     * Details of changelogs associated with the issue.
     * @type {PageOfChangelogs}
     * @memberof IssueBean
     */
    changelog?: PageOfChangelogs;
    /**
     * The versions of each field on the issue.
     * @type {{ [key: string]: { [key: string]: ModelObject; }; }}
     * @memberof IssueBean
     */
    versionedRepresentations?: { [key: string]: { [key: string]: ModelObject } };
    /**
     *
     * @type {IncludedFields}
     * @memberof IssueBean
     */
    fieldsToInclude?: IncludedFields;
    /**
     *
     * @type {{ [key: string]: ModelObject; }}
     * @memberof IssueBean
     */
    fields?: { [key: string]: ModelObject };
}
/**
 *
 * @export
 * @interface IssueCommentListRequestBean
 */
export interface IssueCommentListRequestBean {
    /**
     * The list of comment IDs. A maximum of 1000 IDs can be specified.
     * @type {Array<number>}
     * @memberof IssueCommentListRequestBean
     */
    ids: Array<number>;
}
/**
 * The wrapper for the issue creation metadata for a list of projects.
 * @export
 * @interface IssueCreateMetadata
 */
export interface IssueCreateMetadata {
    /**
     * Expand options that include additional project details in the response.
     * @type {string}
     * @memberof IssueCreateMetadata
     */
    expand?: string;
    /**
     * List of projects and their issue creation metadata.
     * @type {Array<ProjectIssueCreateMetadata>}
     * @memberof IssueCreateMetadata
     */
    projects?: Array<ProjectIssueCreateMetadata>;
}
/**
 * Lists of issues and entity properties. See [Entity properties](https://developer.atlassian.com/cloud/jira/platform/jira-entity-properties/) for more information.
 * @export
 * @interface IssueEntityProperties
 */
export interface IssueEntityProperties {
    /**
     * A list of entity property IDs.
     * @type {Array<number>}
     * @memberof IssueEntityProperties
     */
    entitiesIds?: Array<number>;
    /**
     * A list of entity property keys and values.
     * @type {{ [key: string]: JsonNode; }}
     * @memberof IssueEntityProperties
     */
    properties?: { [key: string]: JsonNode };
}
/**
 * Details of the options for a select list issue field.
 * @export
 * @interface IssueFieldOption
 */
export interface IssueFieldOption {
    /**
     * The unique identifier for the option. This is only unique within the select field's set of options.
     * @type {number}
     * @memberof IssueFieldOption
     */
    id: number;
    /**
     * The option's name, which is displayed in Jira.
     * @type {string}
     * @memberof IssueFieldOption
     */
    value: string;
    /**
     * The properties of the object, as arbitrary key-value pairs. These properties can be searched using JQL, if the extractions (see [Issue Field Option Property Index](https://developer.atlassian.com/cloud/jira/platform/modules/issue-field-option-property-index/)) are defined in the descriptor for the issue field module.
     * @type {{ [key: string]: ModelObject; }}
     * @memberof IssueFieldOption
     */
    properties?: { [key: string]: ModelObject };
    /**
     *
     * @type {IssueFieldOptionConfiguration}
     * @memberof IssueFieldOption
     */
    config?: IssueFieldOptionConfiguration;
}
/**
 * Details of the projects the option is available in.
 * @export
 * @interface IssueFieldOptionConfiguration
 */
export interface IssueFieldOptionConfiguration {
    /**
     * Defines the projects that the option is available in. If the scope is not defined, then the option is available in all projects.
     * @type {IssueFieldOptionScopeBean}
     * @memberof IssueFieldOptionConfiguration
     */
    scope?: IssueFieldOptionScopeBean;
    /**
     * DEPRECATED
     * @type {Array<string>}
     * @memberof IssueFieldOptionConfiguration
     */
    attributes?: Array<IssueFieldOptionConfiguration.AttributesEnum>;
}

/**
 * @export
 * @namespace IssueFieldOptionConfiguration
 */
export namespace IssueFieldOptionConfiguration {
    /**
     * @export
     * @enum {string}
     */
    export enum AttributesEnum {
        NotSelectable = <any>'notSelectable',
        DefaultValue = <any>'defaultValue',
    }
}
/**
 *
 * @export
 * @interface IssueFieldOptionCreateBean
 */
export interface IssueFieldOptionCreateBean {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface IssueFieldOptionScopeBean
 */
export interface IssueFieldOptionScopeBean {
    /**
     * DEPRECATED
     * @type {Array<number>}
     * @memberof IssueFieldOptionScopeBean
     */
    projects?: Array<number>;
    /**
     * Defines the projects in which the option is available and the behavior of the option within each project. Specify one object per project. The behavior of the option in a project context overrides the behavior in the global context.
     * @type {Array<ProjectScopeBean>}
     * @memberof IssueFieldOptionScopeBean
     */
    projects2?: Array<ProjectScopeBean>;
    /**
     * Defines the behavior of the option within the global context. If this property is set, even if set to an empty object, then the option is available in all projects.
     * @type {GlobalScopeBean}
     * @memberof IssueFieldOptionScopeBean
     */
    global?: GlobalScopeBean;
}
/**
 * Bulk operation filter details.
 * @export
 * @interface IssueFilterForBulkPropertyDelete
 */
export interface IssueFilterForBulkPropertyDelete {
    /**
     * List of issues to perform the bulk delete operation on.
     * @type {Array<number>}
     * @memberof IssueFilterForBulkPropertyDelete
     */
    entityIds?: Array<number>;
    /**
     * The value of properties to perform the bulk operation on.
     * @type {ModelObject}
     * @memberof IssueFilterForBulkPropertyDelete
     */
    currentValue?: ModelObject;
}
/**
 * Bulk operation filter details.
 * @export
 * @interface IssueFilterForBulkPropertySet
 */
export interface IssueFilterForBulkPropertySet {
    /**
     * List of issues to perform the bulk operation on.
     * @type {Array<number>}
     * @memberof IssueFilterForBulkPropertySet
     */
    entityIds?: Array<number>;
    /**
     * The value of properties to perform the bulk operation on.
     * @type {ModelObject}
     * @memberof IssueFilterForBulkPropertySet
     */
    currentValue?: ModelObject;
    /**
     * Whether the bulk operation occurs only when the property is present on or absent from an issue.
     * @type {boolean}
     * @memberof IssueFilterForBulkPropertySet
     */
    hasProperty?: boolean;
}
/**
 * Details of a link between issues.
 * @export
 * @interface IssueLink
 */
export interface IssueLink {
    /**
     * The ID of the issue link.
     * @type {string}
     * @memberof IssueLink
     */
    id?: string;
    /**
     * The URL of the issue link.
     * @type {string}
     * @memberof IssueLink
     */
    self?: string;
    /**
     * The type of link between the issues.
     * @type {IssueLinkType}
     * @memberof IssueLink
     */
    type?: IssueLinkType;
    /**
     * The issue the link joins to.
     * @type {LinkedIssue}
     * @memberof IssueLink
     */
    inwardIssue?: LinkedIssue;
    /**
     * The issue the link originates from.
     * @type {LinkedIssue}
     * @memberof IssueLink
     */
    outwardIssue?: LinkedIssue;
}
/**
 * This object is used as follows:   *  In the [ issueLink](#api-rest-api-3-issueLink-post) resource it defines and reports on the type of link between the issues. Find a list of issue link types with [Get issue link types](#api-rest-api-3-issueLinkType-get).  *  In the [ issueLinkType](#api-rest-api-3-issueLinkType-post) resource it defines and reports on issue link types.
 * @export
 * @interface IssueLinkType
 */
export interface IssueLinkType {
    /**
     * The ID of the issue link type and is used as follows:   *  In the [ issueLink](#api-rest-api-3-issueLink-post) resource it is the type of issue link. Required on create when `name` isn't provided. Otherwise, read only.  *  In the [ issueLinkType](#api-rest-api-3-issueLinkType-post) resource it is read only.
     * @type {string}
     * @memberof IssueLinkType
     */
    id?: string;
    /**
     * The name of the issue link type and is used as follows:   *  In the [ issueLink](#api-rest-api-3-issueLink-post) resource it is the type of issue link. Required on create when `id` isn't provided. Otherwise, read only.  *  In the [ issueLinkType](#api-rest-api-3-issueLinkType-post) resource it is required on create and optional on update. Otherwise, read only.
     * @type {string}
     * @memberof IssueLinkType
     */
    name?: string;
    /**
     * The description of the issue link type inward link and is used as follows:   *  In the [ issueLink](#api-rest-api-3-issueLink-post) resource it is read only.  *  In the [ issueLinkType](#api-rest-api-3-issueLinkType-post) resource it is required on create and optional on update. Otherwise, read only.
     * @type {string}
     * @memberof IssueLinkType
     */
    inward?: string;
    /**
     * The description of the issue link type outward link and is used as follows:   *  In the [ issueLink](#api-rest-api-3-issueLink-post) resource it is read only.  *  In the [ issueLinkType](#api-rest-api-3-issueLinkType-post) resource it is required on create and optional on update. Otherwise, read only.
     * @type {string}
     * @memberof IssueLinkType
     */
    outward?: string;
    /**
     * The URL of the issue link type. Read only.
     * @type {string}
     * @memberof IssueLinkType
     */
    self?: string;
}
/**
 * A list of issue link type beans.
 * @export
 * @interface IssueLinkTypes
 */
export interface IssueLinkTypes {
    /**
     * The issue link type bean.
     * @type {Array<IssueLinkType>}
     * @memberof IssueLinkTypes
     */
    issueLinkTypes?: Array<IssueLinkType>;
}
/**
 * A list of matched issues or errors for each JQL query, in the order the JQL queries were passed.
 * @export
 * @interface IssueMatches
 */
export interface IssueMatches {
    /**
     *
     * @type {Array<IssueMatchesForJQL>}
     * @memberof IssueMatches
     */
    matches: Array<IssueMatchesForJQL>;
}
/**
 * A list of the issues matched to a JQL query or details of errors encountered during matching.
 * @export
 * @interface IssueMatchesForJQL
 */
export interface IssueMatchesForJQL {
    /**
     * A list of issue IDs.
     * @type {Array<number>}
     * @memberof IssueMatchesForJQL
     */
    matchedIssues: Array<number>;
    /**
     * A list of errors.
     * @type {Array<string>}
     * @memberof IssueMatchesForJQL
     */
    errors: Array<string>;
}
/**
 * A list of issues suggested for use in auto-completion.
 * @export
 * @interface IssuePickerSuggestions
 */
export interface IssuePickerSuggestions {
    /**
     * A list of issues for an issue type suggested for use in auto-completion.
     * @type {Array<IssuePickerSuggestionsIssueType>}
     * @memberof IssuePickerSuggestions
     */
    sections?: Array<IssuePickerSuggestionsIssueType>;
}
/**
 * A type of issue suggested for use in auto-completion.
 * @export
 * @interface IssuePickerSuggestionsIssueType
 */
export interface IssuePickerSuggestionsIssueType {
    /**
     * The label of the type of issues suggested for use in auto-completion.
     * @type {string}
     * @memberof IssuePickerSuggestionsIssueType
     */
    label?: string;
    /**
     * If issue suggestions are found, returns a message indicating the number of issues suggestions found and returned.
     * @type {string}
     * @memberof IssuePickerSuggestionsIssueType
     */
    sub?: string;
    /**
     * The ID of the type of issues suggested for use in auto-completion.
     * @type {string}
     * @memberof IssuePickerSuggestionsIssueType
     */
    id?: string;
    /**
     * If no issue suggestions are found, returns a message indicating no suggestions were found,
     * @type {string}
     * @memberof IssuePickerSuggestionsIssueType
     */
    msg?: string;
    /**
     * A list of issues suggested for use in auto-completion.
     * @type {Array<SuggestedIssue>}
     * @memberof IssuePickerSuggestionsIssueType
     */
    issues?: Array<SuggestedIssue>;
}
/**
 * Issue security level member.
 * @export
 * @interface IssueSecurityLevelMember
 */
export interface IssueSecurityLevelMember {
    /**
     * The ID of the issue security level member.
     * @type {number}
     * @memberof IssueSecurityLevelMember
     */
    id: number;
    /**
     * The ID of the issue security level.
     * @type {number}
     * @memberof IssueSecurityLevelMember
     */
    issueSecurityLevelId: number;
    /**
     * The user or group being granted the permission. It consists of a `type` and a type-dependent `parameter`. See [Holder object](#holder-object) in *Get all permission schemes* for more information.
     * @type {PermissionHolder}
     * @memberof IssueSecurityLevelMember
     */
    holder?: PermissionHolder;
}
/**
 * Details of an issue transition.
 * @export
 * @interface IssueTransition
 */
export interface IssueTransition {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface IssueTypeCreateBean
 */
export interface IssueTypeCreateBean {
    /**
     * The unique name for the issue type. The maximum length is 60 characters.
     * @type {string}
     * @memberof IssueTypeCreateBean
     */
    name: string;
    /**
     * The description of the issue type.
     * @type {string}
     * @memberof IssueTypeCreateBean
     */
    description?: string;
    /**
     * Whether the issue type is `subtype` or `standard`. Defaults to `standard`.
     * @type {string}
     * @memberof IssueTypeCreateBean
     */
    type?: IssueTypeCreateBean.TypeEnum;
}

/**
 * @export
 * @namespace IssueTypeCreateBean
 */
export namespace IssueTypeCreateBean {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Subtask = <any>'subtask',
        Standard = <any>'standard',
    }
}
/**
 * Details about an issue type.
 * @export
 * @interface IssueTypeDetails
 */
export interface IssueTypeDetails {
    /**
     * The URL of these issue type details.
     * @type {string}
     * @memberof IssueTypeDetails
     */
    self?: string;
    /**
     * The ID of the issue type.
     * @type {string}
     * @memberof IssueTypeDetails
     */
    id?: string;
    /**
     * The description of the issue type.
     * @type {string}
     * @memberof IssueTypeDetails
     */
    description?: string;
    /**
     * The URL of the issue type's avatar.
     * @type {string}
     * @memberof IssueTypeDetails
     */
    iconUrl?: string;
    /**
     * The name of the issue type.
     * @type {string}
     * @memberof IssueTypeDetails
     */
    name?: string;
    /**
     * Whether this issue type is used to create subtasks.
     * @type {boolean}
     * @memberof IssueTypeDetails
     */
    subtask?: boolean;
    /**
     * The ID of the issue type's avatar.
     * @type {number}
     * @memberof IssueTypeDetails
     */
    avatarId?: number;
    /**
     * Unique ID for next-gen projects.
     * @type {string}
     * @memberof IssueTypeDetails
     */
    entityId?: string;
    /**
     * Details of the next-gen projects the issue type is available in.
     * @type {Scope}
     * @memberof IssueTypeDetails
     */
    scope?: Scope;
}
/**
 * A list of issue type IDs.
 * @export
 * @interface IssueTypeIDs
 */
export interface IssueTypeIDs {
    /**
     * List of issue type IDs.
     * @type {Array<string>}
     * @memberof IssueTypeIDs
     */
    issueTypeIds: Array<string>;
}
/**
 * Details of an issue type.
 * @export
 * @interface IssueTypeInfo
 */
export interface IssueTypeInfo {
    /**
     * The ID of the issue type.
     * @type {number}
     * @memberof IssueTypeInfo
     */
    id?: number;
    /**
     * The name of the issue type.
     * @type {string}
     * @memberof IssueTypeInfo
     */
    name?: string;
    /**
     * The avatar of the issue type.
     * @type {number}
     * @memberof IssueTypeInfo
     */
    avatarId?: number;
}
/**
 * Details of the issue creation metadata for an issue type.
 * @export
 * @interface IssueTypeIssueCreateMetadata
 */
export interface IssueTypeIssueCreateMetadata {
    /**
     * The URL of these issue type details.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    self?: string;
    /**
     * The ID of the issue type.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    id?: string;
    /**
     * The description of the issue type.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    description?: string;
    /**
     * The URL of the issue type's avatar.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    iconUrl?: string;
    /**
     * The name of the issue type.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    name?: string;
    /**
     * Whether this issue type is used to create subtasks.
     * @type {boolean}
     * @memberof IssueTypeIssueCreateMetadata
     */
    subtask?: boolean;
    /**
     * The ID of the issue type's avatar.
     * @type {number}
     * @memberof IssueTypeIssueCreateMetadata
     */
    avatarId?: number;
    /**
     * Unique ID for next-gen projects.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    entityId?: string;
    /**
     * Details of the next-gen projects the issue type is available in.
     * @type {Scope}
     * @memberof IssueTypeIssueCreateMetadata
     */
    scope?: Scope;
    /**
     * Expand options that include additional issue type metadata details in the response.
     * @type {string}
     * @memberof IssueTypeIssueCreateMetadata
     */
    expand?: string;
    /**
     * List of the fields available when creating an issue for the issue type.
     * @type {{ [key: string]: FieldMetadata; }}
     * @memberof IssueTypeIssueCreateMetadata
     */
    fields?: { [key: string]: FieldMetadata };
}
/**
 * Details of an issue type scheme.
 * @export
 * @interface IssueTypeScheme
 */
export interface IssueTypeScheme {
    /**
     * The ID of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeScheme
     */
    id: string;
    /**
     * The name of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeScheme
     */
    name: string;
    /**
     * The description of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeScheme
     */
    description?: string;
    /**
     * The ID of the default issue type of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeScheme
     */
    defaultIssueTypeId?: string;
    /**
     * Whether the issue type scheme is the default.
     * @type {boolean}
     * @memberof IssueTypeScheme
     */
    isDefault?: boolean;
}
/**
 * Details of an issue type scheme and its associated issue types.
 * @export
 * @interface IssueTypeSchemeDetails
 */
export interface IssueTypeSchemeDetails {
    /**
     * The name of the issue type scheme. The name must be unique. The maximum length is 255 characters.
     * @type {string}
     * @memberof IssueTypeSchemeDetails
     */
    name: string;
    /**
     * The description of the issue type scheme. The maximum length is 4000 characters.
     * @type {string}
     * @memberof IssueTypeSchemeDetails
     */
    description?: string;
    /**
     * The ID of the default issue type of the issue type scheme. This ID must be included in `issueTypeIds`.
     * @type {string}
     * @memberof IssueTypeSchemeDetails
     */
    defaultIssueTypeId?: string;
    /**
     * The list of issue types IDs of the issue type scheme. At least one standard issue type ID is required.
     * @type {Array<string>}
     * @memberof IssueTypeSchemeDetails
     */
    issueTypeIds: Array<string>;
}
/**
 * The ID of an issue type scheme.
 * @export
 * @interface IssueTypeSchemeID
 */
export interface IssueTypeSchemeID {
    /**
     * The ID of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeSchemeID
     */
    issueTypeSchemeId: string;
}
/**
 * Issue type scheme item.
 * @export
 * @interface IssueTypeSchemeMapping
 */
export interface IssueTypeSchemeMapping {
    /**
     * The ID of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeSchemeMapping
     */
    issueTypeSchemeId: string;
    /**
     * The ID of the issue type.
     * @type {string}
     * @memberof IssueTypeSchemeMapping
     */
    issueTypeId: string;
}
/**
 * Details of the association between an issue type scheme and project.
 * @export
 * @interface IssueTypeSchemeProjectAssociation
 */
export interface IssueTypeSchemeProjectAssociation {
    /**
     * The ID of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeSchemeProjectAssociation
     */
    issueTypeSchemeId: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof IssueTypeSchemeProjectAssociation
     */
    projectId: string;
}
/**
 * Issue type scheme with a list of the projects that use it.
 * @export
 * @interface IssueTypeSchemeProjects
 */
export interface IssueTypeSchemeProjects {
    /**
     * Details of an issue type scheme.
     * @type {IssueTypeScheme}
     * @memberof IssueTypeSchemeProjects
     */
    issueTypeScheme?: IssueTypeScheme;
    /**
     * The IDs of the projects using the issue type scheme.
     * @type {Array<string>}
     * @memberof IssueTypeSchemeProjects
     */
    projectIds: Array<string>;
}
/**
 * Details of the name, description, and default issue type for an issue type scheme.
 * @export
 * @interface IssueTypeSchemeUpdateDetails
 */
export interface IssueTypeSchemeUpdateDetails {
    /**
     * The name of the issue type scheme. The name must be unique. The maximum length is 255 characters.
     * @type {string}
     * @memberof IssueTypeSchemeUpdateDetails
     */
    name?: string;
    /**
     * The description of the issue type scheme. The maximum length is 4000 characters.
     * @type {string}
     * @memberof IssueTypeSchemeUpdateDetails
     */
    description?: string;
    /**
     * The ID of the default issue type of the issue type scheme.
     * @type {string}
     * @memberof IssueTypeSchemeUpdateDetails
     */
    defaultIssueTypeId?: string;
}
/**
 * Details of an issue type screen scheme.
 * @export
 * @interface IssueTypeScreenScheme
 */
export interface IssueTypeScreenScheme {
    /**
     * The ID of the issue type screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenScheme
     */
    id: string;
    /**
     * The name of the issue type screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenScheme
     */
    name: string;
    /**
     * The description of the issue type screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenScheme
     */
    description?: string;
}
/**
 * The screen scheme for an issue type.
 * @export
 * @interface IssueTypeScreenSchemeItem
 */
export interface IssueTypeScreenSchemeItem {
    /**
     * The ID of the issue type screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenSchemeItem
     */
    issueTypeScreenSchemeId: string;
    /**
     * The ID of the issue type or *default*. When set to *default* this issue type screen scheme item applies to all issue types without a screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenSchemeItem
     */
    issueTypeId: string;
    /**
     * The ID of the screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenSchemeItem
     */
    screenSchemeId: string;
}
/**
 * Associated issue type screen scheme and project.
 * @export
 * @interface IssueTypeScreenSchemeProjectAssociation
 */
export interface IssueTypeScreenSchemeProjectAssociation {
    /**
     * The ID of the issue type screen scheme.
     * @type {string}
     * @memberof IssueTypeScreenSchemeProjectAssociation
     */
    issueTypeScreenSchemeId?: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof IssueTypeScreenSchemeProjectAssociation
     */
    projectId?: string;
}
/**
 * Issue type screen scheme with a list of the projects that use it.
 * @export
 * @interface IssueTypeScreenSchemesProjects
 */
export interface IssueTypeScreenSchemesProjects {
    /**
     * Details of an issue type screen scheme.
     * @type {IssueTypeScreenScheme}
     * @memberof IssueTypeScreenSchemesProjects
     */
    issueTypeScreenScheme?: IssueTypeScreenScheme;
    /**
     * The IDs of the projects using the issue type screen scheme.
     * @type {Array<string>}
     * @memberof IssueTypeScreenSchemesProjects
     */
    projectIds: Array<string>;
}
/**
 *
 * @export
 * @interface IssueTypeUpdateBean
 */
export interface IssueTypeUpdateBean {
    /**
     * The unique name for the issue type. The maximum length is 60 characters.
     * @type {string}
     * @memberof IssueTypeUpdateBean
     */
    name?: string;
    /**
     * The description of the issue type.
     * @type {string}
     * @memberof IssueTypeUpdateBean
     */
    description?: string;
    /**
     * The ID of an issue type avatar.
     * @type {number}
     * @memberof IssueTypeUpdateBean
     */
    avatarId?: number;
}
/**
 * Status details for an issue type.
 * @export
 * @interface IssueTypeWithStatus
 */
export interface IssueTypeWithStatus {
    /**
     * The URL of the issue type's status details.
     * @type {string}
     * @memberof IssueTypeWithStatus
     */
    self: string;
    /**
     * The ID of the issue type.
     * @type {string}
     * @memberof IssueTypeWithStatus
     */
    id: string;
    /**
     * The name of the issue type.
     * @type {string}
     * @memberof IssueTypeWithStatus
     */
    name: string;
    /**
     * Whether this issue type represents subtasks.
     * @type {boolean}
     * @memberof IssueTypeWithStatus
     */
    subtask: boolean;
    /**
     * List of status details for the issue type.
     * @type {Array<StatusDetails>}
     * @memberof IssueTypeWithStatus
     */
    statuses: Array<StatusDetails>;
}
/**
 * Details about the mapping between an issue type and a workflow.
 * @export
 * @interface IssueTypeWorkflowMapping
 */
export interface IssueTypeWorkflowMapping {
    /**
     * The ID of the issue type. Not required if updating the issue type-workflow mapping.
     * @type {string}
     * @memberof IssueTypeWorkflowMapping
     */
    issueType?: string;
    /**
     * The name of the workflow.
     * @type {string}
     * @memberof IssueTypeWorkflowMapping
     */
    workflow?: string;
    /**
     * Set to true to create or update the draft of a workflow scheme and update the mapping in the draft, when the workflow scheme cannot be edited. Defaults to `false`. Only applicable when updating the workflow-issue types mapping.
     * @type {boolean}
     * @memberof IssueTypeWorkflowMapping
     */
    updateDraftIfNeeded?: boolean;
}
/**
 * Details about the mapping between issue types and a workflow.
 * @export
 * @interface IssueTypesWorkflowMapping
 */
export interface IssueTypesWorkflowMapping {
    /**
     * The name of the workflow. Optional if updating the workflow-issue types mapping.
     * @type {string}
     * @memberof IssueTypesWorkflowMapping
     */
    workflow?: string;
    /**
     * The list of issue type IDs.
     * @type {Array<string>}
     * @memberof IssueTypesWorkflowMapping
     */
    issueTypes?: Array<string>;
    /**
     * Whether the workflow is the default workflow for the workflow scheme.
     * @type {boolean}
     * @memberof IssueTypesWorkflowMapping
     */
    defaultMapping?: boolean;
    /**
     * Whether a draft workflow scheme is created or updated when updating an active workflow scheme. The draft is updated with the new workflow-issue types mapping. Defaults to `false`.
     * @type {boolean}
     * @memberof IssueTypesWorkflowMapping
     */
    updateDraftIfNeeded?: boolean;
}
/**
 * Details of an issue update request.
 * @export
 * @interface IssueUpdateDetails
 */
export interface IssueUpdateDetails {
    [key: string]: any;
}
/**
 * A list of editable field details.
 * @export
 * @interface IssueUpdateMetadata
 */
export interface IssueUpdateMetadata {
    /**
     * A list of editable field details.
     * @type {{ [key: string]: FieldMetadata; }}
     * @memberof IssueUpdateMetadata
     */
    fields?: { [key: string]: FieldMetadata };
}
/**
 * List of issues and JQL queries.
 * @export
 * @interface IssuesAndJQLQueries
 */
export interface IssuesAndJQLQueries {
    /**
     * A list of JQL queries.
     * @type {Array<string>}
     * @memberof IssuesAndJQLQueries
     */
    jqls: Array<string>;
    /**
     * A list of issue IDs.
     * @type {Array<number>}
     * @memberof IssuesAndJQLQueries
     */
    issueIds: Array<number>;
}
/**
 * The description of the page of issues loaded by the provided JQL query.
 * @export
 * @interface IssuesJqlMetaDataBean
 */
export interface IssuesJqlMetaDataBean {
    /**
     * The index of the first issue.
     * @type {number}
     * @memberof IssuesJqlMetaDataBean
     */
    startAt: number;
    /**
     * The maximum number of issues that could be loaded in this evaluation.
     * @type {number}
     * @memberof IssuesJqlMetaDataBean
     */
    maxResults: number;
    /**
     * The number of issues that were loaded in this evaluation.
     * @type {number}
     * @memberof IssuesJqlMetaDataBean
     */
    count: number;
    /**
     * The total number of issues the JQL returned.
     * @type {number}
     * @memberof IssuesJqlMetaDataBean
     */
    totalCount: number;
    /**
     * Any warnings related to the JQL query. Present only if the validation mode was set to `warn`.
     * @type {Array<string>}
     * @memberof IssuesJqlMetaDataBean
     */
    validationWarnings?: Array<string>;
}
/**
 * Meta data describing the `issues` context variable.
 * @export
 * @interface IssuesMetaBean
 */
export interface IssuesMetaBean {
    /**
     *
     * @type {IssuesJqlMetaDataBean}
     * @memberof IssuesMetaBean
     */
    jql?: IssuesJqlMetaDataBean;
}
/**
 *
 * @export
 * @interface IssuesUpdateBean
 */
export interface IssuesUpdateBean {
    [key: string]: any;
}
/**
 * The JQL queries to be converted.
 * @export
 * @interface JQLPersonalDataMigrationRequest
 */
export interface JQLPersonalDataMigrationRequest {
    /**
     * A list of queries with user identifiers. Maximum of 100 queries.
     * @type {Array<string>}
     * @memberof JQLPersonalDataMigrationRequest
     */
    queryStrings?: Array<string>;
}
/**
 * JQL queries that contained users that could not be found
 * @export
 * @interface JQLQueryWithUnknownUsers
 */
export interface JQLQueryWithUnknownUsers {
    /**
     * The original query, for reference
     * @type {string}
     * @memberof JQLQueryWithUnknownUsers
     */
    originalQuery?: string;
    /**
     * The converted query, with accountIDs instead of user identifiers, or 'unknown' for users that could not be found
     * @type {string}
     * @memberof JQLQueryWithUnknownUsers
     */
    convertedQuery?: string;
}
/**
 * Lists of JQL reference data.
 * @export
 * @interface JQLReferenceData
 */
export interface JQLReferenceData {
    /**
     * List of fields usable in JQL queries.
     * @type {Array<FieldReferenceData>}
     * @memberof JQLReferenceData
     */
    visibleFieldNames?: Array<FieldReferenceData>;
    /**
     * List of functions usable in JQL queries.
     * @type {Array<FunctionReferenceData>}
     * @memberof JQLReferenceData
     */
    visibleFunctionNames?: Array<FunctionReferenceData>;
    /**
     * List of JQL query reserved words.
     * @type {Array<string>}
     * @memberof JQLReferenceData
     */
    jqlReservedWords?: Array<string>;
}
/**
 * The JQL specifying the issues available in the evaluated Jira expression under the `issues` context variable.
 * @export
 * @interface JexpIssues
 */
export interface JexpIssues {
    /**
     * The JQL query that specifies the set of issues available in the Jira expression.
     * @type {JexpJqlIssues}
     * @memberof JexpIssues
     */
    jql?: JexpJqlIssues;
}
/**
 * The JQL specifying the issues available in the evaluated Jira expression under the `issues` context variable. Not all issues returned by the JQL query are loaded, only those described by the `startAt` and `maxResults` properties. To determine whether it is necessary to iterate to ensure all the issues returned by the JQL query are evaluated, inspect `meta.issues.jql.count` in the response.
 * @export
 * @interface JexpJqlIssues
 */
export interface JexpJqlIssues {
    /**
     * The JQL query.
     * @type {string}
     * @memberof JexpJqlIssues
     */
    query?: string;
    /**
     * The index of the first issue to return from the JQL query.
     * @type {number}
     * @memberof JexpJqlIssues
     */
    startAt?: number;
    /**
     * The maximum number of issues to return from the JQL query. Inspect `meta.issues.jql.maxResults` in the response to ensure the maximum value has not been exceeded.
     * @type {number}
     * @memberof JexpJqlIssues
     */
    maxResults?: number;
    /**
     * Determines how to validate the JQL query and treat the validation results.
     * @type {string}
     * @memberof JexpJqlIssues
     */
    validation?: JexpJqlIssues.ValidationEnum;
}

/**
 * @export
 * @namespace JexpJqlIssues
 */
export namespace JexpJqlIssues {
    /**
     * @export
     * @enum {string}
     */
    export enum ValidationEnum {
        Strict = <any>'strict',
        Warn = <any>'warn',
        None = <any>'none',
    }
}
/**
 * Details about the analysed Jira expression.
 * @export
 * @interface JiraExpressionAnalysis
 */
export interface JiraExpressionAnalysis {
    /**
     * The analysed expression.
     * @type {string}
     * @memberof JiraExpressionAnalysis
     */
    expression: string;
    /**
     * A list of validation errors. Not included if the expression is valid.
     * @type {Array<JiraExpressionValidationError>}
     * @memberof JiraExpressionAnalysis
     */
    errors?: Array<JiraExpressionValidationError>;
    /**
     * Whether the expression is valid and the interpreter will evaluate it. Note that the expression may fail at runtime (for example, if it executes too many expensive operations).
     * @type {boolean}
     * @memberof JiraExpressionAnalysis
     */
    valid: boolean;
    /**
     * EXPERIMENTAL. The inferred type of the expression.
     * @type {string}
     * @memberof JiraExpressionAnalysis
     */
    type: string;
}
/**
 *
 * @export
 * @interface JiraExpressionEvalContextBean
 */
export interface JiraExpressionEvalContextBean {
    /**
     * The issue that is available under the `issue` variable when evaluating the expression.
     * @type {IdOrKeyBean}
     * @memberof JiraExpressionEvalContextBean
     */
    issue?: IdOrKeyBean;
    /**
     * The collection of issues that is available under the `issues` variable when evaluating the expression.
     * @type {JexpIssues}
     * @memberof JiraExpressionEvalContextBean
     */
    issues?: JexpIssues;
    /**
     * The project that is available under the `project` variable when evaluating the expression.
     * @type {IdOrKeyBean}
     * @memberof JiraExpressionEvalContextBean
     */
    project?: IdOrKeyBean;
    /**
     * The ID of the sprint that is available under the `sprint` variable when evaluating the expression.
     * @type {number}
     * @memberof JiraExpressionEvalContextBean
     */
    sprint?: number;
    /**
     * The ID of the board that is available under the `board` variable when evaluating the expression.
     * @type {number}
     * @memberof JiraExpressionEvalContextBean
     */
    board?: number;
    /**
     * The ID of the service desk that is available under the `serviceDesk` variable when evaluating the expression.
     * @type {number}
     * @memberof JiraExpressionEvalContextBean
     */
    serviceDesk?: number;
    /**
     * The ID of the customer request that is available under the `customerRequest` variable when evaluating the expression. This is the same as the ID of the underlying Jira issue, but the customer request context variable will have a different type.
     * @type {number}
     * @memberof JiraExpressionEvalContextBean
     */
    customerRequest?: number;
}
/**
 *
 * @export
 * @interface JiraExpressionEvalRequestBean
 */
export interface JiraExpressionEvalRequestBean {
    /**
     * The Jira expression to evaluate.
     * @type {string}
     * @memberof JiraExpressionEvalRequestBean
     */
    expression: string;
    /**
     * The context in which the Jira expression is evaluated.
     * @type {JiraExpressionEvalContextBean}
     * @memberof JiraExpressionEvalRequestBean
     */
    context?: JiraExpressionEvalContextBean;
}
/**
 *
 * @export
 * @interface JiraExpressionEvaluationMetaDataBean
 */
export interface JiraExpressionEvaluationMetaDataBean {
    /**
     * Contains information about the expression complexity. For example, the number of steps it took to evaluate the expression.
     * @type {JiraExpressionsComplexityBean}
     * @memberof JiraExpressionEvaluationMetaDataBean
     */
    complexity?: JiraExpressionsComplexityBean;
    /**
     * Contains information about the `issues` variable in the context. For example, is the issues were loaded with JQL, information about the page will be included here.
     * @type {IssuesMetaBean}
     * @memberof JiraExpressionEvaluationMetaDataBean
     */
    issues?: IssuesMetaBean;
}
/**
 * Details of Jira expressions for analysis.
 * @export
 * @interface JiraExpressionForAnalysis
 */
export interface JiraExpressionForAnalysis {
    /**
     * The list of Jira expressions to analyse.
     * @type {Array<string>}
     * @memberof JiraExpressionForAnalysis
     */
    expressions: Array<string>;
    /**
     * Context variables and their types. The type checker assumes that [common context variables](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/#context-variables), such as `issue` or `project`, are available in context and sets their type. Use this property to override the default types or provide details of new variables.
     * @type {{ [key: string]: string; }}
     * @memberof JiraExpressionForAnalysis
     */
    contextVariables?: { [key: string]: string };
}
/**
 * The result of evaluating a Jira expression.
 * @export
 * @interface JiraExpressionResult
 */
export interface JiraExpressionResult {
    /**
     * The value of the evaluated expression. It may be a primitive JSON value or a Jira REST API object. (Some expressions do not produce any meaningful results—for example, an expression that returns a lambda function—if that's the case a simple string representation is returned. These string representations should not be relied upon and may change without notice.)
     * @type {ModelObject}
     * @memberof JiraExpressionResult
     */
    value: ModelObject;
    /**
     * Contains various characteristics of the performed expression evaluation.
     * @type {JiraExpressionEvaluationMetaDataBean}
     * @memberof JiraExpressionResult
     */
    meta?: JiraExpressionEvaluationMetaDataBean;
}
/**
 * Details about syntax and type errors. The error details apply to the entire expression, unless the object includes:   *  `line` and `column`  *  `expression`
 * @export
 * @interface JiraExpressionValidationError
 */
export interface JiraExpressionValidationError {
    /**
     * The text line in which the error occurred.
     * @type {number}
     * @memberof JiraExpressionValidationError
     */
    line?: number;
    /**
     * The text column in which the error occurred.
     * @type {number}
     * @memberof JiraExpressionValidationError
     */
    column?: number;
    /**
     * The part of the expression in which the error occurred.
     * @type {string}
     * @memberof JiraExpressionValidationError
     */
    expression?: string;
    /**
     * Details about the error.
     * @type {string}
     * @memberof JiraExpressionValidationError
     */
    message: string;
    /**
     * The error type.
     * @type {string}
     * @memberof JiraExpressionValidationError
     */
    type: JiraExpressionValidationError.TypeEnum;
}

/**
 * @export
 * @namespace JiraExpressionValidationError
 */
export namespace JiraExpressionValidationError {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Syntax = <any>'syntax',
        Type = <any>'type',
        Other = <any>'other',
    }
}
/**
 * Details about the analysed Jira expression.
 * @export
 * @interface JiraExpressionsAnalysis
 */
export interface JiraExpressionsAnalysis {
    /**
     * The results of Jira expressions analysis.
     * @type {Array<JiraExpressionAnalysis>}
     * @memberof JiraExpressionsAnalysis
     */
    results: Array<JiraExpressionAnalysis>;
}
/**
 *
 * @export
 * @interface JiraExpressionsComplexityBean
 */
export interface JiraExpressionsComplexityBean {
    /**
     * The number of steps it took to evaluate the expression, where a step is a high-level operation performed by the expression. A step is an operation such as arithmetic, accessing a property, accessing a context variable, or calling a function.
     * @type {JiraExpressionsComplexityValueBean}
     * @memberof JiraExpressionsComplexityBean
     */
    steps?: JiraExpressionsComplexityValueBean;
    /**
     * The number of expensive operations executed while evaluating the expression. Expensive operations are those that load additional data, such as entity properties, comments, or custom fields.
     * @type {JiraExpressionsComplexityValueBean}
     * @memberof JiraExpressionsComplexityBean
     */
    expensiveOperations?: JiraExpressionsComplexityValueBean;
    /**
     * The number of Jira REST API beans returned in the response.
     * @type {JiraExpressionsComplexityValueBean}
     * @memberof JiraExpressionsComplexityBean
     */
    beans?: JiraExpressionsComplexityValueBean;
    /**
     * The number of primitive values returned in the response.
     * @type {JiraExpressionsComplexityValueBean}
     * @memberof JiraExpressionsComplexityBean
     */
    primitiveValues?: JiraExpressionsComplexityValueBean;
}
/**
 *
 * @export
 * @interface JiraExpressionsComplexityValueBean
 */
export interface JiraExpressionsComplexityValueBean {
    /**
     * The complexity value of the current expression.
     * @type {number}
     * @memberof JiraExpressionsComplexityValueBean
     */
    value: number;
    /**
     * The maximum allowed complexity. The evaluation will fail if this value is exceeded.
     * @type {number}
     * @memberof JiraExpressionsComplexityValueBean
     */
    limit: number;
}
/**
 * A list of JQL queries to parse.
 * @export
 * @interface JqlQueriesToParse
 */
export interface JqlQueriesToParse {
    /**
     * A list of queries to parse.
     * @type {Array<string>}
     * @memberof JqlQueriesToParse
     */
    queries: Array<string>;
}
/**
 * A parsed JQL query.
 * @export
 * @interface JqlQuery
 */
export interface JqlQuery {
    /**
     *
     * @type {JqlQueryClause}
     * @memberof JqlQuery
     */
    where?: JqlQueryClause;
    /**
     *
     * @type {JqlQueryOrderByClause}
     * @memberof JqlQuery
     */
    orderBy?: JqlQueryOrderByClause;
}
/**
 * A JQL query clause.
 * @export
 * @interface JqlQueryClause
 */
export interface JqlQueryClause {}
/**
 * Details of an operand in a JQL clause.
 * @export
 * @interface JqlQueryClauseOperand
 */
export interface JqlQueryClauseOperand {}
/**
 * A time predicate for a temporal JQL clause.
 * @export
 * @interface JqlQueryClauseTimePredicate
 */
export interface JqlQueryClauseTimePredicate {
    /**
     * The operator between the field and the operand.
     * @type {string}
     * @memberof JqlQueryClauseTimePredicate
     */
    operator: JqlQueryClauseTimePredicate.OperatorEnum;
    /**
     *
     * @type {JqlQueryClauseOperand}
     * @memberof JqlQueryClauseTimePredicate
     */
    operand: JqlQueryClauseOperand;
}

/**
 * @export
 * @namespace JqlQueryClauseTimePredicate
 */
export namespace JqlQueryClauseTimePredicate {
    /**
     * @export
     * @enum {string}
     */
    export enum OperatorEnum {
        Before = <any>'before',
        After = <any>'after',
        From = <any>'from',
        To = <any>'to',
        On = <any>'on',
        During = <any>'during',
        By = <any>'by',
    }
}
/**
 * A field used in a JQL query. See [Advanced searching - fields reference](https://confluence.atlassian.com/x/dAiiLQ) for more information about fields in JQL queries.
 * @export
 * @interface JqlQueryField
 */
export interface JqlQueryField {
    /**
     * The name of the field.
     * @type {string}
     * @memberof JqlQueryField
     */
    name: string;
    /**
     * When the field refers to a value in an entity property, details of the entity property value.
     * @type {Array<JqlQueryFieldEntityProperty>}
     * @memberof JqlQueryField
     */
    property?: Array<JqlQueryFieldEntityProperty>;
}
/**
 * Details of an entity property.
 * @export
 * @interface JqlQueryFieldEntityProperty
 */
export interface JqlQueryFieldEntityProperty {
    /**
     * The object on which the property is set.
     * @type {string}
     * @memberof JqlQueryFieldEntityProperty
     */
    entity: string;
    /**
     * The key of the property.
     * @type {string}
     * @memberof JqlQueryFieldEntityProperty
     */
    key: string;
    /**
     * The path in the property value to query.
     * @type {string}
     * @memberof JqlQueryFieldEntityProperty
     */
    path: string;
    /**
     * The type of the property value extraction. Not available if the extraction for the property is not registered on the instance with the [Entity property](https://developer.atlassian.com/cloud/jira/platform/modules/entity-property/) module.
     * @type {string}
     * @memberof JqlQueryFieldEntityProperty
     */
    type?: JqlQueryFieldEntityProperty.TypeEnum;
}

/**
 * @export
 * @namespace JqlQueryFieldEntityProperty
 */
export namespace JqlQueryFieldEntityProperty {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Number = <any>'number',
        String = <any>'string',
        Text = <any>'text',
        Date = <any>'date',
        User = <any>'user',
    }
}
/**
 * Details of the order-by JQL clause.
 * @export
 * @interface JqlQueryOrderByClause
 */
export interface JqlQueryOrderByClause {
    /**
     * The list of order-by clause fields and their ordering directives.
     * @type {Array<JqlQueryOrderByClauseElement>}
     * @memberof JqlQueryOrderByClause
     */
    fields: Array<JqlQueryOrderByClauseElement>;
}
/**
 * An element of the order-by JQL clause.
 * @export
 * @interface JqlQueryOrderByClauseElement
 */
export interface JqlQueryOrderByClauseElement {
    /**
     *
     * @type {JqlQueryField}
     * @memberof JqlQueryOrderByClauseElement
     */
    field?: JqlQueryField;
    /**
     * The direction in which to order the results.
     * @type {string}
     * @memberof JqlQueryOrderByClauseElement
     */
    direction?: JqlQueryOrderByClauseElement.DirectionEnum;
}

/**
 * @export
 * @namespace JqlQueryOrderByClauseElement
 */
export namespace JqlQueryOrderByClauseElement {
    /**
     * @export
     * @enum {string}
     */
    export enum DirectionEnum {
        Asc = <any>'asc',
        Desc = <any>'desc',
    }
}
/**
 * An operand that can be part of a list operand.
 * @export
 * @interface JqlQueryUnitaryOperand
 */
export interface JqlQueryUnitaryOperand {}
/**
 *
 * @export
 * @interface JsonNode
 */
export interface JsonNode {
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    valueNode?: boolean;
    /**
     *
     * @type {any}
     * @memberof JsonNode
     */
    elements?: any;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    object?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    containerNode?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    missingNode?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    number?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    pojo?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    integralNumber?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    floatingPointNumber?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    _int?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    _long?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    _double?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    bigDecimal?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    bigInteger?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    textual?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    _boolean?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    binary?: boolean;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    numberValue?: number;
    /**
     *
     * @type {string}
     * @memberof JsonNode
     */
    numberType?: JsonNode.NumberTypeEnum;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    intValue?: number;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    longValue?: number;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    bigIntegerValue?: number;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    doubleValue?: number;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    decimalValue?: number;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    booleanValue?: boolean;
    /**
     *
     * @type {Array<string>}
     * @memberof JsonNode
     */
    binaryValue?: Array<string>;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    valueAsInt?: number;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    valueAsLong?: number;
    /**
     *
     * @type {number}
     * @memberof JsonNode
     */
    valueAsDouble?: number;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    valueAsBoolean?: boolean;
    /**
     *
     * @type {string}
     * @memberof JsonNode
     */
    textValue?: string;
    /**
     *
     * @type {string}
     * @memberof JsonNode
     */
    valueAsText?: string;
    /**
     *
     * @type {any}
     * @memberof JsonNode
     */
    fieldNames?: any;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    array?: boolean;
    /**
     *
     * @type {any}
     * @memberof JsonNode
     */
    fields?: any;
    /**
     *
     * @type {boolean}
     * @memberof JsonNode
     */
    _null?: boolean;
}

/**
 * @export
 * @namespace JsonNode
 */
export namespace JsonNode {
    /**
     * @export
     * @enum {string}
     */
    export enum NumberTypeEnum {
        INT = <any>'INT',
        LONG = <any>'LONG',
        BIGINTEGER = <any>'BIG_INTEGER',
        FLOAT = <any>'FLOAT',
        DOUBLE = <any>'DOUBLE',
        BIGDECIMAL = <any>'BIG_DECIMAL',
    }
}
/**
 * The schema of a field.
 * @export
 * @interface JsonTypeBean
 */
export interface JsonTypeBean {
    /**
     * The data type of the field.
     * @type {string}
     * @memberof JsonTypeBean
     */
    type: string;
    /**
     * When the data type is an array, the name of the field items within the array.
     * @type {string}
     * @memberof JsonTypeBean
     */
    items?: string;
    /**
     * If the field is a system field, the name of the field.
     * @type {string}
     * @memberof JsonTypeBean
     */
    system?: string;
    /**
     * If the field is a custom field, the URI of the field.
     * @type {string}
     * @memberof JsonTypeBean
     */
    custom?: string;
    /**
     * If the field is a custom field, the custom ID of the field.
     * @type {number}
     * @memberof JsonTypeBean
     */
    customId?: number;
    /**
     * If the field is a custom field, the configuration of the field.
     * @type {{ [key: string]: ModelObject; }}
     * @memberof JsonTypeBean
     */
    configuration?: { [key: string]: ModelObject };
}
/**
 * An operand that is a JQL keyword. See [Advanced searching - keywords reference](https://confluence.atlassian.com/jiracorecloud/advanced-searching-keywords-reference-765593717.html#Advancedsearching-keywordsreference-EMPTYEMPTY) for more information about operand keywords.
 * @export
 * @interface KeywordOperand
 */
export interface KeywordOperand {
    /**
     * The keyword that is the operand value.
     * @type {string}
     * @memberof KeywordOperand
     */
    keyword: KeywordOperand.KeywordEnum;
}

/**
 * @export
 * @namespace KeywordOperand
 */
export namespace KeywordOperand {
    /**
     * @export
     * @enum {string}
     */
    export enum KeywordEnum {
        Empty = <any>'empty',
    }
}
/**
 * Details a link group, which defines issue operations.
 * @export
 * @interface LinkGroup
 */
export interface LinkGroup {
    /**
     *
     * @type {string}
     * @memberof LinkGroup
     */
    id?: string;
    /**
     *
     * @type {string}
     * @memberof LinkGroup
     */
    styleClass?: string;
    /**
     *
     * @type {SimpleLink}
     * @memberof LinkGroup
     */
    header?: SimpleLink;
    /**
     *
     * @type {number}
     * @memberof LinkGroup
     */
    weight?: number;
    /**
     *
     * @type {Array<SimpleLink>}
     * @memberof LinkGroup
     */
    links?: Array<SimpleLink>;
    /**
     *
     * @type {Array<LinkGroup>}
     * @memberof LinkGroup
     */
    groups?: Array<LinkGroup>;
}
/**
 *
 * @export
 * @interface LinkIssueRequestJsonBean
 */
export interface LinkIssueRequestJsonBean {
    /**
     *
     * @type {IssueLinkType}
     * @memberof LinkIssueRequestJsonBean
     */
    type?: IssueLinkType;
    /**
     *
     * @type {LinkedIssue}
     * @memberof LinkIssueRequestJsonBean
     */
    inwardIssue?: LinkedIssue;
    /**
     *
     * @type {LinkedIssue}
     * @memberof LinkIssueRequestJsonBean
     */
    outwardIssue?: LinkedIssue;
    /**
     *
     * @type {Comment}
     * @memberof LinkIssueRequestJsonBean
     */
    comment?: Comment;
}
/**
 * The ID or key of a linked issue.
 * @export
 * @interface LinkedIssue
 */
export interface LinkedIssue {
    /**
     * The ID of an issue. Required if `key` isn't provided.
     * @type {string}
     * @memberof LinkedIssue
     */
    id?: string;
    /**
     * The key of an issue. Required if `id` isn't provided.
     * @type {string}
     * @memberof LinkedIssue
     */
    key?: string;
    /**
     * The URL of the issue.
     * @type {string}
     * @memberof LinkedIssue
     */
    self?: string;
    /**
     * The fields associated with the issue.
     * @type {Fields}
     * @memberof LinkedIssue
     */
    fields?: Fields;
}
/**
 * An operand that is a list of values.
 * @export
 * @interface ListOperand
 */
export interface ListOperand {
    /**
     * The list of operand values.
     * @type {Array<JqlQueryUnitaryOperand>}
     * @memberof ListOperand
     */
    values?: Array<JqlQueryUnitaryOperand>;
}
/**
 *
 * @export
 * @interface ListWrapperCallbackApplicationRole
 */
export interface ListWrapperCallbackApplicationRole {}
/**
 *
 * @export
 * @interface ListWrapperCallbackGroupName
 */
export interface ListWrapperCallbackGroupName {}
/**
 * Details of a locale.
 * @export
 * @interface Locale
 */
export interface Locale {
    /**
     * The locale code. The Java the locale format is used: a two character language code (ISO 639), an underscore, and two letter country code (ISO 3166). For example, en\\_US represents a locale of English (United States). Required on create.
     * @type {string}
     * @memberof Locale
     */
    locale?: string;
}
/**
 *
 * @export
 * @interface MoveFieldBean
 */
export interface MoveFieldBean {
    /**
     * The ID of the screen tab field after which to place the moved screen tab field. Required if `position` isn't provided.
     * @type {string}
     * @memberof MoveFieldBean
     */
    after?: string;
    /**
     * The named position to which the screen tab field should be moved. Required if `after` isn't provided.
     * @type {string}
     * @memberof MoveFieldBean
     */
    position?: MoveFieldBean.PositionEnum;
}

/**
 * @export
 * @namespace MoveFieldBean
 */
export namespace MoveFieldBean {
    /**
     * @export
     * @enum {string}
     */
    export enum PositionEnum {
        Earlier = <any>'Earlier',
        Later = <any>'Later',
        First = <any>'First',
        Last = <any>'Last',
    }
}
/**
 *
 * @export
 * @interface NestedResponse
 */
export interface NestedResponse {
    /**
     *
     * @type {number}
     * @memberof NestedResponse
     */
    status?: number;
    /**
     *
     * @type {ErrorCollection}
     * @memberof NestedResponse
     */
    errorCollection?: ErrorCollection;
}
/**
 * Details about a notification.
 * @export
 * @interface Notification
 */
export interface Notification {
    [key: string]: any;
}
/**
 * Details about a notification event.
 * @export
 * @interface NotificationEvent
 */
export interface NotificationEvent {
    /**
     * The ID of the event. The event can be a [Jira system event](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or a [custom event](https://confluence.atlassian.com/x/AIlKLg).
     * @type {number}
     * @memberof NotificationEvent
     */
    id?: number;
    /**
     * The name of the event.
     * @type {string}
     * @memberof NotificationEvent
     */
    name?: string;
    /**
     * The description of the event.
     * @type {string}
     * @memberof NotificationEvent
     */
    description?: string;
    /**
     * The template of the event. Only custom events configured by Jira administrators have template.
     * @type {NotificationEvent}
     * @memberof NotificationEvent
     */
    templateEvent?: NotificationEvent;
}
/**
 * Details of the users and groups to receive the notification.
 * @export
 * @interface NotificationRecipients
 */
export interface NotificationRecipients {
    [key: string]: any;
}
/**
 * Details of the group membership or permissions needed to receive the notification.
 * @export
 * @interface NotificationRecipientsRestrictions
 */
export interface NotificationRecipientsRestrictions {
    /**
     * List of group memberships required to receive the notification.
     * @type {Array<GroupName>}
     * @memberof NotificationRecipientsRestrictions
     */
    groups?: Array<GroupName>;
    /**
     * List of permissions required to receive the notification.
     * @type {Array<RestrictedPermission>}
     * @memberof NotificationRecipientsRestrictions
     */
    permissions?: Array<RestrictedPermission>;
}
/**
 * Details about a notification scheme.
 * @export
 * @interface NotificationScheme
 */
export interface NotificationScheme {
    /**
     * Expand options that include additional notification scheme details in the response.
     * @type {string}
     * @memberof NotificationScheme
     */
    expand?: string;
    /**
     * The ID of the notification scheme.
     * @type {number}
     * @memberof NotificationScheme
     */
    id?: number;
    /**
     *
     * @type {string}
     * @memberof NotificationScheme
     */
    self?: string;
    /**
     * The name of the notification scheme.
     * @type {string}
     * @memberof NotificationScheme
     */
    name?: string;
    /**
     * The description of the notification scheme.
     * @type {string}
     * @memberof NotificationScheme
     */
    description?: string;
    /**
     * The notification events and associated recipients.
     * @type {Array<NotificationSchemeEvent>}
     * @memberof NotificationScheme
     */
    notificationSchemeEvents?: Array<NotificationSchemeEvent>;
    /**
     * The scope of the notification scheme.
     * @type {Scope}
     * @memberof NotificationScheme
     */
    scope?: Scope;
}
/**
 * Details about a notification scheme event.
 * @export
 * @interface NotificationSchemeEvent
 */
export interface NotificationSchemeEvent {
    /**
     *
     * @type {NotificationEvent}
     * @memberof NotificationSchemeEvent
     */
    event?: NotificationEvent;
    /**
     *
     * @type {Array<EventNotification>}
     * @memberof NotificationSchemeEvent
     */
    notifications?: Array<EventNotification>;
}
/**
 *
 * @export
 * @interface OperationMessage
 */
export interface OperationMessage {
    /**
     * The human-readable message that describes the result.
     * @type {string}
     * @memberof OperationMessage
     */
    message: string;
    /**
     * The status code of the response.
     * @type {number}
     * @memberof OperationMessage
     */
    statusCode: number;
}
/**
 * Details of the operations that can be performed on the issue.
 * @export
 * @interface Operations
 */
export interface Operations {
    [key: string]: any;
}
/**
 * An ordered list of issue type IDs and information about where to move them.
 * @export
 * @interface OrderOfIssueTypes
 */
export interface OrderOfIssueTypes {
    /**
     * A list of the issue type IDs to move. The order of the issue type IDs in the list is the order they are given after the move.
     * @type {Array<string>}
     * @memberof OrderOfIssueTypes
     */
    issueTypeIds: Array<string>;
    /**
     * The ID of the issue type to place the moved issue types after. Required if `position` isn't provided.
     * @type {string}
     * @memberof OrderOfIssueTypes
     */
    after?: string;
    /**
     * The position the issue types should be moved to. Required if `after` isn't provided.
     * @type {string}
     * @memberof OrderOfIssueTypes
     */
    position?: OrderOfIssueTypes.PositionEnum;
}

/**
 * @export
 * @namespace OrderOfIssueTypes
 */
export namespace OrderOfIssueTypes {
    /**
     * @export
     * @enum {string}
     */
    export enum PositionEnum {
        First = <any>'First',
        Last = <any>'Last',
    }
}
/**
 * A page of items.
 * @export
 * @interface PageBeanChangelog
 */
export interface PageBeanChangelog {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanChangelog
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanChangelog
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanChangelog
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanChangelog
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanChangelog
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanChangelog
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Changelog>}
     * @memberof PageBeanChangelog
     */
    values?: Array<Changelog>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanComment
 */
export interface PageBeanComment {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanComment
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanComment
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanComment
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanComment
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanComment
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanComment
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Comment>}
     * @memberof PageBeanComment
     */
    values?: Array<Comment>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanComponentWithIssueCount
 */
export interface PageBeanComponentWithIssueCount {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanComponentWithIssueCount
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanComponentWithIssueCount
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanComponentWithIssueCount
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanComponentWithIssueCount
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanComponentWithIssueCount
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanComponentWithIssueCount
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<ComponentWithIssueCount>}
     * @memberof PageBeanComponentWithIssueCount
     */
    values?: Array<ComponentWithIssueCount>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanContext
 */
export interface PageBeanContext {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanContext
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanContext
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanContext
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanContext
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanContext
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanContext
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Context>}
     * @memberof PageBeanContext
     */
    values?: Array<Context>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanCustomFieldOptionDetails
 */
export interface PageBeanCustomFieldOptionDetails {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<CustomFieldOptionDetails>}
     * @memberof PageBeanCustomFieldOptionDetails
     */
    values?: Array<CustomFieldOptionDetails>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanDashboard
 */
export interface PageBeanDashboard {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanDashboard
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanDashboard
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanDashboard
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanDashboard
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanDashboard
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanDashboard
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Dashboard>}
     * @memberof PageBeanDashboard
     */
    values?: Array<Dashboard>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanField
 */
export interface PageBeanField {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanField
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanField
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanField
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanField
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanField
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanField
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Field>}
     * @memberof PageBeanField
     */
    values?: Array<Field>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanFieldConfiguration
 */
export interface PageBeanFieldConfiguration {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanFieldConfiguration
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanFieldConfiguration
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanFieldConfiguration
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanFieldConfiguration
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanFieldConfiguration
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanFieldConfiguration
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<FieldConfiguration>}
     * @memberof PageBeanFieldConfiguration
     */
    values?: Array<FieldConfiguration>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanFieldConfigurationIssueTypeItem
 */
export interface PageBeanFieldConfigurationIssueTypeItem {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<FieldConfigurationIssueTypeItem>}
     * @memberof PageBeanFieldConfigurationIssueTypeItem
     */
    values?: Array<FieldConfigurationIssueTypeItem>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanFieldConfigurationItem
 */
export interface PageBeanFieldConfigurationItem {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationItem
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationItem
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationItem
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationItem
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationItem
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanFieldConfigurationItem
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<FieldConfigurationItem>}
     * @memberof PageBeanFieldConfigurationItem
     */
    values?: Array<FieldConfigurationItem>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanFieldConfigurationScheme
 */
export interface PageBeanFieldConfigurationScheme {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationScheme
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationScheme
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationScheme
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationScheme
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationScheme
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanFieldConfigurationScheme
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<FieldConfigurationScheme>}
     * @memberof PageBeanFieldConfigurationScheme
     */
    values?: Array<FieldConfigurationScheme>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanFieldConfigurationSchemeProjects
 */
export interface PageBeanFieldConfigurationSchemeProjects {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<FieldConfigurationSchemeProjects>}
     * @memberof PageBeanFieldConfigurationSchemeProjects
     */
    values?: Array<FieldConfigurationSchemeProjects>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanFilterDetails
 */
export interface PageBeanFilterDetails {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanFilterDetails
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanFilterDetails
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanFilterDetails
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanFilterDetails
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanFilterDetails
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanFilterDetails
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<FilterDetails>}
     * @memberof PageBeanFilterDetails
     */
    values?: Array<FilterDetails>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanGroupDetails
 */
export interface PageBeanGroupDetails {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanGroupDetails
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanGroupDetails
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanGroupDetails
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanGroupDetails
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanGroupDetails
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanGroupDetails
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<GroupDetails>}
     * @memberof PageBeanGroupDetails
     */
    values?: Array<GroupDetails>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueFieldOption
 */
export interface PageBeanIssueFieldOption {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueFieldOption
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueFieldOption
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueFieldOption
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueFieldOption
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueFieldOption
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueFieldOption
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueFieldOption>}
     * @memberof PageBeanIssueFieldOption
     */
    values?: Array<IssueFieldOption>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueSecurityLevelMember
 */
export interface PageBeanIssueSecurityLevelMember {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueSecurityLevelMember>}
     * @memberof PageBeanIssueSecurityLevelMember
     */
    values?: Array<IssueSecurityLevelMember>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueTypeScheme
 */
export interface PageBeanIssueTypeScheme {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueTypeScheme
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueTypeScheme
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScheme
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScheme
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScheme
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueTypeScheme
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueTypeScheme>}
     * @memberof PageBeanIssueTypeScheme
     */
    values?: Array<IssueTypeScheme>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueTypeSchemeMapping
 */
export interface PageBeanIssueTypeSchemeMapping {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueTypeSchemeMapping>}
     * @memberof PageBeanIssueTypeSchemeMapping
     */
    values?: Array<IssueTypeSchemeMapping>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueTypeSchemeProjects
 */
export interface PageBeanIssueTypeSchemeProjects {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueTypeSchemeProjects>}
     * @memberof PageBeanIssueTypeSchemeProjects
     */
    values?: Array<IssueTypeSchemeProjects>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueTypeScreenScheme
 */
export interface PageBeanIssueTypeScreenScheme {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueTypeScreenScheme>}
     * @memberof PageBeanIssueTypeScreenScheme
     */
    values?: Array<IssueTypeScreenScheme>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueTypeScreenSchemeItem
 */
export interface PageBeanIssueTypeScreenSchemeItem {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueTypeScreenSchemeItem>}
     * @memberof PageBeanIssueTypeScreenSchemeItem
     */
    values?: Array<IssueTypeScreenSchemeItem>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanIssueTypeScreenSchemesProjects
 */
export interface PageBeanIssueTypeScreenSchemesProjects {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<IssueTypeScreenSchemesProjects>}
     * @memberof PageBeanIssueTypeScreenSchemesProjects
     */
    values?: Array<IssueTypeScreenSchemesProjects>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanNotificationScheme
 */
export interface PageBeanNotificationScheme {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanNotificationScheme
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanNotificationScheme
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanNotificationScheme
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanNotificationScheme
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanNotificationScheme
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanNotificationScheme
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<NotificationScheme>}
     * @memberof PageBeanNotificationScheme
     */
    values?: Array<NotificationScheme>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanProject
 */
export interface PageBeanProject {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanProject
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanProject
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanProject
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanProject
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanProject
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanProject
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Project>}
     * @memberof PageBeanProject
     */
    values?: Array<Project>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanScreen
 */
export interface PageBeanScreen {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanScreen
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanScreen
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanScreen
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanScreen
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanScreen
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanScreen
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Screen>}
     * @memberof PageBeanScreen
     */
    values?: Array<Screen>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanScreenScheme
 */
export interface PageBeanScreenScheme {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanScreenScheme
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanScreenScheme
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanScreenScheme
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanScreenScheme
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanScreenScheme
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanScreenScheme
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<ScreenScheme>}
     * @memberof PageBeanScreenScheme
     */
    values?: Array<ScreenScheme>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanString
 */
export interface PageBeanString {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanString
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanString
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanString
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanString
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanString
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanString
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<string>}
     * @memberof PageBeanString
     */
    values?: Array<string>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanUser
 */
export interface PageBeanUser {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanUser
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanUser
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanUser
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanUser
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanUser
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanUser
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<User>}
     * @memberof PageBeanUser
     */
    values?: Array<User>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanUserDetails
 */
export interface PageBeanUserDetails {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanUserDetails
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanUserDetails
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanUserDetails
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanUserDetails
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanUserDetails
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanUserDetails
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<UserDetails>}
     * @memberof PageBeanUserDetails
     */
    values?: Array<UserDetails>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanUserKey
 */
export interface PageBeanUserKey {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanUserKey
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanUserKey
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanUserKey
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanUserKey
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanUserKey
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanUserKey
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<UserKey>}
     * @memberof PageBeanUserKey
     */
    values?: Array<UserKey>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanVersion
 */
export interface PageBeanVersion {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanVersion
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanVersion
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanVersion
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanVersion
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanVersion
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanVersion
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Version>}
     * @memberof PageBeanVersion
     */
    values?: Array<Version>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanWebhook
 */
export interface PageBeanWebhook {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanWebhook
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanWebhook
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanWebhook
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanWebhook
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanWebhook
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanWebhook
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Webhook>}
     * @memberof PageBeanWebhook
     */
    values?: Array<Webhook>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanWorkflow
 */
export interface PageBeanWorkflow {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanWorkflow
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanWorkflow
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanWorkflow
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanWorkflow
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanWorkflow
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanWorkflow
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<Workflow>}
     * @memberof PageBeanWorkflow
     */
    values?: Array<Workflow>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanWorkflowScheme
 */
export interface PageBeanWorkflowScheme {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanWorkflowScheme
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanWorkflowScheme
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanWorkflowScheme
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanWorkflowScheme
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanWorkflowScheme
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanWorkflowScheme
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<WorkflowScheme>}
     * @memberof PageBeanWorkflowScheme
     */
    values?: Array<WorkflowScheme>;
}
/**
 * A page of items.
 * @export
 * @interface PageBeanWorkflowTransitionRules
 */
export interface PageBeanWorkflowTransitionRules {
    /**
     * The URL of the page.
     * @type {string}
     * @memberof PageBeanWorkflowTransitionRules
     */
    self?: string;
    /**
     * If there is another page of results, the URL of the next page.
     * @type {string}
     * @memberof PageBeanWorkflowTransitionRules
     */
    nextPage?: string;
    /**
     * The maximum number of items that could be returned.
     * @type {number}
     * @memberof PageBeanWorkflowTransitionRules
     */
    maxResults?: number;
    /**
     * The index of the first item returned.
     * @type {number}
     * @memberof PageBeanWorkflowTransitionRules
     */
    startAt?: number;
    /**
     * The number of items returned.
     * @type {number}
     * @memberof PageBeanWorkflowTransitionRules
     */
    total?: number;
    /**
     * Whether this is the last page.
     * @type {boolean}
     * @memberof PageBeanWorkflowTransitionRules
     */
    isLast?: boolean;
    /**
     * The list of items.
     * @type {Array<WorkflowTransitionRules>}
     * @memberof PageBeanWorkflowTransitionRules
     */
    values?: Array<WorkflowTransitionRules>;
}
/**
 * A page of changelogs.
 * @export
 * @interface PageOfChangelogs
 */
export interface PageOfChangelogs {
    /**
     * The index of the first item returned on the page.
     * @type {number}
     * @memberof PageOfChangelogs
     */
    startAt?: number;
    /**
     * The maximum number of results that could be on the page.
     * @type {number}
     * @memberof PageOfChangelogs
     */
    maxResults?: number;
    /**
     * The number of results on the page.
     * @type {number}
     * @memberof PageOfChangelogs
     */
    total?: number;
    /**
     * The list of changelogs.
     * @type {Array<Changelog>}
     * @memberof PageOfChangelogs
     */
    histories?: Array<Changelog>;
}
/**
 * A page of comments.
 * @export
 * @interface PageOfComments
 */
export interface PageOfComments {
    [key: string]: any;
}
/**
 * A page containing dashboard details.
 * @export
 * @interface PageOfDashboards
 */
export interface PageOfDashboards {
    /**
     * The index of the first item returned on the page.
     * @type {number}
     * @memberof PageOfDashboards
     */
    startAt?: number;
    /**
     * The maximum number of results that could be on the page.
     * @type {number}
     * @memberof PageOfDashboards
     */
    maxResults?: number;
    /**
     * The number of results on the page.
     * @type {number}
     * @memberof PageOfDashboards
     */
    total?: number;
    /**
     * The URL of the previous page of results, if any.
     * @type {string}
     * @memberof PageOfDashboards
     */
    prev?: string;
    /**
     * The URL of the next page of results, if any.
     * @type {string}
     * @memberof PageOfDashboards
     */
    next?: string;
    /**
     * List of dashboards.
     * @type {Array<Dashboard>}
     * @memberof PageOfDashboards
     */
    dashboards?: Array<Dashboard>;
}
/**
 * Paginated list of worklog details
 * @export
 * @interface PageOfWorklogs
 */
export interface PageOfWorklogs {
    [key: string]: any;
}
/**
 * A paged list. To access additional details append `[start-index:end-index]` to the expand request. For example, `?expand=sharedUsers[10:40]` returns a list starting at item 10 and finishing at item 40.
 * @export
 * @interface PagedListUserDetailsApplicationUser
 */
export interface PagedListUserDetailsApplicationUser {
    /**
     * The number of items on the page.
     * @type {number}
     * @memberof PagedListUserDetailsApplicationUser
     */
    size?: number;
    /**
     * The list of items.
     * @type {Array<UserDetails>}
     * @memberof PagedListUserDetailsApplicationUser
     */
    items?: Array<UserDetails>;
    /**
     * The maximum number of results that could be on the page.
     * @type {number}
     * @memberof PagedListUserDetailsApplicationUser
     */
    maxResults?: number;
    /**
     * The index of the first item returned on the page.
     * @type {number}
     * @memberof PagedListUserDetailsApplicationUser
     */
    startIndex?: number;
    /**
     * The index of the last item returned on the page.
     * @type {number}
     * @memberof PagedListUserDetailsApplicationUser
     */
    endIndex?: number;
}
/**
 * A list of parsed JQL queries.
 * @export
 * @interface ParsedJqlQueries
 */
export interface ParsedJqlQueries {
    /**
     * A list of parsed JQL queries.
     * @type {Array<ParsedJqlQuery>}
     * @memberof ParsedJqlQueries
     */
    queries: Array<ParsedJqlQuery>;
}
/**
 * Details of a parsed JQL query.
 * @export
 * @interface ParsedJqlQuery
 */
export interface ParsedJqlQuery {
    /**
     * The JQL query that was parsed and validated.
     * @type {string}
     * @memberof ParsedJqlQuery
     */
    query: string;
    /**
     * The syntax tree of the query. Empty if the query was invalid.
     * @type {JqlQuery}
     * @memberof ParsedJqlQuery
     */
    structure?: JqlQuery;
    /**
     * The list of syntax or validation errors.
     * @type {Array<string>}
     * @memberof ParsedJqlQuery
     */
    errors?: Array<string>;
}
/**
 * Details about a permission granted to a user or group.
 * @export
 * @interface PermissionGrant
 */
export interface PermissionGrant {
    /**
     * The ID of the permission granted details.
     * @type {number}
     * @memberof PermissionGrant
     */
    id?: number;
    /**
     * The URL of the permission granted details.
     * @type {string}
     * @memberof PermissionGrant
     */
    self?: string;
    /**
     * The user or group being granted the permission. It consists of a `type` and a type-dependent `parameter`. See [Holder object](#holder-object) in *Get all permission schemes* for more information.
     * @type {PermissionHolder}
     * @memberof PermissionGrant
     */
    holder?: PermissionHolder;
    /**
     * The permission to grant. This permission can be one of the built-in permissions or a custom permission added by an app. See [Built-in permissions](#built-in-permissions) in *Get all permission schemes* for more information about the built-in permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information about custom permissions.
     * @type {string}
     * @memberof PermissionGrant
     */
    permission?: string;
}
/**
 * List of permission grants.
 * @export
 * @interface PermissionGrants
 */
export interface PermissionGrants {
    /**
     * Permission grants list.
     * @type {Array<PermissionGrant>}
     * @memberof PermissionGrants
     */
    permissions?: Array<PermissionGrant>;
    /**
     * Expand options that include additional permission grant details in the response.
     * @type {string}
     * @memberof PermissionGrants
     */
    expand?: string;
}
/**
 * Details of a user, group, field, or project role that holds a permission. See [Holder object](#holder-object) in *Get all permission schemes* for more information.
 * @export
 * @interface PermissionHolder
 */
export interface PermissionHolder {
    /**
     * The type of permission holder.
     * @type {string}
     * @memberof PermissionHolder
     */
    type: string;
    /**
     * The identifier of permission holder.
     * @type {string}
     * @memberof PermissionHolder
     */
    parameter?: string;
    /**
     * Expand options that include additional permission holder details in the response.
     * @type {string}
     * @memberof PermissionHolder
     */
    expand?: string;
}
/**
 * Details of a permission scheme.
 * @export
 * @interface PermissionScheme
 */
export interface PermissionScheme {
    [key: string]: any;
}
/**
 * List of all permission schemes.
 * @export
 * @interface PermissionSchemes
 */
export interface PermissionSchemes {
    /**
     * Permission schemes list.
     * @type {Array<PermissionScheme>}
     * @memberof PermissionSchemes
     */
    permissionSchemes?: Array<PermissionScheme>;
}
/**
 * Details about permissions.
 * @export
 * @interface Permissions
 */
export interface Permissions {
    /**
     * List of permissions.
     * @type {{ [key: string]: UserPermission; }}
     * @memberof Permissions
     */
    permissions?: { [key: string]: UserPermission };
}
/**
 *
 * @export
 * @interface PermissionsKeysBean
 */
export interface PermissionsKeysBean {
    /**
     * A list of permission keys.
     * @type {Array<string>}
     * @memberof PermissionsKeysBean
     */
    permissions: Array<string>;
}
/**
 * A list of projects in which a user is granted permissions.
 * @export
 * @interface PermittedProjects
 */
export interface PermittedProjects {
    /**
     * A list of projects.
     * @type {Array<ProjectIdentifierBean>}
     * @memberof PermittedProjects
     */
    projects?: Array<ProjectIdentifierBean>;
}
/**
 * An issue priority.
 * @export
 * @interface Priority
 */
export interface Priority {
    [key: string]: any;
}
/**
 * Details about a project.
 * @export
 * @interface Project
 */
export interface Project {
    /**
     * Expand options that include additional project details in the response.
     * @type {string}
     * @memberof Project
     */
    expand?: string;
    /**
     * The URL of the project details.
     * @type {string}
     * @memberof Project
     */
    self?: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof Project
     */
    id?: string;
    /**
     * The key of the project.
     * @type {string}
     * @memberof Project
     */
    key?: string;
    /**
     * A brief description of the project.
     * @type {string}
     * @memberof Project
     */
    description?: string;
    /**
     * The username of the project lead.
     * @type {User}
     * @memberof Project
     */
    lead?: User;
    /**
     * List of the components contained in the project.
     * @type {Array<Component>}
     * @memberof Project
     */
    components?: Array<Component>;
    /**
     * List of the issue types available in the project.
     * @type {Array<IssueTypeDetails>}
     * @memberof Project
     */
    issueTypes?: Array<IssueTypeDetails>;
    /**
     * A link to information about this project, such as project documentation.
     * @type {string}
     * @memberof Project
     */
    url?: string;
    /**
     * An email address associated with the project.
     * @type {string}
     * @memberof Project
     */
    email?: string;
    /**
     * The default assignee when creating issues for this project.
     * @type {string}
     * @memberof Project
     */
    assigneeType?: Project.AssigneeTypeEnum;
    /**
     * The versions defined in the project. For more information, see [Create version](#api-rest-api-3-version-post).
     * @type {Array<Version>}
     * @memberof Project
     */
    versions?: Array<Version>;
    /**
     * The name of the project.
     * @type {string}
     * @memberof Project
     */
    name?: string;
    /**
     * The name and self URL for each role defined in the project. For more information, see [Create project role](#api-rest-api-3-role-post).
     * @type {{ [key: string]: string; }}
     * @memberof Project
     */
    roles?: { [key: string]: string };
    /**
     * The URLs of the project's avatars.
     * @type {AvatarUrlsBean}
     * @memberof Project
     */
    avatarUrls?: AvatarUrlsBean;
    /**
     * The category the project belongs to.
     * @type {ProjectCategory}
     * @memberof Project
     */
    projectCategory?: ProjectCategory;
    /**
     * The [project type](https://confluence.atlassian.com/x/GwiiLQ#Jiraapplicationsoverview-Productfeaturesandprojecttypes) of the project.
     * @type {string}
     * @memberof Project
     */
    projectTypeKey?: Project.ProjectTypeKeyEnum;
    /**
     * Whether the project is simplified.
     * @type {boolean}
     * @memberof Project
     */
    simplified?: boolean;
    /**
     * The type of the project.
     * @type {string}
     * @memberof Project
     */
    style?: Project.StyleEnum;
    /**
     * Whether the project is selected as a favorite.
     * @type {boolean}
     * @memberof Project
     */
    favourite?: boolean;
    /**
     * Whether the project is private.
     * @type {boolean}
     * @memberof Project
     */
    isPrivate?: boolean;
    /**
     * The issue type hierarchy for the project
     * @type {Hierarchy}
     * @memberof Project
     */
    issueTypeHierarchy?: Hierarchy;
    /**
     * User permissions on the project
     * @type {ProjectPermissions}
     * @memberof Project
     */
    permissions?: ProjectPermissions;
    /**
     * Map of project properties
     * @type {{ [key: string]: ModelObject; }}
     * @memberof Project
     */
    properties?: { [key: string]: ModelObject };
    /**
     * Unique ID for next-gen projects.
     * @type {string}
     * @memberof Project
     */
    uuid?: string;
    /**
     * Insights about the project.
     * @type {ProjectInsight}
     * @memberof Project
     */
    insight?: ProjectInsight;
    /**
     * Whether the project is marked as deleted.
     * @type {boolean}
     * @memberof Project
     */
    deleted?: boolean;
    /**
     * The date when the project is deleted permanently.
     * @type {Date}
     * @memberof Project
     */
    retentionTillDate?: Date;
    /**
     * The date when the project was marked as deleted.
     * @type {Date}
     * @memberof Project
     */
    deletedDate?: Date;
    /**
     * The user who marked the project as deleted.
     * @type {User}
     * @memberof Project
     */
    deletedBy?: User;
    /**
     * Whether the project is archived.
     * @type {boolean}
     * @memberof Project
     */
    archived?: boolean;
    /**
     * The date when the project was archived.
     * @type {Date}
     * @memberof Project
     */
    archivedDate?: Date;
    /**
     * The user who archived the project.
     * @type {User}
     * @memberof Project
     */
    archivedBy?: User;
}

/**
 * @export
 * @namespace Project
 */
export namespace Project {
    /**
     * @export
     * @enum {string}
     */
    export enum AssigneeTypeEnum {
        PROJECTLEAD = <any>'PROJECT_LEAD',
        UNASSIGNED = <any>'UNASSIGNED',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum ProjectTypeKeyEnum {
        Software = <any>'software',
        ServiceDesk = <any>'service_desk',
        Business = <any>'business',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum StyleEnum {
        Classic = <any>'classic',
        NextGen = <any>'next-gen',
    }
}
/**
 * List of project avatars.
 * @export
 * @interface ProjectAvatars
 */
export interface ProjectAvatars {
    /**
     * List of avatars included with Jira. These avatars cannot be deleted.
     * @type {Array<Avatar>}
     * @memberof ProjectAvatars
     */
    system?: Array<Avatar>;
    /**
     * List of avatars added to Jira. These avatars may be deleted.
     * @type {Array<Avatar>}
     * @memberof ProjectAvatars
     */
    custom?: Array<Avatar>;
}
/**
 * A project category.
 * @export
 * @interface ProjectCategory
 */
export interface ProjectCategory {
    /**
     * The URL of the project category.
     * @type {string}
     * @memberof ProjectCategory
     */
    self?: string;
    /**
     * The ID of the project category.
     * @type {string}
     * @memberof ProjectCategory
     */
    id?: string;
    /**
     * The name of the project category. Required on create, optional on update.
     * @type {string}
     * @memberof ProjectCategory
     */
    name?: string;
    /**
     * The description of the project category. Required on create, optional on update.
     * @type {string}
     * @memberof ProjectCategory
     */
    description?: string;
}
/**
 * A project's sender email address.
 * @export
 * @interface ProjectEmailAddress
 */
export interface ProjectEmailAddress {
    /**
     * The email address.
     * @type {string}
     * @memberof ProjectEmailAddress
     */
    emailAddress?: string;
}
/**
 * Details about a next-gen project.
 * @export
 * @interface ProjectForScope
 */
export interface ProjectForScope {
    /**
     * The URL of the project details.
     * @type {string}
     * @memberof ProjectForScope
     */
    self?: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof ProjectForScope
     */
    id?: string;
    /**
     * The key of the project.
     * @type {string}
     * @memberof ProjectForScope
     */
    key?: string;
    /**
     * The name of the project.
     * @type {string}
     * @memberof ProjectForScope
     */
    name?: string;
    /**
     * The [project type](https://confluence.atlassian.com/x/GwiiLQ#Jiraapplicationsoverview-Productfeaturesandprojecttypes) of the project.
     * @type {string}
     * @memberof ProjectForScope
     */
    projectTypeKey?: ProjectForScope.ProjectTypeKeyEnum;
    /**
     * Whether or not the project is simplified.
     * @type {boolean}
     * @memberof ProjectForScope
     */
    simplified?: boolean;
    /**
     * The URLs of the project's avatars.
     * @type {AvatarUrlsBean}
     * @memberof ProjectForScope
     */
    avatarUrls?: AvatarUrlsBean;
    /**
     * The category the project belongs to.
     * @type {UpdatedProjectCategory}
     * @memberof ProjectForScope
     */
    projectCategory?: UpdatedProjectCategory;
}

/**
 * @export
 * @namespace ProjectForScope
 */
export namespace ProjectForScope {
    /**
     * @export
     * @enum {string}
     */
    export enum ProjectTypeKeyEnum {
        Software = <any>'software',
        ServiceDesk = <any>'service_desk',
        Business = <any>'business',
    }
}
/**
 * The identifiers for a project.
 * @export
 * @interface ProjectIdentifierBean
 */
export interface ProjectIdentifierBean {
    /**
     * The ID of the project.
     * @type {number}
     * @memberof ProjectIdentifierBean
     */
    id?: number;
    /**
     * The key of the project.
     * @type {string}
     * @memberof ProjectIdentifierBean
     */
    key?: string;
}
/**
 * Identifiers for a project.
 * @export
 * @interface ProjectIdentifiers
 */
export interface ProjectIdentifiers {
    /**
     * The URL of the created project.
     * @type {string}
     * @memberof ProjectIdentifiers
     */
    self: string;
    /**
     * The ID of the created project.
     * @type {number}
     * @memberof ProjectIdentifiers
     */
    id: number;
    /**
     * The key of the created project.
     * @type {string}
     * @memberof ProjectIdentifiers
     */
    key: string;
}
/**
 *
 * @export
 * @interface ProjectInputBean
 */
export interface ProjectInputBean {
    /**
     * Project keys must be unique and start with an uppercase letter followed by one or more uppercase alphanumeric characters. The maximum length is 10 characters. Required when creating a project. Optional when updating a project.
     * @type {string}
     * @memberof ProjectInputBean
     */
    key?: string;
    /**
     * The name of the project. Required when creating a project. Optional when updating a project.
     * @type {string}
     * @memberof ProjectInputBean
     */
    name?: string;
    /**
     * The [project type](https://confluence.atlassian.com/x/GwiiLQ#Jiraapplicationsoverview-Productfeaturesandprojecttypes), which dictates the application-specific feature set. Required when creating a project. Not applicable for the Update project resource.
     * @type {string}
     * @memberof ProjectInputBean
     */
    projectTypeKey?: ProjectInputBean.ProjectTypeKeyEnum;
    /**
     * A prebuilt configuration for a project. The type of the `projectTemplateKey` must match with the type of the `projectTypeKey`. Required when creating a project. Not applicable for the Update project resource.
     * @type {string}
     * @memberof ProjectInputBean
     */
    projectTemplateKey?: ProjectInputBean.ProjectTemplateKeyEnum;
    /**
     * A brief description of the project.
     * @type {string}
     * @memberof ProjectInputBean
     */
    description?: string;
    /**
     * This parameter is deprecated because of privacy changes. Use `leadAccountId` instead. See the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details. The user name of the project lead. Either `lead` or `leadAccountId` must be set when creating a project. Optional when updating a project. Cannot be provided with `leadAccountId`.
     * @type {string}
     * @memberof ProjectInputBean
     */
    lead?: string;
    /**
     * The account ID of the project lead. Either `lead` or `leadAccountId` must be set when creating a project. Optional when updating a project. Cannot be provided with `lead`.
     * @type {string}
     * @memberof ProjectInputBean
     */
    leadAccountId?: string;
    /**
     * A link to information about this project, such as project documentation
     * @type {string}
     * @memberof ProjectInputBean
     */
    url?: string;
    /**
     * The default assignee when creating issues for this project.
     * @type {string}
     * @memberof ProjectInputBean
     */
    assigneeType?: ProjectInputBean.AssigneeTypeEnum;
    /**
     * An integer value for the project's avatar.
     * @type {number}
     * @memberof ProjectInputBean
     */
    avatarId?: number;
    /**
     * The ID of the issue security scheme for the project, which enables you to control who can and cannot view issues. Use the [Get issue security schemes](#api-rest-api-3-issuesecurityschemes-get) resource to get all issue security scheme IDs.
     * @type {number}
     * @memberof ProjectInputBean
     */
    issueSecurityScheme?: number;
    /**
     * The ID of the permission scheme for the project. Use the [Get all permission schemes](#api-rest-api-3-permissionscheme-get) resource to see a list of all permission scheme IDs.
     * @type {number}
     * @memberof ProjectInputBean
     */
    permissionScheme?: number;
    /**
     * The ID of the notification scheme for the project. Use the [Get notification schemes](#api-rest-api-3-notificationscheme-get) resource to get a list of notification scheme IDs.
     * @type {number}
     * @memberof ProjectInputBean
     */
    notificationScheme?: number;
    /**
     * The ID of the project's category. A complete list of category IDs is found using the [Get all project categories](#api-rest-api-3-projectCategory-get) operation.
     * @type {number}
     * @memberof ProjectInputBean
     */
    categoryId?: number;
}

/**
 * @export
 * @namespace ProjectInputBean
 */
export namespace ProjectInputBean {
    /**
     * @export
     * @enum {string}
     */
    export enum ProjectTypeKeyEnum {
        Software = <any>'software',
        ServiceDesk = <any>'service_desk',
        Business = <any>'business',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum ProjectTemplateKeyEnum {
        PyxisGreenhopperJiraghSimplifiedAgilityKanban = <any>'com.pyxis.greenhopper.jira:gh-simplified-agility-kanban',
        PyxisGreenhopperJiraghSimplifiedAgilityScrum = <any>'com.pyxis.greenhopper.jira:gh-simplified-agility-scrum',
        PyxisGreenhopperJiraghSimplifiedBasic = <any>'com.pyxis.greenhopper.jira:gh-simplified-basic',
        PyxisGreenhopperJiraghSimplifiedKanbanClassic = <any>'com.pyxis.greenhopper.jira:gh-simplified-kanban-classic',
        PyxisGreenhopperJiraghSimplifiedScrumClassic = <any>'com.pyxis.greenhopper.jira:gh-simplified-scrum-classic',
        AtlassianServicedesksimplifiedItServiceDesk = <any>'com.atlassian.servicedesk:simplified-it-service-desk',
        AtlassianServicedesksimplifiedInternalServiceDesk = <any>(
            'com.atlassian.servicedesk:simplified-internal-service-desk'
        ),
        AtlassianServicedesksimplifiedExternalServiceDesk = <any>(
            'com.atlassian.servicedesk:simplified-external-service-desk'
        ),
        AtlassianServicedesksimplifiedHrServiceDesk = <any>'com.atlassian.servicedesk:simplified-hr-service-desk',
        AtlassianServicedesksimplifiedFacilitiesServiceDesk = <any>(
            'com.atlassian.servicedesk:simplified-facilities-service-desk'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedContentManagement = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-content-management'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedDocumentApproval = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-document-approval'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedLeadTracking = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-lead-tracking'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedProcessControl = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-process-control'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedProcurement = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-procurement'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedProjectManagement = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-project-management'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedRecruitment = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-recruitment'
        ),
        AtlassianJiraCoreProjectTemplatesjiraCoreSimplifiedTask = <any>(
            'com.atlassian.jira-core-project-templates:jira-core-simplified-task-'
        ),
        AtlassianJiraJiraIncidentManagementPluginimIncidentManagement = <any>(
            'com.atlassian.jira.jira-incident-management-plugin:im-incident-management'
        ),
    }
    /**
     * @export
     * @enum {string}
     */
    export enum AssigneeTypeEnum {
        PROJECTLEAD = <any>'PROJECT_LEAD',
        UNASSIGNED = <any>'UNASSIGNED',
    }
}
/**
 * Additional details about a project.
 * @export
 * @interface ProjectInsight
 */
export interface ProjectInsight {
    /**
     * Total issue count.
     * @type {number}
     * @memberof ProjectInsight
     */
    totalIssueCount?: number;
    /**
     * The last issue update time.
     * @type {Date}
     * @memberof ProjectInsight
     */
    lastIssueUpdateTime?: Date;
}
/**
 * Details of the issue creation metadata for a project.
 * @export
 * @interface ProjectIssueCreateMetadata
 */
export interface ProjectIssueCreateMetadata {
    /**
     * Expand options that include additional project issue create metadata details in the response.
     * @type {string}
     * @memberof ProjectIssueCreateMetadata
     */
    expand?: string;
    /**
     * The URL of the project.
     * @type {string}
     * @memberof ProjectIssueCreateMetadata
     */
    self?: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof ProjectIssueCreateMetadata
     */
    id?: string;
    /**
     * The key of the project.
     * @type {string}
     * @memberof ProjectIssueCreateMetadata
     */
    key?: string;
    /**
     * The name of the project.
     * @type {string}
     * @memberof ProjectIssueCreateMetadata
     */
    name?: string;
    /**
     * List of the project's avatars, returning the avatar size and associated URL.
     * @type {AvatarUrlsBean}
     * @memberof ProjectIssueCreateMetadata
     */
    avatarUrls?: AvatarUrlsBean;
    /**
     * List of the issue types supported by the project.
     * @type {Array<IssueTypeIssueCreateMetadata>}
     * @memberof ProjectIssueCreateMetadata
     */
    issuetypes?: Array<IssueTypeIssueCreateMetadata>;
}
/**
 * List of issue level security items in a project.
 * @export
 * @interface ProjectIssueSecurityLevels
 */
export interface ProjectIssueSecurityLevels {
    /**
     * Issue level security items list.
     * @type {Array<SecurityLevel>}
     * @memberof ProjectIssueSecurityLevels
     */
    levels: Array<SecurityLevel>;
}
/**
 * The hierarchy of issue types within a project.
 * @export
 * @interface ProjectIssueTypeHierarchy
 */
export interface ProjectIssueTypeHierarchy {
    /**
     * The ID of the project.
     * @type {number}
     * @memberof ProjectIssueTypeHierarchy
     */
    projectId?: number;
    /**
     * Details of an issue type hierarchy level.
     * @type {Array<ProjectIssueTypesHierarchyLevel>}
     * @memberof ProjectIssueTypeHierarchy
     */
    hierarchy?: Array<ProjectIssueTypesHierarchyLevel>;
}
/**
 * Details of an issue type hierarchy level.
 * @export
 * @interface ProjectIssueTypesHierarchyLevel
 */
export interface ProjectIssueTypesHierarchyLevel {
    /**
     * The ID of the issue type hierarchy level.
     * @type {string}
     * @memberof ProjectIssueTypesHierarchyLevel
     */
    entityId?: string;
    /**
     * The level of the issue type hierarchy level.
     * @type {number}
     * @memberof ProjectIssueTypesHierarchyLevel
     */
    level?: number;
    /**
     * The name of the issue type hierarchy level.
     * @type {string}
     * @memberof ProjectIssueTypesHierarchyLevel
     */
    name?: string;
    /**
     * The list of issue types in the hierarchy level.
     * @type {Array<IssueTypeInfo>}
     * @memberof ProjectIssueTypesHierarchyLevel
     */
    issueTypes?: Array<IssueTypeInfo>;
}
/**
 * Permissions which a user has on a project.
 * @export
 * @interface ProjectPermissions
 */
export interface ProjectPermissions {
    /**
     * Whether the logged user can edit the project.
     * @type {boolean}
     * @memberof ProjectPermissions
     */
    canEdit?: boolean;
}
/**
 * Details about the roles in a project.
 * @export
 * @interface ProjectRole
 */
export interface ProjectRole {
    /**
     * The URL the project role details.
     * @type {string}
     * @memberof ProjectRole
     */
    self?: string;
    /**
     * The name of the project role.
     * @type {string}
     * @memberof ProjectRole
     */
    name?: string;
    /**
     * The ID of the project role.
     * @type {number}
     * @memberof ProjectRole
     */
    id?: number;
    /**
     * The description of the project role.
     * @type {string}
     * @memberof ProjectRole
     */
    description?: string;
    /**
     * The list of users who act in this role.
     * @type {Array<RoleActor>}
     * @memberof ProjectRole
     */
    actors?: Array<RoleActor>;
    /**
     * The scope of the role. Indicated for roles associated with [next-gen projects](https://confluence.atlassian.com/x/loMyO).
     * @type {Scope}
     * @memberof ProjectRole
     */
    scope?: Scope;
    /**
     * The translated name of the project role.
     * @type {string}
     * @memberof ProjectRole
     */
    translatedName?: string;
    /**
     * Whether the calling user is part of this role.
     * @type {boolean}
     * @memberof ProjectRole
     */
    currentUserRole?: boolean;
    /**
     * Whether this role is the admin role for the project.
     * @type {boolean}
     * @memberof ProjectRole
     */
    admin?: boolean;
    /**
     * Whether the roles are configurable for this project.
     * @type {boolean}
     * @memberof ProjectRole
     */
    roleConfigurable?: boolean;
    /**
     * Whether this role is the default role for the project
     * @type {boolean}
     * @memberof ProjectRole
     */
    _default?: boolean;
}
/**
 *
 * @export
 * @interface ProjectRoleActorsUpdateBean
 */
export interface ProjectRoleActorsUpdateBean {
    /**
     * The ID of the project role. Use [Get all project roles](#api-rest-api-3-role-get) to get a list of project role IDs.
     * @type {number}
     * @memberof ProjectRoleActorsUpdateBean
     */
    id?: number;
    /**
     * The actors to add to the project role. Add groups using `atlassian-group-role-actor` and a list of group names. For example, `\"atlassian-group-role-actor\":[\"another\",\"administrators\"]}`. Add users using `atlassian-user-role-actor` and a list of account IDs. For example, `\"atlassian-user-role-actor\":[\"12345678-9abc-def1-2345-6789abcdef12\", \"abcdef12-3456-789a-bcde-f123456789ab\"]`.
     * @type {{ [key: string]: Array<string>; }}
     * @memberof ProjectRoleActorsUpdateBean
     */
    categorisedActors?: { [key: string]: Array<string> };
}
/**
 * Details of the group associated with the role.
 * @export
 * @interface ProjectRoleGroup
 */
export interface ProjectRoleGroup {
    /**
     * The display name of the group.
     * @type {string}
     * @memberof ProjectRoleGroup
     */
    displayName?: string;
    /**
     * The name of the group
     * @type {string}
     * @memberof ProjectRoleGroup
     */
    name?: string;
}
/**
 * Details of the user associated with the role.
 * @export
 * @interface ProjectRoleUser
 */
export interface ProjectRoleUser {
    /**
     * The account ID of the user, which uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*. Returns *unknown* if the record is deleted and corrupted, for example, as the result of a server import.
     * @type {string}
     * @memberof ProjectRoleUser
     */
    accountId?: string;
}
/**
 *
 * @export
 * @interface ProjectScopeBean
 */
export interface ProjectScopeBean {
    /**
     * The ID of the project that the option's behavior applies to.
     * @type {number}
     * @memberof ProjectScopeBean
     */
    id?: number;
    /**
     * Defines the behavior of the option in the project.If notSelectable is set, the option cannot be set as the field's value. This is useful for archiving an option that has previously been selected but shouldn't be used anymore.If defaultValue is set, the option is selected by default.
     * @type {Array<string>}
     * @memberof ProjectScopeBean
     */
    attributes?: Array<ProjectScopeBean.AttributesEnum>;
}

/**
 * @export
 * @namespace ProjectScopeBean
 */
export namespace ProjectScopeBean {
    /**
     * @export
     * @enum {string}
     */
    export enum AttributesEnum {
        NotSelectable = <any>'notSelectable',
        DefaultValue = <any>'defaultValue',
    }
}
/**
 * Details about a project type.
 * @export
 * @interface ProjectType
 */
export interface ProjectType {
    /**
     * The key of the project type.
     * @type {string}
     * @memberof ProjectType
     */
    key?: string;
    /**
     * The formatted key of the project type.
     * @type {string}
     * @memberof ProjectType
     */
    formattedKey?: string;
    /**
     * The key of the project type's description.
     * @type {string}
     * @memberof ProjectType
     */
    descriptionI18nKey?: string;
    /**
     * The icon of the project type.
     * @type {string}
     * @memberof ProjectType
     */
    icon?: string;
    /**
     * The color of the project type.
     * @type {string}
     * @memberof ProjectType
     */
    color?: string;
}
/**
 * Property key details.
 * @export
 * @interface PropertyKey
 */
export interface PropertyKey {
    /**
     * The URL of the property.
     * @type {string}
     * @memberof PropertyKey
     */
    self?: string;
    /**
     * The key of the property.
     * @type {string}
     * @memberof PropertyKey
     */
    key?: string;
}
/**
 * List of property keys.
 * @export
 * @interface PropertyKeys
 */
export interface PropertyKeys {
    /**
     * Property key details.
     * @type {Array<PropertyKey>}
     * @memberof PropertyKeys
     */
    keys?: Array<PropertyKey>;
}
/**
 * Properties that identify a published workflow.
 * @export
 * @interface PublishedWorkflowId
 */
export interface PublishedWorkflowId {
    /**
     * The name of the workflow.
     * @type {string}
     * @memberof PublishedWorkflowId
     */
    name: string;
}
/**
 * ID of a registered webhook or error messages explaining why a webhook wasn't registered.
 * @export
 * @interface RegisteredWebhook
 */
export interface RegisteredWebhook {
    /**
     * The ID of the webhook. Returned if the webhook is created.
     * @type {number}
     * @memberof RegisteredWebhook
     */
    createdWebhookId?: number;
    /**
     * Error messages specifying why the webhook creation failed.
     * @type {Array<string>}
     * @memberof RegisteredWebhook
     */
    errors?: Array<string>;
}
/**
 * Details of an issue remote link.
 * @export
 * @interface RemoteIssueLink
 */
export interface RemoteIssueLink {
    /**
     * The ID of the link.
     * @type {number}
     * @memberof RemoteIssueLink
     */
    id?: number;
    /**
     * The URL of the link.
     * @type {string}
     * @memberof RemoteIssueLink
     */
    self?: string;
    /**
     * The global ID of the link, such as the ID of the item on the remote system.
     * @type {string}
     * @memberof RemoteIssueLink
     */
    globalId?: string;
    /**
     * Details of the remote application the linked item is in.
     * @type {Application}
     * @memberof RemoteIssueLink
     */
    application?: Application;
    /**
     * Description of the relationship between the issue and the linked item.
     * @type {string}
     * @memberof RemoteIssueLink
     */
    relationship?: string;
    /**
     * Details of the item linked to.
     * @type {RemoteObject}
     * @memberof RemoteIssueLink
     */
    object?: RemoteObject;
}
/**
 * Details of the identifiers for a created or updated remote issue link.
 * @export
 * @interface RemoteIssueLinkIdentifies
 */
export interface RemoteIssueLinkIdentifies {
    /**
     * The ID of the remote issue link, such as the ID of the item on the remote system.
     * @type {number}
     * @memberof RemoteIssueLinkIdentifies
     */
    id?: number;
    /**
     * The URL of the remote issue link.
     * @type {string}
     * @memberof RemoteIssueLinkIdentifies
     */
    self?: string;
}
/**
 * Details of a remote issue link.
 * @export
 * @interface RemoteIssueLinkRequest
 */
export interface RemoteIssueLinkRequest {
    [key: string]: any;
}
/**
 * The linked item.
 * @export
 * @interface RemoteObject
 */
export interface RemoteObject {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface RemoveOptionFromIssuesResult
 */
export interface RemoveOptionFromIssuesResult {
    /**
     * The IDs of the modified issues.
     * @type {Array<number>}
     * @memberof RemoveOptionFromIssuesResult
     */
    modifiedIssues?: Array<number>;
    /**
     * The IDs of the unchanged issues, those issues where errors prevent modification.
     * @type {Array<number>}
     * @memberof RemoveOptionFromIssuesResult
     */
    unmodifiedIssues?: Array<number>;
    /**
     * A collection of errors related to unchanged issues. The collection size is limited, which means not all errors may be returned.
     * @type {SimpleErrorCollection}
     * @memberof RemoveOptionFromIssuesResult
     */
    errors?: SimpleErrorCollection;
}
/**
 * Details of a custom field cascading option to rename.
 * @export
 * @interface RenamedCascadingOption
 */
export interface RenamedCascadingOption {
    /**
     * The value of the cascading option.
     * @type {string}
     * @memberof RenamedCascadingOption
     */
    value: string;
    /**
     * The new value of the cascading option.
     * @type {string}
     * @memberof RenamedCascadingOption
     */
    newValue: string;
}
/**
 * Details of a custom field option to rename.
 * @export
 * @interface RenamedOption
 */
export interface RenamedOption {
    /**
     * The current option value.
     * @type {string}
     * @memberof RenamedOption
     */
    value: string;
    /**
     * The new value of the option.
     * @type {string}
     * @memberof RenamedOption
     */
    newValue: string;
    /**
     * The new values for the cascading options of this option. Only used for Select List (cascading) fields.
     * @type {Array<RenamedCascadingOption>}
     * @memberof RenamedOption
     */
    cascadingOptions?: Array<RenamedCascadingOption>;
}
/**
 * Details of an issue resolution.
 * @export
 * @interface Resolution
 */
export interface Resolution {
    /**
     * The URL of the issue resolution.
     * @type {string}
     * @memberof Resolution
     */
    self?: string;
    /**
     * The ID of the issue resolution.
     * @type {string}
     * @memberof Resolution
     */
    id?: string;
    /**
     * The description of the issue resolution.
     * @type {string}
     * @memberof Resolution
     */
    description?: string;
    /**
     * The name of the issue resolution.
     * @type {string}
     * @memberof Resolution
     */
    name?: string;
}
/**
 * Details of the permission.
 * @export
 * @interface RestrictedPermission
 */
export interface RestrictedPermission {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface RichText
 */
export interface RichText {
    /**
     *
     * @type {boolean}
     * @memberof RichText
     */
    emptyAdf?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof RichText
     */
    valueSet?: boolean;
}
/**
 * Details about a user assigned to a project role.
 * @export
 * @interface RoleActor
 */
export interface RoleActor {
    /**
     * The ID of the role actor.
     * @type {number}
     * @memberof RoleActor
     */
    id?: number;
    /**
     * The display name of the role actor. For users, depending on the user’s privacy setting, this may return an alternative value for the user's name.
     * @type {string}
     * @memberof RoleActor
     */
    displayName?: string;
    /**
     * The type of role actor.
     * @type {string}
     * @memberof RoleActor
     */
    type?: RoleActor.TypeEnum;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof RoleActor
     */
    name?: string;
    /**
     * The avatar of the role actor.
     * @type {string}
     * @memberof RoleActor
     */
    avatarUrl?: string;
    /**
     *
     * @type {ProjectRoleUser}
     * @memberof RoleActor
     */
    actorUser?: ProjectRoleUser;
    /**
     *
     * @type {ProjectRoleGroup}
     * @memberof RoleActor
     */
    actorGroup?: ProjectRoleGroup;
}

/**
 * @export
 * @namespace RoleActor
 */
export namespace RoleActor {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        GroupRoleActor = <any>'atlassian-group-role-actor',
        UserRoleActor = <any>'atlassian-user-role-actor',
    }
}
/**
 * A rule configuration.
 * @export
 * @interface RuleConfiguration
 */
export interface RuleConfiguration {
    /**
     * Configuration of the rule, as it is stored by the Connect app on the rule configuration page.
     * @type {string}
     * @memberof RuleConfiguration
     */
    value: string;
}
/**
 * The projects the item is associated with. Indicated for items associated with [next-gen projects](https://confluence.atlassian.com/x/loMyO).
 * @export
 * @interface Scope
 */
export interface Scope {
    [key: string]: any;
}
/**
 * A screen.
 * @export
 * @interface Screen
 */
export interface Screen {
    /**
     * The ID of the screen.
     * @type {number}
     * @memberof Screen
     */
    id?: number;
    /**
     * The name of the screen.
     * @type {string}
     * @memberof Screen
     */
    name?: string;
    /**
     * The description of the screen.
     * @type {string}
     * @memberof Screen
     */
    description?: string;
    /**
     * The scope of the screen.
     * @type {Scope}
     * @memberof Screen
     */
    scope?: Scope;
}
/**
 * Details of a screen.
 * @export
 * @interface ScreenDetails
 */
export interface ScreenDetails {
    /**
     * The name of the screen. The name must be unique. The maximum length is 255 characters.
     * @type {string}
     * @memberof ScreenDetails
     */
    name: string;
    /**
     * The description of the screen. The maximum length is 255 characters.
     * @type {string}
     * @memberof ScreenDetails
     */
    description?: string;
}
/**
 * ID of a screen.
 * @export
 * @interface ScreenID
 */
export interface ScreenID {
    /**
     * The ID of the screen.
     * @type {string}
     * @memberof ScreenID
     */
    id: string;
}
/**
 * A screen scheme.
 * @export
 * @interface ScreenScheme
 */
export interface ScreenScheme {
    /**
     * The ID of the screen scheme.
     * @type {number}
     * @memberof ScreenScheme
     */
    id?: number;
    /**
     * The name of the screen scheme.
     * @type {string}
     * @memberof ScreenScheme
     */
    name?: string;
    /**
     * The description of the screen scheme.
     * @type {string}
     * @memberof ScreenScheme
     */
    description?: string;
    /**
     * The IDs of the screens for the screen types of the screen scheme.
     * @type {ScreenTypes}
     * @memberof ScreenScheme
     */
    screens?: ScreenTypes;
}
/**
 * Details of a screen scheme.
 * @export
 * @interface ScreenSchemeDetails
 */
export interface ScreenSchemeDetails {
    /**
     * The name of the screen scheme. The name must be unique. The maximum length is 255 characters.
     * @type {string}
     * @memberof ScreenSchemeDetails
     */
    name: string;
    /**
     * The description of the screen scheme. The maximum length is 255 characters.
     * @type {string}
     * @memberof ScreenSchemeDetails
     */
    description?: string;
    /**
     * The IDs of the screens for the screen types of the screen scheme. Only screens used in classic projects are accepted.
     * @type {ScreenTypes}
     * @memberof ScreenSchemeDetails
     */
    screens?: ScreenTypes;
}
/**
 * The ID of a screen scheme.
 * @export
 * @interface ScreenSchemeId
 */
export interface ScreenSchemeId {
    /**
     * The ID of the screen scheme.
     * @type {number}
     * @memberof ScreenSchemeId
     */
    id: number;
}
/**
 * The IDs of the screens for the screen types of the screen scheme.
 * @export
 * @interface ScreenTypes
 */
export interface ScreenTypes {
    /**
     * The ID of the edit screen.
     * @type {number}
     * @memberof ScreenTypes
     */
    edit?: number;
    /**
     * The ID of the create screen.
     * @type {number}
     * @memberof ScreenTypes
     */
    create?: number;
    /**
     * The ID of the view screen.
     * @type {number}
     * @memberof ScreenTypes
     */
    view?: number;
    /**
     * The ID of the default screen. Required when creating a screen scheme.
     * @type {number}
     * @memberof ScreenTypes
     */
    _default?: number;
}
/**
 * A screen tab field.
 * @export
 * @interface ScreenableField
 */
export interface ScreenableField {
    /**
     * The ID of the screen tab field.
     * @type {string}
     * @memberof ScreenableField
     */
    id?: string;
    /**
     * The name of the screen tab field. Required on create and update. The maximum length is 255 characters.
     * @type {string}
     * @memberof ScreenableField
     */
    name?: string;
}
/**
 * A screen tab.
 * @export
 * @interface ScreenableTab
 */
export interface ScreenableTab {
    /**
     * The ID of the screen tab.
     * @type {number}
     * @memberof ScreenableTab
     */
    id?: number;
    /**
     * The name of the screen tab. The maximum length is 255 characters.
     * @type {string}
     * @memberof ScreenableTab
     */
    name: string;
}
/**
 *
 * @export
 * @interface SearchRequestBean
 */
export interface SearchRequestBean {
    /**
     * A [JQL](https://confluence.atlassian.com/x/egORLQ) expression.
     * @type {string}
     * @memberof SearchRequestBean
     */
    jql?: string;
    /**
     * The index of the first item to return in the page of results (page offset). The base index is `0`.
     * @type {number}
     * @memberof SearchRequestBean
     */
    startAt?: number;
    /**
     * The maximum number of items to return per page.
     * @type {number}
     * @memberof SearchRequestBean
     */
    maxResults?: number;
    /**
     * A list of fields to return for each issue, use it to retrieve a subset of fields. This parameter accepts a comma-separated list. Expand options include:   *  `*all` Returns all fields.  *  `*navigable` Returns navigable fields.  *  Any issue field, prefixed with a minus to exclude.  The default is `*navigable`.  Examples:   *  `summary,comment` Returns the summary and comments fields only.  *  `-description` Returns all navigable (default) fields except description.  *  `*all,-comment` Returns all fields except comments.  Multiple `fields` parameters can be included in a request.  Note: All navigable fields are returned by default. This differs from [GET issue](#api-rest-api-3-issue-issueIdOrKey-get) where the default is all fields.
     * @type {Array<string>}
     * @memberof SearchRequestBean
     */
    fields?: Array<string>;
    /**
     * Determines how to validate the JQL query and treat the validation results. Supported values:   *  `strict` Returns a 400 response code if any errors are found, along with a list of all errors (and warnings).  *  `warn` Returns all errors as warnings.  *  `none` No validation is performed.  *  `true` *Deprecated* A legacy synonym for `strict`.  *  `false` *Deprecated* A legacy synonym for `warn`.  The default is `strict`.  Note: If the JQL is not correctly formed a 400 response code is returned, regardless of the `validateQuery` value.
     * @type {string}
     * @memberof SearchRequestBean
     */
    validateQuery?: SearchRequestBean.ValidateQueryEnum;
    /**
     * Use [expand](em>#expansion) to include additional information about issues in the response. Note that, unlike the majority of instances where `expand` is specified, `expand` is defined as a list of values. The expand options are:   *  `renderedFields` Returns field values rendered in HTML format.  *  `names` Returns the display name of each field.  *  `schema` Returns the schema describing a field type.  *  `transitions` Returns all possible transitions for the issue.  *  `operations` Returns all possible operations for the issue.  *  `editmeta` Returns information about how each field can be edited.  *  `changelog` Returns a list of recent updates to an issue, sorted by date, starting from the most recent.  *  `versionedRepresentations` Instead of `fields`, returns `versionedRepresentations` a JSON array containing each version of a field's value, with the highest numbered item representing the most recent version.
     * @type {Array<string>}
     * @memberof SearchRequestBean
     */
    expand?: Array<string>;
    /**
     * A list of up to 5 issue properties to include in the results. This parameter accepts a comma-separated list.
     * @type {Array<string>}
     * @memberof SearchRequestBean
     */
    properties?: Array<string>;
    /**
     * Reference fields by their key (rather than ID). The default is `false`.
     * @type {boolean}
     * @memberof SearchRequestBean
     */
    fieldsByKeys?: boolean;
}

/**
 * @export
 * @namespace SearchRequestBean
 */
export namespace SearchRequestBean {
    /**
     * @export
     * @enum {string}
     */
    export enum ValidateQueryEnum {
        Strict = <any>'strict',
        Warn = <any>'warn',
        None = <any>'none',
        True = <any>'true',
        False = <any>'false',
    }
}
/**
 * The result of a JQL search.
 * @export
 * @interface SearchResults
 */
export interface SearchResults {
    /**
     * Expand options that include additional search result details in the response.
     * @type {string}
     * @memberof SearchResults
     */
    expand?: string;
    /**
     * The index of the first item returned on the page.
     * @type {number}
     * @memberof SearchResults
     */
    startAt?: number;
    /**
     * The maximum number of results that could be on the page.
     * @type {number}
     * @memberof SearchResults
     */
    maxResults?: number;
    /**
     * The number of results on the page.
     * @type {number}
     * @memberof SearchResults
     */
    total?: number;
    /**
     * The list of issues found by the search.
     * @type {Array<IssueBean>}
     * @memberof SearchResults
     */
    issues?: Array<IssueBean>;
    /**
     * Any warnings related to the JQL query.
     * @type {Array<string>}
     * @memberof SearchResults
     */
    warningMessages?: Array<string>;
    /**
     * The ID and name of each field in the search results.
     * @type {{ [key: string]: string; }}
     * @memberof SearchResults
     */
    names?: { [key: string]: string };
    /**
     * The schema describing the field types in the search results.
     * @type {{ [key: string]: JsonTypeBean; }}
     * @memberof SearchResults
     */
    schema?: { [key: string]: JsonTypeBean };
}
/**
 * Details of an issue level security item.
 * @export
 * @interface SecurityLevel
 */
export interface SecurityLevel {
    /**
     * The URL of the issue level security item.
     * @type {string}
     * @memberof SecurityLevel
     */
    self?: string;
    /**
     * The ID of the issue level security item.
     * @type {string}
     * @memberof SecurityLevel
     */
    id?: string;
    /**
     * The description of the issue level security item.
     * @type {string}
     * @memberof SecurityLevel
     */
    description?: string;
    /**
     * The name of the issue level security item.
     * @type {string}
     * @memberof SecurityLevel
     */
    name?: string;
}
/**
 * Details about a security scheme.
 * @export
 * @interface SecurityScheme
 */
export interface SecurityScheme {
    /**
     * The URL of the issue security scheme.
     * @type {string}
     * @memberof SecurityScheme
     */
    self?: string;
    /**
     * The ID of the issue security scheme.
     * @type {number}
     * @memberof SecurityScheme
     */
    id?: number;
    /**
     * The name of the issue security scheme.
     * @type {string}
     * @memberof SecurityScheme
     */
    name?: string;
    /**
     * The description of the issue security scheme.
     * @type {string}
     * @memberof SecurityScheme
     */
    description?: string;
    /**
     * The ID of the default security level.
     * @type {number}
     * @memberof SecurityScheme
     */
    defaultSecurityLevelId?: number;
    /**
     *
     * @type {Array<SecurityLevel>}
     * @memberof SecurityScheme
     */
    levels?: Array<SecurityLevel>;
}
/**
 * List of security schemes.
 * @export
 * @interface SecuritySchemes
 */
export interface SecuritySchemes {
    /**
     * List of security schemes.
     * @type {Array<SecurityScheme>}
     * @memberof SecuritySchemes
     */
    issueSecuritySchemes?: Array<SecurityScheme>;
}
/**
 * Details about the Jira instance.
 * @export
 * @interface ServerInformation
 */
export interface ServerInformation {
    /**
     * The base URL of the Jira instance.
     * @type {string}
     * @memberof ServerInformation
     */
    baseUrl?: string;
    /**
     * The version of Jira.
     * @type {string}
     * @memberof ServerInformation
     */
    version?: string;
    /**
     * The major, minor, and revision version numbers of the Jira version.
     * @type {Array<number>}
     * @memberof ServerInformation
     */
    versionNumbers?: Array<number>;
    /**
     * The type of server deployment. This is always returned as *Cloud*.
     * @type {string}
     * @memberof ServerInformation
     */
    deploymentType?: string;
    /**
     * The build number of the Jira version.
     * @type {number}
     * @memberof ServerInformation
     */
    buildNumber?: number;
    /**
     * The timestamp when the Jira version was built.
     * @type {Date}
     * @memberof ServerInformation
     */
    buildDate?: Date;
    /**
     * The time in Jira when this request was responded to.
     * @type {Date}
     * @memberof ServerInformation
     */
    serverTime?: Date;
    /**
     * The unique identifier of the Jira version.
     * @type {string}
     * @memberof ServerInformation
     */
    scmInfo?: string;
    /**
     * The name of the Jira instance.
     * @type {string}
     * @memberof ServerInformation
     */
    serverTitle?: string;
    /**
     * Jira instance health check results. Deprecated and no longer returned.
     * @type {Array<HealthCheckResult>}
     * @memberof ServerInformation
     */
    healthChecks?: Array<HealthCheckResult>;
}
/**
 * Details of a share permission for the filter.
 * @export
 * @interface SharePermission
 */
export interface SharePermission {
    /**
     * The unique identifier of the share permission.
     * @type {number}
     * @memberof SharePermission
     */
    id?: number;
    /**
     * The type of share permission:   *  `group` Shared with a group. If set in a request, then specify `sharePermission.group` as well.  *  `project` Shared with a project. If set in a request, then specify `sharePermission.project` as well.  *  `projectRole` Share with a project role in a project. This value is not returned in responses. It is used in requests, where it needs to be specify with `projectId` and `projectRoleId`.  *  `global` Shared globally. If set in a request, no other `sharePermission` properties need to be specified.  *  `loggedin` Shared with all logged-in users. Note: This value is set in a request by specifying `authenticated` as the `type`.  *  `project-unknown` Shared with a project that the user does not have access to. Cannot be set in a request.
     * @type {string}
     * @memberof SharePermission
     */
    type: SharePermission.TypeEnum;
    /**
     * The project that the filter is shared with. This is similar to the project object returned by [Get project](#api-rest-api-3-project-projectIdOrKey-get) but it contains a subset of the properties, which are: `self`, `id`, `key`, `assigneeType`, `name`, `roles`, `avatarUrls`, `projectType`, `simplified`.   For a request, specify the `id` for the project.
     * @type {Project}
     * @memberof SharePermission
     */
    project?: Project;
    /**
     * The project role that the filter is shared with.   For a request, specify the `id` for the role. You must also specify the `project` object and `id` for the project that the role is in.
     * @type {ProjectRole}
     * @memberof SharePermission
     */
    role?: ProjectRole;
    /**
     * The group that the filter is shared with. For a request, specify the `name` property for the group.
     * @type {GroupName}
     * @memberof SharePermission
     */
    group?: GroupName;
}

/**
 * @export
 * @namespace SharePermission
 */
export namespace SharePermission {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Group = <any>'group',
        Project = <any>'project',
        ProjectRole = <any>'projectRole',
        Global = <any>'global',
        Loggedin = <any>'loggedin',
        Authenticated = <any>'authenticated',
        ProjectUnknown = <any>'project-unknown',
    }
}
/**
 *
 * @export
 * @interface SharePermissionInputBean
 */
export interface SharePermissionInputBean {
    /**
     * The type of the share permission.Specify the type as follows:   *  `group` Share with a group. Specify `groupname` as well.  *  `project` Share with a project. Specify `projectId` as well.  *  `projectRole` Share with a project role in a project. Specify `projectId` and `projectRoleId` as well.  *  `global` Share globally, including anonymous users. If set, this type overrides all existing share permissions and must be deleted before any non-global share permissions is set.  *  `authenticated` Share with all logged-in users. This shows as `loggedin` in the response. If set, this type overrides all existing share permissions and must be deleted before any non-global share permissions is set.
     * @type {string}
     * @memberof SharePermissionInputBean
     */
    type: SharePermissionInputBean.TypeEnum;
    /**
     * The ID of the project to share the filter with. Set `type` to `project`.
     * @type {string}
     * @memberof SharePermissionInputBean
     */
    projectId?: string;
    /**
     * The name of the group to share the filter with. Set `type` to `group`.
     * @type {string}
     * @memberof SharePermissionInputBean
     */
    groupname?: string;
    /**
     * The ID of the project role to share the filter with. Set `type` to `projectRole` and the `projectId` for the project that the role is in.
     * @type {string}
     * @memberof SharePermissionInputBean
     */
    projectRoleId?: string;
}

/**
 * @export
 * @namespace SharePermissionInputBean
 */
export namespace SharePermissionInputBean {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Project = <any>'project',
        Group = <any>'group',
        ProjectRole = <any>'projectRole',
        Global = <any>'global',
        Authenticated = <any>'authenticated',
    }
}
/**
 *
 * @export
 * @interface SimpleApplicationPropertyBean
 */
export interface SimpleApplicationPropertyBean {
    /**
     * The ID of the application property.
     * @type {string}
     * @memberof SimpleApplicationPropertyBean
     */
    id?: string;
    /**
     * The new value.
     * @type {string}
     * @memberof SimpleApplicationPropertyBean
     */
    value?: string;
}
/**
 *
 * @export
 * @interface SimpleErrorCollection
 */
export interface SimpleErrorCollection {
    /**
     * The list of errors by parameter returned by the operation. For example,\"projectKey\": \"Project keys must start with an uppercase letter, followed by one or more uppercase alphanumeric characters.\"
     * @type {{ [key: string]: string; }}
     * @memberof SimpleErrorCollection
     */
    errors?: { [key: string]: string };
    /**
     * The list of error messages produced by this operation. For example, \"input parameter 'key' must be provided\"
     * @type {Array<string>}
     * @memberof SimpleErrorCollection
     */
    errorMessages?: Array<string>;
    /**
     *
     * @type {number}
     * @memberof SimpleErrorCollection
     */
    httpStatusCode?: number;
}
/**
 * Details about the operations available in this version.
 * @export
 * @interface SimpleLink
 */
export interface SimpleLink {
    /**
     *
     * @type {string}
     * @memberof SimpleLink
     */
    id?: string;
    /**
     *
     * @type {string}
     * @memberof SimpleLink
     */
    styleClass?: string;
    /**
     *
     * @type {string}
     * @memberof SimpleLink
     */
    iconClass?: string;
    /**
     *
     * @type {string}
     * @memberof SimpleLink
     */
    label?: string;
    /**
     *
     * @type {string}
     * @memberof SimpleLink
     */
    title?: string;
    /**
     *
     * @type {string}
     * @memberof SimpleLink
     */
    href?: string;
    /**
     *
     * @type {number}
     * @memberof SimpleLink
     */
    weight?: number;
}
/**
 *
 * @export
 * @interface SimpleListWrapperApplicationRole
 */
export interface SimpleListWrapperApplicationRole {
    /**
     *
     * @type {number}
     * @memberof SimpleListWrapperApplicationRole
     */
    size?: number;
    /**
     *
     * @type {Array<ApplicationRole>}
     * @memberof SimpleListWrapperApplicationRole
     */
    items?: Array<ApplicationRole>;
    /**
     *
     * @type {ListWrapperCallbackApplicationRole}
     * @memberof SimpleListWrapperApplicationRole
     */
    pagingCallback?: ListWrapperCallbackApplicationRole;
    /**
     *
     * @type {ListWrapperCallbackApplicationRole}
     * @memberof SimpleListWrapperApplicationRole
     */
    callback?: ListWrapperCallbackApplicationRole;
    /**
     *
     * @type {number}
     * @memberof SimpleListWrapperApplicationRole
     */
    maxResults?: number;
}
/**
 *
 * @export
 * @interface SimpleListWrapperGroupName
 */
export interface SimpleListWrapperGroupName {
    /**
     *
     * @type {number}
     * @memberof SimpleListWrapperGroupName
     */
    size?: number;
    /**
     *
     * @type {Array<GroupName>}
     * @memberof SimpleListWrapperGroupName
     */
    items?: Array<GroupName>;
    /**
     *
     * @type {ListWrapperCallbackGroupName}
     * @memberof SimpleListWrapperGroupName
     */
    pagingCallback?: ListWrapperCallbackGroupName;
    /**
     *
     * @type {ListWrapperCallbackGroupName}
     * @memberof SimpleListWrapperGroupName
     */
    callback?: ListWrapperCallbackGroupName;
    /**
     *
     * @type {number}
     * @memberof SimpleListWrapperGroupName
     */
    maxResults?: number;
}
/**
 * The status of the item.
 * @export
 * @interface Status
 */
export interface Status {
    [key: string]: any;
}
/**
 * A status category.
 * @export
 * @interface StatusCategory
 */
export interface StatusCategory {
    [key: string]: any;
}
/**
 * A status.
 * @export
 * @interface StatusDetails
 */
export interface StatusDetails {
    [key: string]: any;
}
/**
 * An issue suggested for use in the issue picker auto-completion.
 * @export
 * @interface SuggestedIssue
 */
export interface SuggestedIssue {
    /**
     * The ID of the issue.
     * @type {number}
     * @memberof SuggestedIssue
     */
    id?: number;
    /**
     * The key of the issue.
     * @type {string}
     * @memberof SuggestedIssue
     */
    key?: string;
    /**
     * The key of the issue in HTML format.
     * @type {string}
     * @memberof SuggestedIssue
     */
    keyHtml?: string;
    /**
     * The URL of the issue type's avatar.
     * @type {string}
     * @memberof SuggestedIssue
     */
    img?: string;
    /**
     * The phrase containing the query string in HTML format, with the string highlighted with HTML bold tags.
     * @type {string}
     * @memberof SuggestedIssue
     */
    summary?: string;
    /**
     * The phrase containing the query string, as plain text.
     * @type {string}
     * @memberof SuggestedIssue
     */
    summaryText?: string;
}
/**
 * List of system avatars.
 * @export
 * @interface SystemAvatars
 */
export interface SystemAvatars {
    /**
     * A list of avatar details.
     * @type {Array<Avatar>}
     * @memberof SystemAvatars
     */
    system?: Array<Avatar>;
}
/**
 * Details about a task.
 * @export
 * @interface TaskProgressBeanObject
 */
export interface TaskProgressBeanObject {
    /**
     * The URL of the task.
     * @type {string}
     * @memberof TaskProgressBeanObject
     */
    self: string;
    /**
     * The ID of the task.
     * @type {string}
     * @memberof TaskProgressBeanObject
     */
    id: string;
    /**
     * The description of the task.
     * @type {string}
     * @memberof TaskProgressBeanObject
     */
    description?: string;
    /**
     * The status of the task.
     * @type {string}
     * @memberof TaskProgressBeanObject
     */
    status: TaskProgressBeanObject.StatusEnum;
    /**
     * Information about the progress of the task.
     * @type {string}
     * @memberof TaskProgressBeanObject
     */
    message?: string;
    /**
     * The result of the task execution.
     * @type {ModelObject}
     * @memberof TaskProgressBeanObject
     */
    result?: ModelObject;
    /**
     * The ID of the user who submitted the task.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    submittedBy: number;
    /**
     * The progress of the task, as a percentage complete.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    progress: number;
    /**
     * The execution time of the task, in milliseconds.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    elapsedRuntime: number;
    /**
     * A timestamp recording when the task was submitted.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    submitted: number;
    /**
     * A timestamp recording when the task was started.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    started?: number;
    /**
     * A timestamp recording when the task was finished.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    finished?: number;
    /**
     * A timestamp recording when the task progress was last updated.
     * @type {number}
     * @memberof TaskProgressBeanObject
     */
    lastUpdate: number;
}

/**
 * @export
 * @namespace TaskProgressBeanObject
 */
export namespace TaskProgressBeanObject {
    /**
     * @export
     * @enum {string}
     */
    export enum StatusEnum {
        ENQUEUED = <any>'ENQUEUED',
        RUNNING = <any>'RUNNING',
        COMPLETE = <any>'COMPLETE',
        FAILED = <any>'FAILED',
        CANCELREQUESTED = <any>'CANCEL_REQUESTED',
        CANCELLED = <any>'CANCELLED',
        DEAD = <any>'DEAD',
    }
}
/**
 * Details about a task.
 * @export
 * @interface TaskProgressBeanRemoveOptionFromIssuesResult
 */
export interface TaskProgressBeanRemoveOptionFromIssuesResult {
    /**
     * The URL of the task.
     * @type {string}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    self: string;
    /**
     * The ID of the task.
     * @type {string}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    id: string;
    /**
     * The description of the task.
     * @type {string}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    description?: string;
    /**
     * The status of the task.
     * @type {string}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    status: TaskProgressBeanRemoveOptionFromIssuesResult.StatusEnum;
    /**
     * Information about the progress of the task.
     * @type {string}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    message?: string;
    /**
     * The result of the task execution.
     * @type {RemoveOptionFromIssuesResult}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    result?: RemoveOptionFromIssuesResult;
    /**
     * The ID of the user who submitted the task.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    submittedBy: number;
    /**
     * The progress of the task, as a percentage complete.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    progress: number;
    /**
     * The execution time of the task, in milliseconds.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    elapsedRuntime: number;
    /**
     * A timestamp recording when the task was submitted.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    submitted: number;
    /**
     * A timestamp recording when the task was started.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    started?: number;
    /**
     * A timestamp recording when the task was finished.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    finished?: number;
    /**
     * A timestamp recording when the task progress was last updated.
     * @type {number}
     * @memberof TaskProgressBeanRemoveOptionFromIssuesResult
     */
    lastUpdate: number;
}

/**
 * @export
 * @namespace TaskProgressBeanRemoveOptionFromIssuesResult
 */
export namespace TaskProgressBeanRemoveOptionFromIssuesResult {
    /**
     * @export
     * @enum {string}
     */
    export enum StatusEnum {
        ENQUEUED = <any>'ENQUEUED',
        RUNNING = <any>'RUNNING',
        COMPLETE = <any>'COMPLETE',
        FAILED = <any>'FAILED',
        CANCELREQUESTED = <any>'CANCEL_REQUESTED',
        CANCELLED = <any>'CANCELLED',
        DEAD = <any>'DEAD',
    }
}
/**
 * Details of the time tracking configuration.
 * @export
 * @interface TimeTrackingConfiguration
 */
export interface TimeTrackingConfiguration {
    /**
     * The number of hours in a working day.
     * @type {number}
     * @memberof TimeTrackingConfiguration
     */
    workingHoursPerDay: number;
    /**
     * The number of days in a working week.
     * @type {number}
     * @memberof TimeTrackingConfiguration
     */
    workingDaysPerWeek: number;
    /**
     * The format that will appear on an issue's *Time Spent* field.
     * @type {string}
     * @memberof TimeTrackingConfiguration
     */
    timeFormat: TimeTrackingConfiguration.TimeFormatEnum;
    /**
     * The default unit of time applied to logged time.
     * @type {string}
     * @memberof TimeTrackingConfiguration
     */
    defaultUnit: TimeTrackingConfiguration.DefaultUnitEnum;
}

/**
 * @export
 * @namespace TimeTrackingConfiguration
 */
export namespace TimeTrackingConfiguration {
    /**
     * @export
     * @enum {string}
     */
    export enum TimeFormatEnum {
        Pretty = <any>'pretty',
        Days = <any>'days',
        Hours = <any>'hours',
    }
    /**
     * @export
     * @enum {string}
     */
    export enum DefaultUnitEnum {
        Minute = <any>'minute',
        Hour = <any>'hour',
        Day = <any>'day',
        Week = <any>'week',
    }
}
/**
 * Time tracking details.
 * @export
 * @interface TimeTrackingDetails
 */
export interface TimeTrackingDetails {
    /**
     * The original estimate of time needed for this issue in readable format.
     * @type {string}
     * @memberof TimeTrackingDetails
     */
    originalEstimate?: string;
    /**
     * The remaining estimate of time needed for this issue in readable format.
     * @type {string}
     * @memberof TimeTrackingDetails
     */
    remainingEstimate?: string;
    /**
     * Time worked on this issue in readable format.
     * @type {string}
     * @memberof TimeTrackingDetails
     */
    timeSpent?: string;
    /**
     * The original estimate of time needed for this issue in seconds.
     * @type {number}
     * @memberof TimeTrackingDetails
     */
    originalEstimateSeconds?: number;
    /**
     * The remaining estimate of time needed for this issue in seconds.
     * @type {number}
     * @memberof TimeTrackingDetails
     */
    remainingEstimateSeconds?: number;
    /**
     * Time worked on this issue in seconds.
     * @type {number}
     * @memberof TimeTrackingDetails
     */
    timeSpentSeconds?: number;
}
/**
 * Details about the time tracking provider.
 * @export
 * @interface TimeTrackingProvider
 */
export interface TimeTrackingProvider {
    /**
     * The key for the time tracking provider. For example, *JIRA*.
     * @type {string}
     * @memberof TimeTrackingProvider
     */
    key: string;
    /**
     * The name of the time tracking provider. For example, *JIRA provided time tracking*.
     * @type {string}
     * @memberof TimeTrackingProvider
     */
    name?: string;
    /**
     * The URL of the configuration page for the time tracking provider app. For example, *_/example/config/url*. This property is only returned if the `adminPageKey` property is set in the module descriptor of the time tracking provider app.
     * @type {string}
     * @memberof TimeTrackingProvider
     */
    url?: string;
}
/**
 * Details of a workflow transition.
 * @export
 * @interface Transition
 */
export interface Transition {
    /**
     * The ID of the transition.
     * @type {string}
     * @memberof Transition
     */
    id: string;
    /**
     * The name of the transition.
     * @type {string}
     * @memberof Transition
     */
    name: string;
    /**
     * The description of the transition.
     * @type {string}
     * @memberof Transition
     */
    description: string;
    /**
     * The statuses the transition can start from.
     * @type {Array<string>}
     * @memberof Transition
     */
    from: Array<string>;
    /**
     * The status the transition goes to.
     * @type {string}
     * @memberof Transition
     */
    to: string;
    /**
     * The type of the transition.
     * @type {string}
     * @memberof Transition
     */
    type: Transition.TypeEnum;
    /**
     *
     * @type {ScreenID}
     * @memberof Transition
     */
    screen?: ScreenID;
    /**
     *
     * @type {WorkflowRules}
     * @memberof Transition
     */
    rules?: WorkflowRules;
}

/**
 * @export
 * @namespace Transition
 */
export namespace Transition {
    /**
     * @export
     * @enum {string}
     */
    export enum TypeEnum {
        Global = <any>'global',
        Initial = <any>'initial',
        Directed = <any>'directed',
    }
}
/**
 * List of issue transitions.
 * @export
 * @interface Transitions
 */
export interface Transitions {
    /**
     * Expand options that include additional transitions details in the response.
     * @type {string}
     * @memberof Transitions
     */
    expand?: string;
    /**
     * List of issue transitions.
     * @type {Array<IssueTransition>}
     * @memberof Transitions
     */
    transitions?: Array<IssueTransition>;
}
/**
 *
 * @export
 * @interface UnrestrictedUserEmail
 */
export interface UnrestrictedUserEmail {
    /**
     * The accountId of the user
     * @type {string}
     * @memberof UnrestrictedUserEmail
     */
    accountId?: string;
    /**
     * The email of the user
     * @type {string}
     * @memberof UnrestrictedUserEmail
     */
    email?: string;
}
/**
 * Details of the options to update for a custom field.
 * @export
 * @interface UpdateCustomFieldOption
 */
export interface UpdateCustomFieldOption {
    /**
     * Details of the options to update.
     * @type {Array<RenamedOption>}
     * @memberof UpdateCustomFieldOption
     */
    options?: Array<RenamedOption>;
}
/**
 *
 * @export
 * @interface UpdateUserToGroupBean
 */
export interface UpdateUserToGroupBean {
    [key: string]: any;
}
/**
 * A project category.
 * @export
 * @interface UpdatedProjectCategory
 */
export interface UpdatedProjectCategory {
    /**
     * The URL of the project category.
     * @type {string}
     * @memberof UpdatedProjectCategory
     */
    self?: string;
    /**
     * The ID of the project category.
     * @type {string}
     * @memberof UpdatedProjectCategory
     */
    id?: string;
    /**
     * The name of the project category.
     * @type {string}
     * @memberof UpdatedProjectCategory
     */
    description?: string;
    /**
     * The description of the project category.
     * @type {string}
     * @memberof UpdatedProjectCategory
     */
    name?: string;
}
/**
 * A user with details as permitted by the user's Atlassian Account privacy settings. However, be aware of these exceptions:   *  User record deleted from Atlassian: This occurs as the result of a right to be forgotten request. In this case, `displayName` provides an indication and other parameters have default values or are blank (for example, email is blank).  *  User record corrupted: This occurs as a results of events such as a server import and can only happen to deleted users. In this case, `accountId` returns *unknown* and all other parameters have fallback values.  *  User record unavailable: This usually occurs due to an internal service outage. In this case, all parameters have fallback values.
 * @export
 * @interface User
 */
export interface User {
    /**
     * The URL of the user.
     * @type {string}
     * @memberof User
     */
    self?: string;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof User
     */
    key?: string;
    /**
     * The account ID of the user, which uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*. Required in requests.
     * @type {string}
     * @memberof User
     */
    accountId?: string;
    /**
     * The user account type. Can take the following values:   *  `atlassian` regular Atlassian user account  *  `app` system account used for Connect applications and OAuth to represent external systems  *  `customer` Jira Service Desk account representing an external service desk
     * @type {string}
     * @memberof User
     */
    accountType?: User.AccountTypeEnum;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof User
     */
    name?: string;
    /**
     * The email address of the user. Depending on the user’s privacy setting, this may be returned as null.
     * @type {string}
     * @memberof User
     */
    emailAddress?: string;
    /**
     * The avatars of the user.
     * @type {AvatarUrlsBean}
     * @memberof User
     */
    avatarUrls?: AvatarUrlsBean;
    /**
     * The display name of the user. Depending on the user’s privacy setting, this may return an alternative value.
     * @type {string}
     * @memberof User
     */
    displayName?: string;
    /**
     * Whether the user is active.
     * @type {boolean}
     * @memberof User
     */
    active?: boolean;
    /**
     * The time zone specified in the user's profile. Depending on the user’s privacy setting, this may be returned as null.
     * @type {string}
     * @memberof User
     */
    timeZone?: string;
    /**
     * The locale of the user. Depending on the user’s privacy setting, this may be returned as null.
     * @type {string}
     * @memberof User
     */
    locale?: string;
    /**
     * The groups that the user belongs to.
     * @type {SimpleListWrapperGroupName}
     * @memberof User
     */
    groups?: SimpleListWrapperGroupName;
    /**
     * The application roles the user is assigned to.
     * @type {SimpleListWrapperApplicationRole}
     * @memberof User
     */
    applicationRoles?: SimpleListWrapperApplicationRole;
    /**
     * Expand options that include additional user details in the response.
     * @type {string}
     * @memberof User
     */
    expand?: string;
}

/**
 * @export
 * @namespace User
 */
export namespace User {
    /**
     * @export
     * @enum {string}
     */
    export enum AccountTypeEnum {
        Atlassian = <any>'atlassian',
        App = <any>'app',
        Customer = <any>'customer',
        Unknown = <any>'unknown',
    }
}
/**
 *
 * @export
 * @interface UserBean
 */
export interface UserBean {
    /**
     * This property is deprecated in favor of `accountId` because of privacy changes. See the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.   The key of the user.
     * @type {string}
     * @memberof UserBean
     */
    key?: string;
    /**
     * The URL of the user.
     * @type {string}
     * @memberof UserBean
     */
    self?: string;
    /**
     * This property is deprecated in favor of `accountId` because of privacy changes. See the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.   The username of the user.
     * @type {string}
     * @memberof UserBean
     */
    name?: string;
    /**
     * The display name of the user. Depending on the user’s privacy setting, this may return an alternative value.
     * @type {string}
     * @memberof UserBean
     */
    displayName?: string;
    /**
     * Whether the user is active.
     * @type {boolean}
     * @memberof UserBean
     */
    active?: boolean;
    /**
     * The account ID of the user, which uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*.
     * @type {string}
     * @memberof UserBean
     */
    accountId?: string;
    /**
     * The avatars of the user.
     * @type {UserBeanAvatarUrls}
     * @memberof UserBean
     */
    avatarUrls?: UserBeanAvatarUrls;
}
/**
 *
 * @export
 * @interface UserBeanAvatarUrls
 */
export interface UserBeanAvatarUrls {
    /**
     * The URL of the user's 24x24 pixel avatar.
     * @type {string}
     * @memberof UserBeanAvatarUrls
     */
    _24x24?: string;
    /**
     * The URL of the user's 32x32 pixel avatar.
     * @type {string}
     * @memberof UserBeanAvatarUrls
     */
    _32x32?: string;
    /**
     * The URL of the user's 48x48 pixel avatar.
     * @type {string}
     * @memberof UserBeanAvatarUrls
     */
    _48x48?: string;
    /**
     * The URL of the user's 16x16 pixel avatar.
     * @type {string}
     * @memberof UserBeanAvatarUrls
     */
    _16x16?: string;
}
/**
 * User details permitted by the user's Atlassian Account privacy settings. However, be aware of these exceptions:   *  User record deleted from Atlassian: This occurs as the result of a right to be forgotten request. In this case, `displayName` provides an indication and other parameters have default values or are blank (for example, email is blank).  *  User record corrupted: This occurs as a results of events such as a server import and can only happen to deleted users. In this case, `accountId` returns *unknown* and all other parameters have fallback values.  *  User record unavailable: This usually occurs due to an internal service outage. In this case, all parameters have fallback values.
 * @export
 * @interface UserDetails
 */
export interface UserDetails {
    /**
     * The URL of the user.
     * @type {string}
     * @memberof UserDetails
     */
    self?: string;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof UserDetails
     */
    name?: string;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof UserDetails
     */
    key?: string;
    /**
     * The account ID of the user, which uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*.
     * @type {string}
     * @memberof UserDetails
     */
    accountId?: string;
    /**
     * The email address of the user. Depending on the user’s privacy settings, this may be returned as null.
     * @type {string}
     * @memberof UserDetails
     */
    emailAddress?: string;
    /**
     * The avatars of the user.
     * @type {AvatarUrlsBean}
     * @memberof UserDetails
     */
    avatarUrls?: AvatarUrlsBean;
    /**
     * The display name of the user. Depending on the user’s privacy settings, this may return an alternative value.
     * @type {string}
     * @memberof UserDetails
     */
    displayName?: string;
    /**
     * Whether the user is active.
     * @type {boolean}
     * @memberof UserDetails
     */
    active?: boolean;
    /**
     * The time zone specified in the user's profile. Depending on the user’s privacy settings, this may be returned as null.
     * @type {string}
     * @memberof UserDetails
     */
    timeZone?: string;
    /**
     * The type of account represented by this user. This will be one of 'atlassian' (normal users), 'app' (application user) or 'customer' (Jira Service Desk customer user)
     * @type {string}
     * @memberof UserDetails
     */
    accountType?: string;
}
/**
 * List of user account IDs.
 * @export
 * @interface UserKey
 */
export interface UserKey {
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof UserKey
     */
    key?: string;
    /**
     * The account ID of the user, which uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*. Returns *unknown* if the record is deleted and corrupted, for example, as the result of a server import.
     * @type {string}
     * @memberof UserKey
     */
    accountId?: string;
}
/**
 * A paginated list of users sharing the filter. This includes users that are members of the groups or can browse the projects that the filter is shared with.
 * @export
 * @interface UserList
 */
export interface UserList {
    /**
     * The number of items on the page.
     * @type {number}
     * @memberof UserList
     */
    size?: number;
    /**
     * The list of items.
     * @type {Array<User>}
     * @memberof UserList
     */
    items?: Array<User>;
    /**
     * The maximum number of results that could be on the page.
     * @type {number}
     * @memberof UserList
     */
    maxResults?: number;
    /**
     * The index of the first item returned on the page.
     * @type {number}
     * @memberof UserList
     */
    startIndex?: number;
    /**
     * The index of the last item returned on the page.
     * @type {number}
     * @memberof UserList
     */
    endIndex?: number;
}
/**
 *
 * @export
 * @interface UserMigrationBean
 */
export interface UserMigrationBean {
    /**
     *
     * @type {string}
     * @memberof UserMigrationBean
     */
    key?: string;
    /**
     *
     * @type {string}
     * @memberof UserMigrationBean
     */
    username?: string;
    /**
     *
     * @type {string}
     * @memberof UserMigrationBean
     */
    accountId?: string;
}
/**
 * Details of a permission and its availability to a user.
 * @export
 * @interface UserPermission
 */
export interface UserPermission {
    [key: string]: any;
}
/**
 * A user found in a search.
 * @export
 * @interface UserPickerUser
 */
export interface UserPickerUser {
    /**
     * The account ID of the user, which uniquely identifies the user across all Atlassian products. For example, *5b10ac8d82e05b22cc7d4ef5*.
     * @type {string}
     * @memberof UserPickerUser
     */
    accountId?: string;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof UserPickerUser
     */
    name?: string;
    /**
     * This property is no longer available and will be removed from the documentation soon. See the [deprecation notice](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/) for details.
     * @type {string}
     * @memberof UserPickerUser
     */
    key?: string;
    /**
     * The display name, email address, and key of the user with the matched query string highlighted with the HTML bold tag.
     * @type {string}
     * @memberof UserPickerUser
     */
    html?: string;
    /**
     * The display name of the user. Depending on the user’s privacy setting, this may be returned as null.
     * @type {string}
     * @memberof UserPickerUser
     */
    displayName?: string;
    /**
     * The avatar URL of the user.
     * @type {string}
     * @memberof UserPickerUser
     */
    avatarUrl?: string;
}
/**
 *
 * @export
 * @interface UserWriteBean
 */
export interface UserWriteBean {
    [key: string]: any;
}
/**
 * An operand that is a user-provided value.
 * @export
 * @interface ValueOperand
 */
export interface ValueOperand {
    /**
     * The operand value.
     * @type {string}
     * @memberof ValueOperand
     */
    value: string;
}
/**
 * Details about a project version.
 * @export
 * @interface Version
 */
export interface Version {
    /**
     * Use [expand](em>#expansion) to include additional information about version in the response. This parameter accepts a comma-separated list. Expand options include:   *  `operations` Returns the list of operations available for this version.  *  `issuesstatus` Returns the count of issues in this version for each of the status categories *to do*, *in progress*, *done*, and *unmapped*. The *unmapped* property contains a count of issues with a status other than *to do*, *in progress*, and *done*.  Optional for create and update.
     * @type {string}
     * @memberof Version
     */
    expand?: string;
    /**
     * The URL of the version.
     * @type {string}
     * @memberof Version
     */
    self?: string;
    /**
     * The ID of the version.
     * @type {string}
     * @memberof Version
     */
    id?: string;
    /**
     * The description of the version. Optional when creating or updating a version.
     * @type {string}
     * @memberof Version
     */
    description?: string;
    /**
     * The unique name of the version. Required when creating a version. Optional when updating a version. The maximum length is 255 characters.
     * @type {string}
     * @memberof Version
     */
    name?: string;
    /**
     * Indicates that the version is archived. Optional when creating or updating a version.
     * @type {boolean}
     * @memberof Version
     */
    archived?: boolean;
    /**
     * Indicates that the version is released. If the version is released a request to release again is ignored. Not applicable when creating a version. Optional when updating a version.
     * @type {boolean}
     * @memberof Version
     */
    released?: boolean;
    /**
     * The start date of the version. Expressed in ISO 8601 format (yyyy-mm-dd). Optional when creating or updating a version.
     * @type {string}
     * @memberof Version
     */
    startDate?: string;
    /**
     * The release date of the version. Expressed in ISO 8601 format (yyyy-mm-dd). Optional when creating or updating a version.
     * @type {string}
     * @memberof Version
     */
    releaseDate?: string;
    /**
     * Indicates that the version is overdue.
     * @type {boolean}
     * @memberof Version
     */
    overdue?: boolean;
    /**
     * The date on which work on this version is expected to start, expressed in the instance's *Day/Month/Year Format* date format.
     * @type {string}
     * @memberof Version
     */
    userStartDate?: string;
    /**
     * The date on which work on this version is expected to finish, expressed in the instance's *Day/Month/Year Format* date format.
     * @type {string}
     * @memberof Version
     */
    userReleaseDate?: string;
    /**
     * Deprecated. Use `projectId`.
     * @type {string}
     * @memberof Version
     */
    project?: string;
    /**
     * The ID of the project to which this version is attached. Required when creating a version. Not applicable when updating a version.
     * @type {number}
     * @memberof Version
     */
    projectId?: number;
    /**
     * The URL of the self link to the version to which all unfixed issues are moved when a version is released. Not applicable when creating a version. Optional when updating a version.
     * @type {string}
     * @memberof Version
     */
    moveUnfixedIssuesTo?: string;
    /**
     * If the expand option `operations` is used, returns the list of operations available for this version.
     * @type {Array<SimpleLink>}
     * @memberof Version
     */
    operations?: Array<SimpleLink>;
    /**
     * If the expand option `issuesstatus` is used, returns the count of issues in this version for each of the status categories *to do*, *in progress*, *done*, and *unmapped*. The *unmapped* property contains a count of issues with a status other than *to do*, *in progress*, and *done*.
     * @type {VersionIssuesStatus}
     * @memberof Version
     */
    issuesStatusForFixVersion?: VersionIssuesStatus;
}
/**
 * Various counts of issues within a version.
 * @export
 * @interface VersionIssueCounts
 */
export interface VersionIssueCounts {
    /**
     * The URL of these count details.
     * @type {string}
     * @memberof VersionIssueCounts
     */
    self?: string;
    /**
     * Count of issues where the `fixVersion` is set to the version.
     * @type {number}
     * @memberof VersionIssueCounts
     */
    issuesFixedCount?: number;
    /**
     * Count of issues where the `affectedVersion` is set to the version.
     * @type {number}
     * @memberof VersionIssueCounts
     */
    issuesAffectedCount?: number;
    /**
     * Count of issues where a version custom field is set to the version.
     * @type {number}
     * @memberof VersionIssueCounts
     */
    issueCountWithCustomFieldsShowingVersion?: number;
    /**
     * List of custom fields using the version.
     * @type {Array<VersionUsageInCustomField>}
     * @memberof VersionIssueCounts
     */
    customFieldUsage?: Array<VersionUsageInCustomField>;
}
/**
 * Counts of the number of issues in various statuses.
 * @export
 * @interface VersionIssuesStatus
 */
export interface VersionIssuesStatus {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface VersionMoveBean
 */
export interface VersionMoveBean {
    /**
     * The URL (self link) of the version after which to place the moved version. Cannot be used with `position`.
     * @type {string}
     * @memberof VersionMoveBean
     */
    after?: string;
    /**
     * An absolute position in which to place the moved version. Cannot be used with `after`.
     * @type {string}
     * @memberof VersionMoveBean
     */
    position?: VersionMoveBean.PositionEnum;
}

/**
 * @export
 * @namespace VersionMoveBean
 */
export namespace VersionMoveBean {
    /**
     * @export
     * @enum {string}
     */
    export enum PositionEnum {
        Earlier = <any>'Earlier',
        Later = <any>'Later',
        First = <any>'First',
        Last = <any>'Last',
    }
}
/**
 * Count of a version's unresolved issues.
 * @export
 * @interface VersionUnresolvedIssuesCount
 */
export interface VersionUnresolvedIssuesCount {
    /**
     * The URL of these count details.
     * @type {string}
     * @memberof VersionUnresolvedIssuesCount
     */
    self?: string;
    /**
     * Count of unresolved issues.
     * @type {number}
     * @memberof VersionUnresolvedIssuesCount
     */
    issuesUnresolvedCount?: number;
    /**
     * Count of issues.
     * @type {number}
     * @memberof VersionUnresolvedIssuesCount
     */
    issuesCount?: number;
}
/**
 * List of custom fields using the version.
 * @export
 * @interface VersionUsageInCustomField
 */
export interface VersionUsageInCustomField {
    /**
     * The name of the custom field.
     * @type {string}
     * @memberof VersionUsageInCustomField
     */
    fieldName?: string;
    /**
     * The ID of the custom field.
     * @type {number}
     * @memberof VersionUsageInCustomField
     */
    customFieldId?: number;
    /**
     * Count of the issues where the custom field contains the version.
     * @type {number}
     * @memberof VersionUsageInCustomField
     */
    issueCountWithVersionInCustomField?: number;
}
/**
 * The group or role to which this item is visible.
 * @export
 * @interface Visibility
 */
export interface Visibility {
    [key: string]: any;
}
/**
 * The details of votes on an issue.
 * @export
 * @interface Votes
 */
export interface Votes {
    /**
     * The URL of these issue vote details.
     * @type {string}
     * @memberof Votes
     */
    self?: string;
    /**
     * The number of votes on the issue.
     * @type {number}
     * @memberof Votes
     */
    votes?: number;
    /**
     * Whether the user making this request has voted on the issue.
     * @type {boolean}
     * @memberof Votes
     */
    hasVoted?: boolean;
    /**
     * List of the users who have voted on this issue. An empty list is returned when the calling user doesn't have the *View voters and watchers* project permission.
     * @type {Array<User>}
     * @memberof Votes
     */
    voters?: Array<User>;
}
/**
 * The details of watchers on an issue.
 * @export
 * @interface Watchers
 */
export interface Watchers {
    /**
     * The URL of these issue watcher details.
     * @type {string}
     * @memberof Watchers
     */
    self?: string;
    /**
     * Whether the calling user is watching this issue.
     * @type {boolean}
     * @memberof Watchers
     */
    isWatching?: boolean;
    /**
     * The number of users watching this issue.
     * @type {number}
     * @memberof Watchers
     */
    watchCount?: number;
    /**
     * Details of the users watching this issue.
     * @type {Array<UserDetails>}
     * @memberof Watchers
     */
    watchers?: Array<UserDetails>;
}
/**
 * A webhook.
 * @export
 * @interface Webhook
 */
export interface Webhook {
    /**
     * The ID of the webhook.
     * @type {number}
     * @memberof Webhook
     */
    id: number;
    /**
     * The JQL filter that specifies which issues the webhook is sent for.
     * @type {string}
     * @memberof Webhook
     */
    jqlFilter: string;
    /**
     * The Jira events that trigger the webhook.
     * @type {Array<string>}
     * @memberof Webhook
     */
    events?: Array<Webhook.EventsEnum>;
    /**
     *
     * @type {number}
     * @memberof Webhook
     */
    expirationDate: number;
}

/**
 * @export
 * @namespace Webhook
 */
export namespace Webhook {
    /**
     * @export
     * @enum {string}
     */
    export enum EventsEnum {
        JiraissueCreated = <any>'jira:issue_created',
        JiraissueUpdated = <any>'jira:issue_updated',
        JiraissueDeleted = <any>'jira:issue_deleted',
        CommentCreated = <any>'comment_created',
        CommentUpdated = <any>'comment_updated',
        CommentDeleted = <any>'comment_deleted',
        IssuePropertySet = <any>'issue_property_set',
        IssuePropertyDeleted = <any>'issue_property_deleted',
    }
}
/**
 * A list of webhooks.
 * @export
 * @interface WebhookDetails
 */
export interface WebhookDetails {
    /**
     * The JQL filter that specifies which issues the webhook is sent for. Only a subset of JQL can be used. The supported elements are:   *  Fields: `issueKey`, `project`, `issuetype`, `status`, `assignee`, `reporter`, `issue.property`, and `cf[id]` (for custom fields—only the epic label custom field is supported).  *  Operators: `=`, `!=`, `IN`, and `NOT IN`.
     * @type {string}
     * @memberof WebhookDetails
     */
    jqlFilter: string;
    /**
     * The Jira events that trigger the webhook.
     * @type {Array<string>}
     * @memberof WebhookDetails
     */
    events?: Array<WebhookDetails.EventsEnum>;
}

/**
 * @export
 * @namespace WebhookDetails
 */
export namespace WebhookDetails {
    /**
     * @export
     * @enum {string}
     */
    export enum EventsEnum {
        JiraissueCreated = <any>'jira:issue_created',
        JiraissueUpdated = <any>'jira:issue_updated',
        JiraissueDeleted = <any>'jira:issue_deleted',
        CommentCreated = <any>'comment_created',
        CommentUpdated = <any>'comment_updated',
        CommentDeleted = <any>'comment_deleted',
        IssuePropertySet = <any>'issue_property_set',
        IssuePropertyDeleted = <any>'issue_property_deleted',
    }
}
/**
 * Details of webhooks to register.
 * @export
 * @interface WebhookRegistrationDetails
 */
export interface WebhookRegistrationDetails {
    /**
     * A list of webhooks.
     * @type {Array<WebhookDetails>}
     * @memberof WebhookRegistrationDetails
     */
    webhooks: Array<WebhookDetails>;
    /**
     * The URL that specifies where to send the webhooks.
     * @type {string}
     * @memberof WebhookRegistrationDetails
     */
    url: string;
}
/**
 * The date the newly refreshed webhooks expire.
 * @export
 * @interface WebhooksExpirationDate
 */
export interface WebhooksExpirationDate {
    /**
     *
     * @type {number}
     * @memberof WebhooksExpirationDate
     */
    expirationDate: number;
}
/**
 * Details about a workflow.
 * @export
 * @interface Workflow
 */
export interface Workflow {
    /**
     *
     * @type {PublishedWorkflowId}
     * @memberof Workflow
     */
    id?: PublishedWorkflowId;
    /**
     * The description of the workflow.
     * @type {string}
     * @memberof Workflow
     */
    description: string;
    /**
     * The transitions of the workflow.
     * @type {Array<Transition>}
     * @memberof Workflow
     */
    transitions?: Array<Transition>;
    /**
     * The statuses of the workflow.
     * @type {Array<WorkflowStatus>}
     * @memberof Workflow
     */
    statuses?: Array<WorkflowStatus>;
}
/**
 * Properties that identify a workflow.
 * @export
 * @interface WorkflowId
 */
export interface WorkflowId {
    /**
     * The name of the workflow.
     * @type {string}
     * @memberof WorkflowId
     */
    name: string;
    /**
     * Whether the workflow is in the draft state.
     * @type {boolean}
     * @memberof WorkflowId
     */
    draft: boolean;
}
/**
 * A collection of transition rules.
 * @export
 * @interface WorkflowRules
 */
export interface WorkflowRules {
    /**
     * The workflow conditions.
     * @type {Array<WorkflowTransitionRule>}
     * @memberof WorkflowRules
     */
    conditions: Array<WorkflowTransitionRule>;
    /**
     * The workflow validators.
     * @type {Array<WorkflowTransitionRule>}
     * @memberof WorkflowRules
     */
    validators: Array<WorkflowTransitionRule>;
    /**
     * The workflow post functions.
     * @type {Array<WorkflowTransitionRule>}
     * @memberof WorkflowRules
     */
    postFunctions: Array<WorkflowTransitionRule>;
}
/**
 * Details about a workflow scheme.
 * @export
 * @interface WorkflowScheme
 */
export interface WorkflowScheme {
    /**
     * The ID of the workflow scheme.
     * @type {number}
     * @memberof WorkflowScheme
     */
    id?: number;
    /**
     * The name of the workflow scheme. The name must be unique. The maximum length is 255 characters. Required when creating a workflow scheme.
     * @type {string}
     * @memberof WorkflowScheme
     */
    name?: string;
    /**
     * The description of the workflow scheme.
     * @type {string}
     * @memberof WorkflowScheme
     */
    description?: string;
    /**
     * The name of the default workflow for the workflow scheme. The default workflow has *All Unassigned Issue Types* assigned to it in Jira. If `defaultWorkflow` is not specified when creating a workflow scheme, it is set to *Jira Workflow (jira)*.
     * @type {string}
     * @memberof WorkflowScheme
     */
    defaultWorkflow?: string;
    /**
     * The issue type to workflow mappings, where each mapping is an issue type ID and workflow name pair. Note that an issue type can only be mapped to one workflow in a workflow scheme.
     * @type {{ [key: string]: string; }}
     * @memberof WorkflowScheme
     */
    issueTypeMappings?: { [key: string]: string };
    /**
     * For draft workflow schemes, this property is the name of the default workflow for the original workflow scheme. The default workflow has *All Unassigned Issue Types* assigned to it in Jira.
     * @type {string}
     * @memberof WorkflowScheme
     */
    originalDefaultWorkflow?: string;
    /**
     * For draft workflow schemes, this property is the issue type to workflow mappings for the original workflow scheme, where each mapping is an issue type ID and workflow name pair. Note that an issue type can only be mapped to one workflow in a workflow scheme.
     * @type {{ [key: string]: string; }}
     * @memberof WorkflowScheme
     */
    originalIssueTypeMappings?: { [key: string]: string };
    /**
     * Whether the workflow scheme is a draft or not.
     * @type {boolean}
     * @memberof WorkflowScheme
     */
    draft?: boolean;
    /**
     * The user that last modified the draft workflow scheme. A modification is a change to the issue type-project mappings only. This property does not apply to non-draft workflows.
     * @type {User}
     * @memberof WorkflowScheme
     */
    lastModifiedUser?: User;
    /**
     * The date-time that the draft workflow scheme was last modified. A modification is a change to the issue type-project mappings only. This property does not apply to non-draft workflows.
     * @type {string}
     * @memberof WorkflowScheme
     */
    lastModified?: string;
    /**
     *
     * @type {string}
     * @memberof WorkflowScheme
     */
    self?: string;
    /**
     * Whether to create or update a draft workflow scheme when updating an active workflow scheme. An active workflow scheme is a workflow scheme that is used by at least one project. The following examples show how this property works:   *  Update an active workflow scheme with `updateDraftIfNeeded` set to `true`: If a draft workflow scheme exists, it is updated. Otherwise, a draft workflow scheme is created.  *  Update an active workflow scheme with `updateDraftIfNeeded` set to `false`: An error is returned, as active workflow schemes cannot be updated.  *  Update an inactive workflow scheme with `updateDraftIfNeeded` set to `true`: The workflow scheme is updated, as inactive workflow schemes do not require drafts to update.  Defaults to `false`.
     * @type {boolean}
     * @memberof WorkflowScheme
     */
    updateDraftIfNeeded?: boolean;
    /**
     * The issue types available in Jira.
     * @type {{ [key: string]: IssueTypeDetails; }}
     * @memberof WorkflowScheme
     */
    issueTypes?: { [key: string]: IssueTypeDetails };
}
/**
 * A workflow scheme along with a list of projects that use it.
 * @export
 * @interface WorkflowSchemeAssociations
 */
export interface WorkflowSchemeAssociations {
    /**
     * The list of projects that use the workflow scheme.
     * @type {Array<string>}
     * @memberof WorkflowSchemeAssociations
     */
    projectIds: Array<string>;
    /**
     * The workflow scheme.
     * @type {WorkflowScheme}
     * @memberof WorkflowSchemeAssociations
     */
    workflowScheme?: WorkflowScheme;
}
/**
 * An associated workflow scheme and project.
 * @export
 * @interface WorkflowSchemeProjectAssociation
 */
export interface WorkflowSchemeProjectAssociation {
    /**
     * The ID of the workflow scheme.
     * @type {string}
     * @memberof WorkflowSchemeProjectAssociation
     */
    workflowSchemeId: string;
    /**
     * The ID of the project.
     * @type {string}
     * @memberof WorkflowSchemeProjectAssociation
     */
    projectId: string;
}
/**
 * Details of a workflow status.
 * @export
 * @interface WorkflowStatus
 */
export interface WorkflowStatus {
    /**
     * The ID of the issue status.
     * @type {string}
     * @memberof WorkflowStatus
     */
    id: string;
    /**
     * The name of the status in the workflow.
     * @type {string}
     * @memberof WorkflowStatus
     */
    name: string;
    /**
     *
     * @type {WorkflowStatusProperties}
     * @memberof WorkflowStatus
     */
    properties?: WorkflowStatusProperties;
}
/**
 * Properties of a workflow status.
 * @export
 * @interface WorkflowStatusProperties
 */
export interface WorkflowStatusProperties {
    /**
     * Whether issues are editable in this status.
     * @type {boolean}
     * @memberof WorkflowStatusProperties
     */
    issueEditable: boolean;
}
/**
 * A workflow transition.
 * @export
 * @interface WorkflowTransition
 */
export interface WorkflowTransition {
    /**
     * The transition ID.
     * @type {number}
     * @memberof WorkflowTransition
     */
    id: number;
    /**
     * The transition name.
     * @type {string}
     * @memberof WorkflowTransition
     */
    name: string;
}
/**
 * Details about the server Jira is running on.
 * @export
 * @interface WorkflowTransitionProperty
 */
export interface WorkflowTransitionProperty {
    [key: string]: any;
}
/**
 * A workflow transition rule.
 * @export
 * @interface WorkflowTransitionRule
 */
export interface WorkflowTransitionRule {
    /**
     * The type of the transition rule.
     * @type {string}
     * @memberof WorkflowTransitionRule
     */
    type: string;
    /**
     * The configuration of the transition rule. This is currently returned only for some of the rule types. Availability of this property is subject to change.
     * @type {ModelObject}
     * @memberof WorkflowTransitionRule
     */
    configuration?: ModelObject;
}
/**
 * A workflow with transition rules.
 * @export
 * @interface WorkflowTransitionRules
 */
export interface WorkflowTransitionRules {
    /**
     *
     * @type {WorkflowId}
     * @memberof WorkflowTransitionRules
     */
    workflowId?: WorkflowId;
    /**
     * The list of post functions within the workflow.
     * @type {Array<ConnectWorkflowTransitionRule>}
     * @memberof WorkflowTransitionRules
     */
    postFunctions?: Array<ConnectWorkflowTransitionRule>;
    /**
     * The list of conditions within the workflow.
     * @type {Array<ConnectWorkflowTransitionRule>}
     * @memberof WorkflowTransitionRules
     */
    conditions?: Array<ConnectWorkflowTransitionRule>;
    /**
     * The list of validators within the workflow.
     * @type {Array<ConnectWorkflowTransitionRule>}
     * @memberof WorkflowTransitionRules
     */
    validators?: Array<ConnectWorkflowTransitionRule>;
}
/**
 * Details about a workflow configuration update request.
 * @export
 * @interface WorkflowTransitionRulesUpdate
 */
export interface WorkflowTransitionRulesUpdate {
    /**
     * The list of workflows with transition rules to update.
     * @type {Array<WorkflowTransitionRules>}
     * @memberof WorkflowTransitionRulesUpdate
     */
    workflows?: Array<WorkflowTransitionRules>;
}
/**
 * Details of any errors encountered while updating workflow transition rules for a workflow.
 * @export
 * @interface WorkflowTransitionRulesUpdateErrorDetails
 */
export interface WorkflowTransitionRulesUpdateErrorDetails {
    /**
     *
     * @type {WorkflowId}
     * @memberof WorkflowTransitionRulesUpdateErrorDetails
     */
    workflowId?: WorkflowId;
    /**
     * A list of transition rule update errors, indexed by the transition rule ID. Any transition rule that appears here wasn't updated.
     * @type {{ [key: string]: Array<string>; }}
     * @memberof WorkflowTransitionRulesUpdateErrorDetails
     */
    ruleUpdateErrors: { [key: string]: Array<string> };
    /**
     * The list of errors that specify why the workflow update failed. The workflow was not updated if the list contains any entries.
     * @type {Array<string>}
     * @memberof WorkflowTransitionRulesUpdateErrorDetails
     */
    updateErrors?: Array<string>;
}
/**
 * Details of any errors encountered while updating workflow transition rules.
 * @export
 * @interface WorkflowTransitionRulesUpdateErrors
 */
export interface WorkflowTransitionRulesUpdateErrors {
    /**
     * A list of workflows.
     * @type {Array<WorkflowTransitionRulesUpdateErrorDetails>}
     * @memberof WorkflowTransitionRulesUpdateErrors
     */
    updateResults: Array<WorkflowTransitionRulesUpdateErrorDetails>;
}
/**
 * Details of a worklog.
 * @export
 * @interface Worklog
 */
export interface Worklog {
    [key: string]: any;
}
/**
 *
 * @export
 * @interface WorklogIdsRequestBean
 */
export interface WorklogIdsRequestBean {
    /**
     * A list of worklog IDs.
     * @type {Array<number>}
     * @memberof WorklogIdsRequestBean
     */
    ids: Array<number>;
}
