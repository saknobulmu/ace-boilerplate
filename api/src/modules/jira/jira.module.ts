import { Module, DynamicModule, MiddlewareConsumer } from '@nestjs/common';
import { AddOn } from 'atlassian-connect-express';
import { AuthenticateMiddleware } from '../../middlewares/authenticate.middleware';
import { ValidateUserMiddleware } from '../../middlewares/validate-user.middleware';
import { RestApiConnector } from './connector/restapi.connector';
import { ACE_ADDON } from './jira.constants';
// eslint-disable-next-line import/no-cycle
import { CoreModule } from '../core/core.module';
// eslint-disable-next-line import/no-cycle
import { GraphQLModule } from '../graphql/graphql.module';
import * as jiraServices from './services';
import * as jiraWebhooks from './webhooks';
import { webhookRequestLimiter } from '../../middlewares/request-limit.middlewares';
import { InstanceInfoMiddleware } from '../../middlewares/instance-info.middleware';
import { AccountInfoMiddleware } from '../../middlewares/account-info.middleware';

const allJiraServices = Object.values(jiraServices);
const allJiraWebhooks = Object.values(jiraWebhooks);

@Module({})
export class JiraModule {
    private static jiraModuleInstance: DynamicModule;

    static register(aceAddon: AddOn): DynamicModule {
        const aceAddonProvider = { provide: ACE_ADDON, useValue: aceAddon };
        this.jiraModuleInstance = {
            module: JiraModule,
            imports: [CoreModule, GraphQLModule],
            controllers: [...allJiraWebhooks],
            providers: [aceAddonProvider, RestApiConnector, ...allJiraServices],
            exports: [ACE_ADDON, ...allJiraServices],
        };
        return this.jiraModuleInstance;
    }

    static instance(): DynamicModule {
        return this.jiraModuleInstance;
    }

    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(
                webhookRequestLimiter,
                AuthenticateMiddleware,
                ValidateUserMiddleware,
                InstanceInfoMiddleware,
                AccountInfoMiddleware,
            )
            .exclude('enabled', 'disabled')
            .forRoutes(...allJiraWebhooks);
    }
}
