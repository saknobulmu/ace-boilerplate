import { Request } from 'express';

export interface AddonSettingVal {
    key?: string;
    clientKey?: string;
    sharedSecret?: string;
    baseUrl?: string;
}

type ACEOpts = { clientKey: string; userAccountId: string };
export type ReqOrOpts = ACERequest | ACEOpts;

export interface ACERequest extends Request {
    context?: ACEContext;
    instanceName?: string;
    clientKey?: string;
    userAccountId?: string;
    body: any;
    query: any;
}

export interface ACEContext {
    title: string;
    addonKey: string;
    localBaseUrl: string;
    hostBaseUrl: string;
    hostStylesheetUrl: string;
    hostScriptUrl: string;
    token: string;
    license: string;
    context: string;
    clientKey: string;
    userAccountId: string;
    http: {
        addon: {
            descriptor: {
                apiVersion: number;
            };
        };
    };
}

export interface ACEWebhooks {
    event: string;
    url: string;
}

export interface ACEDescriptor {
    key: string;
    name: string;
    description: string;
    baseUrl: string;
    modules: {
        webhooks: Array<ACEWebhooks>;
    };
}

export type ResultIdentifier = 'issues' | 'users' | 'values';

export enum JiraQueryType {
    ISSUE,
    USER,
    FIELD,
    LABEL,
    PROJECT,
}

export interface HttpClient {
    getUnpaginated: <T>(path: string, jiraQueryType: JiraQueryType, input?: BaseInput) => Promise<Array<T>>;
    get: <T>(path: string, input?: BaseInput) => Promise<T>;
    post: <T>(path: string, input?: BaseInput) => Promise<T>;
    put: <T>(path: string, input?: BaseInput) => Promise<T>;
    del: <T>(path: string, input?: BaseInput) => Promise<T>;
}

export interface BaseInput {
    pathParameters?: Record<string, string | undefined>;
    queryParameters?: Record<string, string | string[] | number | number[] | undefined>;
    body?: Record<string, unknown>;
}
