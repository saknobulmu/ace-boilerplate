import { Controller, Post, Req, Res } from '@nestjs/common';
import { Response } from 'express';
import { ACERequest } from '../jira.types';

@Controller()
export class EnabledWebhook {
    @Post('enabled')
    async handleWebhook(@Req() req: ACERequest, @Res() res: Response): Promise<void> {
        console.log('Enabled webhook called!');
        res.status(204).end();
    }
}
