import { Controller, Post, Req, Res } from '@nestjs/common';
import { Response } from 'express';
import { ACERequest } from '../jira.types';

@Controller()
export class IssueUpdatedWebhook {
    @Post('issue-updated')
    handleWebhook(@Req() req: ACERequest, @Res() res: Response): void {
        console.log('Issue updated webhook called!');
        res.status(204).end();
    }
}
