import {
    Column,
    PrimaryKey,
    Unique,
    AutoIncrement,
    Model,
    Table,
    DataType,
    CreatedAt,
    UpdatedAt,
} from 'sequelize-typescript';
import { AddonSettingVal } from '../../jira/jira.types';

@Table({
    underscored: false,
    tableName: 'AddonSettings',
})
export class AddonSetting extends Model<AddonSetting> {
    @PrimaryKey
    @Unique
    @AutoIncrement
    @Column(DataType.BIGINT)
    id: string;

    @Column(DataType.STRING)
    clientKey: string;

    @Column(DataType.STRING)
    key: string;

    @Column(DataType.JSON)
    val: AddonSettingVal;

    @CreatedAt
    @Column
    createdAt: Date;

    @UpdatedAt
    @Column
    updatedAt: Date;
}
