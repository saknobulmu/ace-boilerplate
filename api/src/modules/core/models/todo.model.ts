import {
    Column,
    PrimaryKey,
    Unique,
    AllowNull,
    Default,
    Model,
    Table,
    DataType,
    CreatedAt,
    UpdatedAt,
} from 'sequelize-typescript';

@Table({
    tableName: 'todos',
})
export class Todo extends Model<Todo> {
    @PrimaryKey
    @Unique
    @Column(DataType.UUID)
    id: string;

    @AllowNull(false)
    @Column(DataType.TEXT)
    title: string;

    @AllowNull(false)
    @Default(false)
    @Column(DataType.BOOLEAN)
    done: boolean;

    @CreatedAt
    @Column
    createdAt: Date;

    @UpdatedAt
    @Column
    updatedAt: Date;
}
