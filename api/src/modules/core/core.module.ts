import { Module, MiddlewareConsumer, forwardRef } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
// eslint-disable-next-line import/no-cycle
import { JiraModule } from '../jira/jira.module';
import { CheckValidTokenMiddleware } from '../../middlewares/check-valid-token.middleware';
import { AppController } from './controllers/app.controller';
import { DescriptorController } from './controllers/descriptor.controller';
import { HealthController } from './controllers/health.controller';
import * as coreModels from './models';
import * as coreServices from './services';
import { generalRequestLimiter } from '../../middlewares/request-limit.middlewares';

const allCoreModels = Object.values(coreModels);
const allCoreServices = Object.values(coreServices);

@Module({
    imports: [SequelizeModule.forFeature([...allCoreModels]), forwardRef(() => JiraModule.instance())],
    controllers: [HealthController, DescriptorController, AppController],
    providers: [...allCoreServices],
    exports: [...allCoreServices],
})
export class CoreModule {
    configure = (consumer: MiddlewareConsumer) => {
        consumer.apply(generalRequestLimiter).forRoutes(HealthController, DescriptorController);

        consumer.apply(generalRequestLimiter, CheckValidTokenMiddleware).forRoutes(AppController);
    };
}
