import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller()
export class HealthController {
    @Get('ping')
    ping(@Res() res: Response): void {
        res.status(200).send('PONG');
    }
}
