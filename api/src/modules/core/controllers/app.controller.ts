import { Controller, Get, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';

@Controller()
export class AppController {
    @Get('todos')
    openTodosScreen(@Res() res: Response, @Req() req: Request): void {
        res.redirect(`/app?${this.getQueryString(req)}`);
    }

    @Get('expired')
    openExpiredScreen(@Res() res: Response, @Req() req: Request): void {
        res.redirect(`/app/expired?${this.getQueryString(req)}`);
    }

    getQueryString = ({ query, context: { token } }: any): string => {
        const keys = Object.keys(query);
        return keys.reduce((acc: string, cur: string) => {
            // replace jira session jwt with addon generated token that respects expiration time
            // set in atlassian-connect.json
            const value: string = cur === 'jwt' ? token : query[cur];
            return `${acc}&${cur}=${value}`;
        }, '');
    };
}
