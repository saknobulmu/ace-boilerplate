import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller()
export class DescriptorController {
    @Get()
    appDescriptor(@Res() res: Response): void {
        res.redirect('/atlassian-connect.json');
    }
}
