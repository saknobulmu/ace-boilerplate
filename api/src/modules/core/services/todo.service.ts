import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { FindOptions } from 'sequelize';
import { omitBy, omit, isUndefined } from 'lodash';
import { Todo } from '../models/todo.model';

@Injectable()
export class TodoService {
    constructor(
        @InjectModel(Todo)
        private todoModel: typeof Todo,
    ) {}

    create(todo: Todo): Promise<Todo> {
        return this.todoModel.create(todo);
    }

    findById(id: string): Promise<Todo> {
        return this.todoModel.findByPk(id);
    }

    findOne(options?: FindOptions): Promise<Todo> {
        return this.todoModel.findOne(options);
    }

    findAll(options?: FindOptions): Promise<Todo[]> {
        return this.todoModel.findAll(options);
    }

    update = async (input: Todo): Promise<Todo> => {
        await this.todoModel.update(omitBy(omit(input, 'id'), isUndefined), {
            where: { id: input.id },
        });
        return this.findById(input.id);
    };
}
