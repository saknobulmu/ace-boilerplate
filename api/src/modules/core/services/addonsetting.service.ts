import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { FindOptions } from 'sequelize';
import { AddonSetting } from '../models/addonsetting.model';

@Injectable()
export class AddonSettingService {
    constructor(
        @InjectModel(AddonSetting)
        private addonSettingModel: typeof AddonSetting,
    ) {}

    findOne(options?: FindOptions): Promise<AddonSetting> {
        return this.addonSettingModel.findOne(options);
    }

    async getSharedSecret(clientKey: string): Promise<string> {
        const clientInfo = await this.findOne({
            where: {
                clientKey,
                key: 'clientInfo',
            },
        });

        let sharedSecret = '';
        if (clientInfo && clientInfo.val && clientInfo.val.sharedSecret) {
            sharedSecret = clientInfo.val.sharedSecret;
        }

        return sharedSecret;
    }
}
