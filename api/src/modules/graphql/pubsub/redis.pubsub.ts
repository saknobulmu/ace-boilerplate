import { RedisPubSub } from 'graphql-redis-subscriptions';
import { PUB_SUB } from '../graphql.constants';

export const CustomRedisPubSub = {
    provide: PUB_SUB,
    useValue: new RedisPubSub({
        connection: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
        },
    }),
};
