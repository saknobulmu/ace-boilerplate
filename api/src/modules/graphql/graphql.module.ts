import { Module, MiddlewareConsumer, forwardRef } from '@nestjs/common';
// eslint-disable-next-line import/no-cycle
import { JiraModule } from '../jira/jira.module';
// eslint-disable-next-line import/no-cycle
import { CoreModule } from '../core/core.module';
import { CheckValidTokenMiddleware } from '../../middlewares/check-valid-token.middleware';
import { ValidateUserMiddleware } from '../../middlewares/validate-user.middleware';
import { InstanceInfoMiddleware } from '../../middlewares/instance-info.middleware';
import { AccountInfoMiddleware } from '../../middlewares/account-info.middleware';
import { CustomRedisPubSub } from './pubsub/redis.pubsub';
import * as graphQLResolvers from './resolvers';
import { graphqlRequestLimiter } from '../../middlewares/request-limit.middlewares';

const allGraphQLResolvers = Object.values(graphQLResolvers);

@Module({
    imports: [forwardRef(() => CoreModule), forwardRef(() => JiraModule.instance())],
    providers: [...allGraphQLResolvers, CustomRedisPubSub],
    exports: [CustomRedisPubSub],
})
export class GraphQLModule {
    configure = (consumer: MiddlewareConsumer) => {
        consumer
            .apply(
                graphqlRequestLimiter,
                CheckValidTokenMiddleware,
                ValidateUserMiddleware,
                InstanceInfoMiddleware,
                AccountInfoMiddleware,
            )
            .forRoutes('/graphql');
    };
}
