import { Mutation, Resolver, Context, Args } from '@nestjs/graphql';
import { UpdateJiraIssueSummaryInput, SuccessResponse } from '../../graphql.schema';
import { JiraIssueService } from '../../../jira/services/jira.issue.service';

@Resolver('JiraIssue')
export class JiraIssueMutation {
    constructor(private readonly jiraIssueService: JiraIssueService) {}

    @Mutation('updateJiraIssueSummary')
    async updateJiraIssueSummary(
        @Context() context,
        @Args('input') input: UpdateJiraIssueSummaryInput,
    ): Promise<SuccessResponse> {
        await this.jiraIssueService.updateIssueSummary(context.req, input.issueIdOrKey, input.summary);
        return { status: true };
    }
}
