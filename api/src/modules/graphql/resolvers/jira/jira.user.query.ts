import { Query, Resolver, Context } from '@nestjs/graphql';
import { JiraUser } from '../../graphql.schema';
import { JiraUserService } from '../../../jira/services/jira.user.service';

@Resolver('JiraUser')
export class JiraUserQuery {
    constructor(private readonly jiraUserService: JiraUserService) {}

    @Query('myself')
    async getCurrentUser(@Context() context): Promise<JiraUser> {
        return this.jiraUserService.getCurrentUser(context.req, {
            queryParameters: {
                expand: 'groups',
            },
        });
    }
}
