import { Inject } from '@nestjs/common';
import { Subscription, Resolver } from '@nestjs/graphql';
import { Todo } from '../../../core/models/todo.model';
import { PUB_SUB, EVENT_TODO_CREATED, EVENT_TODO_UPDATED } from '../../graphql.constants';

@Resolver('Todo')
export class TodoSubscription {
    constructor(@Inject(PUB_SUB) private pubSub) {}

    @Subscription('todoCreated', {
        resolve: (payload: Todo) => payload,
    })
    async todoCreated(): Promise<Todo> {
        return this.pubSub.asyncIterator(EVENT_TODO_CREATED);
    }

    @Subscription('todoUpdated', {
        resolve: (payload: Todo) => payload,
    })
    async todoUpdated(): Promise<Todo> {
        return this.pubSub.asyncIterator(EVENT_TODO_UPDATED);
    }
}
