import { Inject } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Todo } from '../../../core/models/todo.model';
import { TodoService } from '../../../core/services/todo.service';
import { PUB_SUB, EVENT_TODO_CREATED, EVENT_TODO_UPDATED } from '../../graphql.constants';

@Resolver('Todo')
export class TodoMutation {
    constructor(private readonly todoService: TodoService, @Inject(PUB_SUB) private readonly pubSub) {}

    @Mutation('createTodo')
    async createTodo(@Args('input') input: Todo): Promise<Todo> {
        const todo = await this.todoService.create(input);
        this.pubSub.publish(EVENT_TODO_CREATED, todo);
        return todo;
    }

    @Mutation('updateTodo')
    async updateTodo(@Args('input') input: Todo): Promise<Todo> {
        const todo = await this.todoService.update(input);
        this.pubSub.publish(EVENT_TODO_UPDATED, todo);
        return todo;
    }
}
