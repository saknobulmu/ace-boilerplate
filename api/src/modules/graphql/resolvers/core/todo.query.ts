import { Query, Resolver } from '@nestjs/graphql';
import { Todo } from '../../graphql.schema';
import { TodoService } from '../../../core/services/todo.service';

@Resolver('Todo')
export class TodoQuery {
    constructor(private readonly todoService: TodoService) {}

    @Query('todos')
    async getTodos(): Promise<Array<Todo>> {
        return this.todoService.findAll();
    }
}
