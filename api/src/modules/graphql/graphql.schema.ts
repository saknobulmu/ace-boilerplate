
/*
 * ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface TodoInput {
    title: string;
    done?: boolean;
}

export interface UpdateTodoInput {
    id: string;
    title?: string;
    done?: boolean;
}

export interface UpdateJiraIssueSummaryInput {
    issueIdOrKey: string;
    summary: string;
}

export interface Todo {
    id: string;
    createdAt: DateTime;
    updatedAt?: DateTime;
    title: string;
    done: boolean;
}

export interface IQuery {
    todos(): Todo[] | Promise<Todo[]>;
    myself(): JiraUser | Promise<JiraUser>;
}

export interface IMutation {
    createTodo(input: TodoInput): Todo | Promise<Todo>;
    updateTodo(input: UpdateTodoInput): Todo | Promise<Todo>;
    updateJiraIssueSummary(input: UpdateJiraIssueSummaryInput): SuccessResponse | Promise<SuccessResponse>;
}

export interface ISubscription {
    todoCreated(): Todo | Promise<Todo>;
    todoUpdated(): Todo | Promise<Todo>;
}

export interface SuccessResponse {
    status?: boolean;
}

export interface JiraUser {
    self?: string;
    accountId?: string;
    displayName?: string;
    active?: boolean;
}

export type JSON = any;
export type JSONObject = any;
export type DateTime = any;
export type Long = any;
