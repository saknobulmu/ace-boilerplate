export const PUB_SUB = Symbol('PUB_SUB');

export const EVENT_TODO_CREATED = 'todo-created';
export const EVENT_TODO_UPDATED = 'todo-updated';
