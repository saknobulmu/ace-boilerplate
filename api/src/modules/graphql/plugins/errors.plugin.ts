import { Plugin } from '@nestjs/graphql';
import { ApolloServerPlugin, GraphQLRequestListener, GraphQLRequestContext } from 'apollo-server-plugin-base';
import * as Sentry from '@sentry/node';

@Plugin()
export class HandleErrorsPlugin implements ApolloServerPlugin {
    requestDidStart(_requestContext: GraphQLRequestContext): GraphQLRequestListener {
        return {
            // https://blog.sentry.io/2020/07/22/handling-graphql-errors-using-sentry
            didEncounterErrors(errorContext: GraphQLRequestContext) {
                for (const err of errorContext.errors) {
                    if (err?.originalError?.name !== 'UserError') {
                        Sentry.withScope((scope) => {
                            scope.setTag('instance', errorContext.context.req.instanceName);
                            Sentry.captureException(err);
                        });
                    }
                }
            },
        };
    }
}
