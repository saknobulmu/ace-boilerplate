export class UnrecognizedPermissionError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = 'UnrecognizedPermissionError';
    }
}
