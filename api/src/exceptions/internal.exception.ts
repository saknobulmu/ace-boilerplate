import { ACERequest } from '../modules/jira/jira.types';

const buildMessage = (message: string, callerFunctionName: string, request?: ACERequest) => {
    const instanceInfo = request ? `Instance: ${request.instanceName}` : '-';
    return `[${callerFunctionName}] [${instanceInfo}] - ${message}`;
};

export class InternalError extends Error {
    constructor(message: string, callerFunctionName: string, request?: ACERequest) {
        super(buildMessage(message, callerFunctionName, request));
        this.name = 'InternalError';
    }
}
