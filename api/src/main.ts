import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import express from 'express';
import atlassianConnectExpress, { AddOn } from 'atlassian-connect-express';
import * as Sentry from '@sentry/node';
import { MainModule } from './main.module';
import { applyExpressMiddlewares } from './middlewares/express.middlewares';
import { removeDefaultWebhooksRoutes } from './utils/manage.routes';
import { enableSequelizeCLS } from './config/sequelize-cls.config';
import { UnhandledExceptionsFilter } from './common/unhandled-exceptions.filter';
import { SwanlyCustomLogger } from './common/custom-logger';

const { AC_OPTS, NODE_ENV, API_PORT, ENVIRONMENT_TYPE, SENTRY_DSN_API } = process.env;
const devEnv = NODE_ENV !== 'production';

interface ExpressAce {
    appExpress: express.Application;
    aceAddon: AddOn;
}

enableSequelizeCLS();

function initSentry() {
    Sentry.init({
        dsn: SENTRY_DSN_API,
        environment: ENVIRONMENT_TYPE,
    });
}

function expressAceBootstrap(): ExpressAce {
    const appExpress: express.Application = express();
    appExpress.set('env', NODE_ENV);
    appExpress.use(Sentry.Handlers.requestHandler());

    const aceAddon = atlassianConnectExpress(appExpress);
    applyExpressMiddlewares(appExpress, aceAddon);

    appExpress.use(Sentry.Handlers.errorHandler());
    return { appExpress, aceAddon };
}

async function nestJsBootstrap(expressAce: ExpressAce) {
    const { aceAddon, appExpress } = expressAce;

    const appNest: NestExpressApplication = await NestFactory.create<NestExpressApplication>(
        MainModule.register(aceAddon),
        new ExpressAdapter(appExpress),
        {
            logger: new SwanlyCustomLogger(),
        },
    );
    removeDefaultWebhooksRoutes(appExpress, aceAddon);

    appNest.use(Sentry.Handlers.requestHandler());
    const { httpAdapter } = appNest.get(HttpAdapterHost);
    appNest.useGlobalFilters(new UnhandledExceptionsFilter(httpAdapter));

    await appNest.listen(API_PORT, () => {
        console.log('✅ Add-on server running ✨');
        if (devEnv && (AC_OPTS || '').includes('force-reg')) {
            aceAddon.register();
        }
    });
}

function bootstrap() {
    initSentry();
    const expressAce: ExpressAce = expressAceBootstrap();
    nestJsBootstrap(expressAce);
}

bootstrap();
