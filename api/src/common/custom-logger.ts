import { LoggerService, LogLevel } from '@nestjs/common';

const { LOG_LEVEL } = process.env;
const DEFAULT_LOG_LEVEL: LogLevel[] = ['log', 'error', 'warn'];
const logLevels = LOG_LEVEL ? (LOG_LEVEL.split(',') as LogLevel[]) : DEFAULT_LOG_LEVEL;

const logEnabled = logLevels.includes('log');
const errorEnabled = logLevels.includes('error');
const warnEnabled = logLevels.includes('warn');
const debugEnabled = logLevels.includes('debug');
const verboseEnabled = logLevels.includes('verbose');

export class SwanlyCustomLogger implements LoggerService {
    log(message: string) {
        if (logEnabled) {
            console.log(message);
        }
    }

    error(message: string, trace?: string) {
        if (errorEnabled) {
            const errorTrace = trace ? ` :::: Trace: ${trace}` : '';
            console.error(`${message}${errorTrace}`);
        }
    }

    warn(message: string) {
        if (warnEnabled) {
            console.warn(message);
        }
    }

    debug(message: string) {
        if (debugEnabled) {
            console.debug(message);
        }
    }

    verbose(message: string) {
        if (verboseEnabled) {
            console.log(message);
        }
    }
}
