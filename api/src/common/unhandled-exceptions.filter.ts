import { Catch, ArgumentsHost } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import * as Sentry from '@sentry/node';

@Catch()
export class UnhandledExceptionsFilter extends BaseExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        if (host.getType().toString() !== 'graphql') {
            super.catch(exception, host);
            console.error('🔴 Uncaught exception', exception);

            if (exception.status !== 404) {
                const ctx = host.switchToHttp();
                const request = ctx.getRequest();

                Sentry.withScope((scope) => {
                    scope.setTag('instance', request.InstanceName);
                    Sentry.captureException(exception);
                });
            }
        }
    }
}
