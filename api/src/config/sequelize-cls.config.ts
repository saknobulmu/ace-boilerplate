import { Sequelize } from 'sequelize-typescript';
import { createNamespace } from 'cls-hooked';

export const enableSequelizeCLS = () => {
    const session = createNamespace('ace-boilerplate-cls');
    // eslint-disable-next-line no-proto
    (Sequelize as any).__proto__.useCLS(session);
};
