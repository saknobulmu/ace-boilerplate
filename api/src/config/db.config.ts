import { v4 as uuidv4 } from 'uuid';
import { SequelizeModuleOptions } from '@nestjs/sequelize';
import { Dialect, CreateOptions } from 'sequelize/types';

const DB_DEFAULT_POOL_SIZE = 20;

const beforeCreate = (instance: any, createOptions: CreateOptions) => {
    if (instance.constructor.name !== 'AddonSetting' && createOptions.fields.includes('id')) {
        // eslint-disable-next-line no-param-reassign
        instance.id = uuidv4();
    }
};

export const dbConfig = (): SequelizeModuleOptions => ({
    dialect: 'postgres' as Dialect,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    dialectOptions: {
        collate: process.env.DB_DIALECT_OPTIONS_COLLATE || 'utf8_general_ci',
    },
    define: {
        underscored: true,
        freezeTableName: true,
        charset: process.env.DB_CHARSET || 'utf8',
        timestamps: true,
        hooks: {
            beforeCreate,
        },
    },
    pool: {
        max: process.env.DB_POOL_MAX ? parseInt(process.env.DB_POOL_MAX, 10) : DB_DEFAULT_POOL_SIZE,
        idle: 30000,
        acquire: 60000,
    },
    autoLoadModels: true,
    synchronize: false,
    logging: false,
});
