import { JSONResolver, JSONObjectResolver, DateResolver, DateTimeResolver, LongResolver } from 'graphql-scalars';
import { join } from 'path';
import { GqlModuleAsyncOptions } from '@nestjs/graphql';
import { AddonSettingService } from '../modules/core/services/addonsetting.service';
import { CoreModule } from '../modules/core/core.module';
import { JiraModule } from '../modules/jira/jira.module';
import { GraphQLModule } from '../modules/graphql/graphql.module';
import { onConnect, onDisconnect } from './subscription-auth.config';
import { HandleErrorsPlugin } from '../modules/graphql/plugins/errors.plugin';

export const graphQLConfig = (): GqlModuleAsyncOptions => ({
    imports: [CoreModule, JiraModule.instance()],
    inject: [AddonSettingService],
    useFactory: async (addonSettingService: AddonSettingService) => ({
        include: [GraphQLModule],
        context: async (context) => {
            // subscriptions context
            if (context.connection) {
                return context.connection.context;
            }
            // queries and mutations context
            return context;
        },
        playground: process.env.NODE_ENV !== 'production',
        installSubscriptionHandlers: true,
        subscriptions: {
            path: '/subscriptions',
            onConnect: onConnect(addonSettingService),
            onDisconnect,
        },
        resolvers: {
            JSON: JSONResolver,
            JSONObject: JSONObjectResolver,
            Date: DateResolver,
            DateTime: DateTimeResolver,
            Long: LongResolver,
        },
        typePaths: [
            join(process.cwd(), 'src/modules/graphql/types/**/*.graphql'),
            join(process.cwd(), 'src/modules/graphql/types/scalars.graphql'),
        ],
        definitions: {
            path: join(process.cwd(), 'src/modules/graphql/graphql.schema.ts'),
        },
        formatError: (error) => {
            console.log('😡 GraphQL ERR', error);
            if (error?.originalError?.name !== 'UserError') {
                return new Error('Operation failed. Please try again later.');
            }
            return error;
        },
        plugins: [new HandleErrorsPlugin()],
    }),
});
