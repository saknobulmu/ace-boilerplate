import atob from 'atob';
import { verify } from 'jsonwebtoken';
import { AddonSettingService } from '../modules/core/services/addonsetting.service';

const getClientKey = (jwt: string) => {
    const jwtPayload = JSON.parse(atob(jwt.split('.')[1]));
    return jwtPayload.aud ? jwtPayload.aud.pop() : jwtPayload.iss;
};

export const onConnect =
    (addonSettingService: AddonSettingService) =>
    async ({ jwt }: { jwt: string }, _websocket: any, context: any) => {
        try {
            const augmentedContext = { ...context, ioc: {}, clientKey: '' };
            if ((process.env.AC_OPTS || '').includes('no-auth') === false) {
                console.log(`Establishing websocket connection for ${jwt}`);

                const clientKey = getClientKey(jwt);
                const sharedSecret = await addonSettingService.getSharedSecret(clientKey);

                if (verify(jwt, sharedSecret)) {
                    augmentedContext.clientKey = clientKey;
                } else {
                    console.error('Websocket connection failed. jwt Token verification failed');
                    throw new Error('Websocket token verification failed');
                }
            }
            console.log('Websocket connection authorized');
            return augmentedContext;
        } catch (e) {
            console.error(`Error while authorizing websocket connection for ${jwt}`, e);
            throw new Error('Error while authorizing websocket connection');
        }
    };

export const onDisconnect = () => {
    console.log('ws disconnected');
};
