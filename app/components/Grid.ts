/* eslint-disable import/prefer-default-export */
import styled from '@emotion/styled';

type RowProps = {
    justifyContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
    alignItems?: 'flex-start' | 'flex-end' | 'center';
    flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
    shouldFitContainer?: boolean;
    flexGrow?: number;
    width?: number | string;
};
export const Row = styled.div<RowProps>`
    display: flex;
    flex-direction: row;
    flex-grow: ${({ flexGrow }) => (flexGrow !== undefined ? flexGrow : 1)};
    ${({ justifyContent }) => justifyContent && `justify-content: ${justifyContent};`}
    ${({ alignItems }) => alignItems && `align-items: ${alignItems};`}
    ${({ shouldFitContainer }) => shouldFitContainer && `width: 100%;`}
    ${({ flexWrap }) => flexWrap && `flex-wrap: ${flexWrap};`}
    ${({ width }) => width && `width: ${width}${typeof width === 'number' ? 'px' : ''};`}
`;

export const Column = styled(Row)`
    flex-grow: 0;
    width: 100%;
    flex-direction: column;
`;
