import React, { FunctionComponent } from 'react';
import { SideNavigation, NavigationHeader, Header, ButtonItem, Section } from '@atlaskit/side-navigation';
import BulletListIcon from '@atlaskit/icon/glyph/bullet-list';
import { useThemeContext } from 'lib/contexts/themeContext';

const Navigation: FunctionComponent<unknown> = () => {
    const { toggleTheme } = useThemeContext();

    return (
        <SideNavigation label="main">
            <NavigationHeader>
                <Header>Awesome Todos</Header>
            </NavigationHeader>
            <Section>
                <ButtonItem iconBefore={<BulletListIcon label="" />} onClick={toggleTheme}>
                    Toggle Dark Mode
                </ButtonItem>
            </Section>
        </SideNavigation>
    );
};

export default Navigation;
