import styled from '@emotion/styled';
import { LoadingButton, LoadingButtonProps } from '@atlaskit/button';
import { transparentize } from 'polished';
import { hexToRgb } from 'lib/theme/ThemeUtils';

export type ButtonAppearance = 'default' | 'danger' | 'link' | 'primary' | 'subtle' | 'subtle-link' | 'warning';

type ThemedButtonStyle = {
    background: {
        default: string;
        active: string;
        hover: string;
        selected?: string;
    };
    font: {
        default: string;
        active: string;
        hover: string;
        selected?: string;
    };
};

export const getHexColorWithOpacity = (hexColor: string, opacity: number): string => {
    const rgbColor = hexToRgb(hexColor);
    return `rgba(${rgbColor.r}, ${rgbColor.g}, ${rgbColor.b}, ${opacity})`;
};

const DISABLED_BUTTON_TRANSPARENCY = 1 - 0.04;
const getButtonStyle = (props: LoadingButtonProps): ThemedButtonStyle => {
    // @ts-ignore - needed because Theme prop comes from Emotion
    const { theme, isDisabled = false, appearance = 'default' } = props;
    const defaultStyle = {
        background: {
            default: getHexColorWithOpacity(theme.colors.n900, 0.04),
            hover: getHexColorWithOpacity(theme.colors.n900, 0.08),
            active: theme.colors.activeBackground,
            selected: theme.colors.n700,
        },
        font: {
            default: theme.colors.n500,
            hover: theme.colors.n900,
            active: theme.colors.activeFont,
            selected: theme.colors.n0,
        },
    };

    const PRIMARY_DISABLED_BTN_BG = transparentize(DISABLED_BUTTON_TRANSPARENCY, theme.colors.n900);

    // @ts-ignore
    const style = {
        default: defaultStyle,
        subtle: {
            background: {
                default: 'none',
                hover: getHexColorWithOpacity(theme.colors.n900, 0.08),
                active: theme.colors.activeBackground,
            },
            font: {
                default: theme.colors.n600,
                hover: theme.colors.n600,
                active: theme.colors.activeFont,
            },
        },
        primary: {
            background: {
                default: isDisabled ? PRIMARY_DISABLED_BTN_BG : theme.colors.blue,
                hover: isDisabled ? PRIMARY_DISABLED_BTN_BG : '#0065ff',
                active: isDisabled ? PRIMARY_DISABLED_BTN_BG : '#0747a6',
            },
            font: {
                default: isDisabled ? theme.colors.n70 : theme.colors.white,
                hover: isDisabled ? theme.colors.n70 : theme.colors.white,
                active: isDisabled ? theme.colors.n70 : theme.colors.white,
            },
        },
        link: {
            background: {
                default: 'none',
                hover: 'none',
                active: 'none',
            },
            font: {
                default: theme.colors.link,
                hover: theme.colors.link,
                active: theme.colors.n900,
            },
        },
    }[appearance];

    return style || defaultStyle;
};

const ThemedButton = styled(LoadingButton)`
    background: ${(props) => getButtonStyle(props).background[props.isSelected ? 'selected' : 'default']};
    &:hover {
        background: ${(props) => getButtonStyle(props).background.hover} !important;
        text-decoration: ${(props) => (props.appearance === 'link' ? 'underline' : 'inherit')};
        text-decoration-color: ${(props) => getButtonStyle(props).font.hover};
    }
    &:active {
        background: ${(props) => getButtonStyle(props).background.active} !important;
        > span,
        > span > span > svg {
            color: ${(props) => getButtonStyle(props).font.active} !important;
        }
    }
    > span {
        color: ${(props) => getButtonStyle(props).font[props.isSelected ? 'selected' : 'default']};
        &:hover {
            color: ${(props) => getButtonStyle(props).font.hover};
        }
    }
`;

export default ThemedButton;
