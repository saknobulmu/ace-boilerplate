import styled from '@emotion/styled';
import { Row } from 'components/Grid';

const ThemedTextFieldRow = styled(Row)`
    textarea[name='inlineEdit'] {
        background-color: ${({ theme }) => theme.colors.n10};
        color: ${({ theme }) => theme.colors.n900};
        border-color: ${({ theme }) => theme.colors.n40};
        &:focus-within {
            border-color: ${({ theme }) => theme.colors.lightBlue};
        }
        &:hover {
            background-color: ${({ theme }) => theme.colors.n10} !important;
        }
    }
`;

export default ThemedTextFieldRow;
