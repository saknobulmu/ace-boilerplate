import styled from '@emotion/styled';
import { ModalBody } from '@atlaskit/modal-dialog';

const ThemedModalBody = styled(ModalBody)`
    color: ${({ theme }) => theme.colors.n900};
`;

export default ThemedModalBody;
