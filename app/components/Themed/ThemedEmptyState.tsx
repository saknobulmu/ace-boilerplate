import React, { FunctionComponent, ReactNode } from 'react';
import styled from '@emotion/styled';
import EmptyState from '@atlaskit/empty-state';
import { RenderImageProps } from '@atlaskit/empty-state/dist/types/EmptyState';

type Sizes = 'narrow' | 'wide';
type EmptyStateProps = {
    header: string;
    description?: ReactNode;
    size?: Sizes;
    imageUrl?: string;
    maxImageWidth?: number;
    maxImageHeight?: number;
    primaryAction?: ReactNode;
    renderImage?: (props: RenderImageProps) => React.ReactNode;
    secondaryAction?: ReactNode;
    tertiaryAction?: ReactNode;
    isLoading?: boolean;
    imageWidth?: number;
    imageHeight?: number;
};

const Container = styled.div`
    h4,
    p {
        color: ${({ theme }) => theme.colors.n800};
    }
`;

type ThemedEmptyStateProps = EmptyStateProps & {
    children?: React.ReactNode;
};

const ThemedEmptyState: FunctionComponent<ThemedEmptyStateProps> = ({ children, ...props }: ThemedEmptyStateProps) => (
    <Container>
        <EmptyState {...props}>{children}</EmptyState>
    </Container>
);

ThemedEmptyState.defaultProps = {
    children: undefined,
    description: undefined,
    size: undefined,
    imageUrl: undefined,
    maxImageWidth: undefined,
    maxImageHeight: undefined,
    primaryAction: undefined,
    renderImage: undefined,
    secondaryAction: undefined,
    tertiaryAction: undefined,
    isLoading: undefined,
    imageWidth: undefined,
    imageHeight: undefined,
};

export default ThemedEmptyState;
