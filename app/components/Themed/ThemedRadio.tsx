import React, { FunctionComponent } from 'react';
import styled from '@emotion/styled';
import { Radio } from '@atlaskit/radio';
import { RadioProps } from '@atlaskit/radio/types';

const Container = styled.div`
    label[class$='-Radio'] {
        color: ${({ theme }) => theme.colors.n900};
    }
`;

const ThemedRadio: FunctionComponent<RadioProps> = ({ children, ...props }: RadioProps) => (
    <Container>
        <Radio {...props}>{children}</Radio>
    </Container>
);

export default ThemedRadio;
