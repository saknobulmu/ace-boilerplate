import React, { FunctionComponent, PropsWithChildren } from 'react';
import styled from '@emotion/styled';
import SectionMessage from '@atlaskit/section-message';
import { Appearance } from '@atlaskit/section-message/dist/types/types';

const Container = styled.div`
    > section {
        background-color: ${({ theme }) => theme.colors.warningBackground} !important;
        color: ${({ theme }) => theme.colors.n900};
    }
`;

const ThemedSectionMessage: FunctionComponent<PropsWithChildren<{ appearance?: Appearance }>> = ({
    children,
    ...props
}) => (
    <Container>
        <SectionMessage {...props}>{children}</SectionMessage>
    </Container>
);

export default ThemedSectionMessage;
