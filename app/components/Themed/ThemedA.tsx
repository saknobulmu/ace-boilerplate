import styled from '@emotion/styled';

const ThemedA = styled.a`
    color: ${({ theme }) => theme.colors.link};
    &:hover {
        color: ${({ theme }) => theme.colors.link};
    }
`;

export default ThemedA;
