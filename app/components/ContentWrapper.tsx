import React, { FunctionComponent, PropsWithChildren } from 'react';
import { gridSize } from '@atlaskit/theme';
import styled from '@emotion/styled';

const Padding = styled.div`
    height: 100vh;
    margin: ${gridSize() * 4}px ${gridSize() * 8}px;
    padding-bottom: ${gridSize() * 3}px;
`;

const ContentWrapper: FunctionComponent<PropsWithChildren<unknown>> = ({ children }) => <Padding>{children}</Padding>;

export default ContentWrapper;
