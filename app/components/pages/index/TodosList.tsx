import { UpdateTodoInput, useRealtimeTodos, useUpdateTodoMutation } from 'lib/graphql/queries/todo';
import { FunctionComponent, useCallback } from 'react';
import PageHeader from '@atlaskit/page-header';
import DynamicTable from '@atlaskit/dynamic-table';
import { RowType } from '@atlaskit/dynamic-table/dist/types/types';
import { Checkbox } from '@atlaskit/checkbox';
import InlineEdit from '@atlaskit/inline-edit';
import TextField from '@atlaskit/textfield';
import { Todo } from 'lib/types/todo';
import styled from '@emotion/styled';
import ContentWrapper from 'components/ContentWrapper';
import NewTodoModal from './NewTodoModal';

const TABLE_HEAD = {
    cells: [
        {
            content: 'Is done',
            key: 'done',
            isSortable: true,
        },
        {
            content: 'Title',
            key: 'title',
            isSortable: true,
        },
        {
            content: 'Created',
            key: 'createdAt',
        },
    ],
};

type TodoTextProps = {
    isDone: boolean;
};
const TodoText = styled.span<TodoTextProps>`
    ${({ isDone }) => isDone && 'text-decoration: line-through;'}
    ${({ isDone }) => isDone && 'color: #aaa;'}
`;

type FormatData = (data: { todos: Todo[] }, update: (input: UpdateTodoInput) => Promise<unknown>) => RowType[];
const formatData: FormatData = (data, update) => {
    if (!data || !data.todos) {
        return [];
    }

    return data.todos.map((todo) => ({
        key: todo.id,
        cells: [
            {
                content: <Checkbox isChecked={todo.done} onChange={() => update({ id: todo.id, done: !todo.done })} />,
                key: `todo-done-${todo.id}`,
            },
            {
                content: (
                    <InlineEdit
                        defaultValue={todo.title}
                        editView={(fieldProps) => <TextField {...fieldProps} autoFocus />}
                        readView={() => <TodoText isDone={todo.done}>{todo.title}</TodoText>}
                        onConfirm={(title) => update({ id: todo.id, title })}
                    />
                ),
                key: `todo-title-${todo.id}`,
            },
            {
                content: <TodoText isDone={todo.done}>{todo.createdAt}</TodoText>,
                key: `todo-createdAt-${todo.id}`,
            },
        ],
    }));
};

const TodosList: FunctionComponent<unknown> = () => {
    const { loading, data = { todos: [] } } = useRealtimeTodos();

    const [updateTodoMutation] = useUpdateTodoMutation();

    const update = useCallback(
        (input: UpdateTodoInput) =>
            updateTodoMutation({
                variables: { input },
            }),
        [updateTodoMutation],
    );

    return (
        <ContentWrapper>
            <PageHeader actions={<NewTodoModal />} disableTitleStyles>
                <h1>Todos</h1>
            </PageHeader>
            <DynamicTable isLoading={loading} head={TABLE_HEAD} rows={formatData(data, update)} rowsPerPage={10} />
        </ContentWrapper>
    );
};

export default TodosList;
