import { FunctionComponent, useState, useCallback } from 'react';
import Modal, { ModalHeader } from '@atlaskit/modal-dialog';
import Form, { Field, ErrorMessage, FormFooter } from '@atlaskit/form';
import TextField from '@atlaskit/textfield';
import { ButtonGroup } from '@atlaskit/button';
import { CreateTodoInput, useCreateTodoMutation } from 'lib/graphql/queries/todo';
import ThemedModalBody from 'components/Themed/ThemedModalBody';
import ThemedButton from 'components/Themed/ThemedButton';
import { FlagTypes, useFlags } from 'lib/contexts/flagContext';

type FormType = {
    title: string;
};

type TodoFormProps = {
    onSave: (input: CreateTodoInput) => void;
    onCancel: () => void;
};

const TodoForm: FunctionComponent<TodoFormProps> = ({ onSave, onCancel }) => (
    <Form<FormType> onSubmit={onSave}>
        {({ formProps }) => (
            <form {...formProps}>
                <Field name="title" label="New Todo" isRequired>
                    {({ fieldProps, error }) => (
                        <>
                            <TextField autoComplete="off" {...fieldProps} />
                            {error && <ErrorMessage>Title is required</ErrorMessage>}
                        </>
                    )}
                </Field>
                <FormFooter>
                    <ButtonGroup>
                        <ThemedButton appearance="subtle" onClick={onCancel}>
                            Cancel
                        </ThemedButton>
                        <ThemedButton appearance="primary" type="submit">
                            Create
                        </ThemedButton>
                    </ButtonGroup>
                </FormFooter>
            </form>
        )}
    </Form>
);

const NewTodoModal: FunctionComponent<unknown> = () => {
    const [isModalOpen, setModalOpen] = useState(false);

    const { addFlag } = useFlags();

    const openModal = useCallback(() => setModalOpen(true), []);
    const closeModal = useCallback(() => setModalOpen(false), []);

    const [createTodoMutation] = useCreateTodoMutation();

    const onSave = useCallback(
        async (input: CreateTodoInput) => {
            try {
                await createTodoMutation({
                    variables: { input },
                });
                closeModal();
            } catch (error) {
                addFlag({
                    title: 'Error adding Todo',
                    description: 'Try again in a few moments. If the error persist, please contact support.',
                    type: FlagTypes.ERROR,
                });
            }
        },
        [addFlag, closeModal, createTodoMutation],
    );

    return (
        <>
            <ThemedButton appearance="primary" onClick={openModal}>
                Create Todo
            </ThemedButton>
            {isModalOpen && (
                <Modal>
                    <ModalHeader>Create Todo</ModalHeader>
                    <ThemedModalBody>
                        <TodoForm onSave={onSave} onCancel={closeModal} />
                    </ThemedModalBody>
                </Modal>
            )}
        </>
    );
};

export default NewTodoModal;
