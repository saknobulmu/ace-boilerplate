// This file sets a custom webpack configuration to use your Next.js app
// with Sentry.
// https://nextjs.org/docs/api-reference/next.config.js/introduction
// https://docs.sentry.io/platforms/javascript/guides/nextjs/
const { withSentryConfig } = require('@sentry/nextjs');
const isEmpty = require('lodash/isEmpty');

const isDev = process.env.NODE_ENV === 'development';
const isSentryEnabled =
    (isDev && !isEmpty(process.env.SENTRY_DSN_APP)) ||
    (!isDev &&
        !isEmpty(process.env.SENTRY_DSN_APP) &&
        !isEmpty(process.env.SENTRY_URL) &&
        !isEmpty(process.env.SENTRY_ORG) &&
        !isEmpty(process.env.SENTRY_PROJECT) &&
        !isEmpty(process.env.SENTRY_AUTH_TOKEN));

const moduleExports = {
    reactStrictMode: true,
    basePath: '/web',
    publicRuntimeConfig: {
        isDev,
        isSentryEnabled,
        appKey: process.env.APP_KEY,
        segmentWriteKey: process.env.SEGMENT_WRITE_KEY,
        environmentType: process.env.ENVIRONMENT_TYPE,
        sentryDsn: process.env.SENTRY_DSN_APP,
    },
};

const SentryWebpackPluginOptions = {
    // Additional config options for the Sentry Webpack plugin. Keep in mind that
    // the following options are set automatically, and overriding them is not
    // recommended:
    //   release, url, org, project, authToken, configFile, stripPrefix,
    //   urlPrefix, include, ignore

    silent: true, // Suppresses all logs
    // For all available options, see:
    // https://github.com/getsentry/sentry-webpack-plugin#options.
};

// Make sure adding Sentry options is the last code to run before exporting, to
// ensure that your source maps include changes from all other Webpack plugins
module.exports = isSentryEnabled ? withSentryConfig(moduleExports, SentryWebpackPluginOptions) : moduleExports;
