// This file configures the initialization of Sentry on the browser.
// The config you add here will be used whenever a page is visited.
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

import * as Sentry from '@sentry/nextjs';
import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

if (publicRuntimeConfig.isSentryEnabled) {
    Sentry.init({
        dsn: publicRuntimeConfig.sentryDsn,
        environment: publicRuntimeConfig.environmentType,
    });
}
