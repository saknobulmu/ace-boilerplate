import { JiraIssue } from './jira';

export type WindowAP = {
    context: {
        getToken: () => Promise<string>;
    };
    request: (opts: unknown) => Promise<void>;
    jira: {
        openCreateIssueDialog: (
            callback: (createdIssues: JiraIssue[]) => void,
            params: { pid: number; issueType: number; fields: Record<string, unknown> },
        ) => void;
    };
};
