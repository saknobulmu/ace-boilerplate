export interface JiraIssueFixVersion {
    id?: string;
}

export type JiraIssuePriority = {
    iconUrl?: string;
    name?: string;
};

export type JiraIssueStatusCategory = {
    key?: string;
    colorName?: string;
    name?: string;
};

export type JiraIssueStatus = {
    iconUrl?: string;
    name?: string;
    statusCategory?: JiraIssueStatusCategory;
};

export type JiraIssueTimetracking = {
    progress?: number;
    total?: number;
    percent?: number;
};

export type JiraIssueAssignee = {
    displayName?: string;
    avatarUrl?: string;
};

export interface JiraIssueTypeProject {
    id?: string;
    key?: string;
    name?: string;
}

export interface JiraIssueTypeScope {
    type?: string;
    project?: JiraIssueTypeProject;
}

export interface JiraIssueTypeField {
    required?: boolean;
    key?: string;
}

export interface JiraIssueType {
    id: string;
    name: string;
    iconUrl?: string;
    subtask?: boolean;
    description?: string;
    scope?: JiraIssueTypeScope;
    projectIds?: string[];
    fields?: JiraIssueTypeField[];
}

export interface JiraIssueProject {
    id?: string;
}

export type JiraIssueFields = {
    summary?: string;
    priority?: JiraIssuePriority;
    status?: JiraIssueStatus;
    resolutiondate?: string;
    aggregateprogress?: JiraIssueTimetracking;
    statuscategorychangedate?: string;
    aggregatetimeoriginalestimate?: number;
    timeoriginalestimate?: number;
    workratio?: string;
    aggregatetimeestimate?: number;
    storyPoints?: number;
    projectKey?: string;
    projectAvatar?: string;
    assignee?: JiraIssueAssignee;
    fixVersionId?: number;
    fixVersions?: JiraIssueFixVersion[];
    issuetype?: JiraIssueType;
    project?: JiraIssueProject;
};

export type JiraIssue = {
    id?: string;
    self?: string;
    key?: string;
    fields?: JiraIssueFields;
};
