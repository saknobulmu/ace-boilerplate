declare module '@emotion/react' {
    export interface Theme extends ThemeDefault, ThemePartial {
        colors: ThemeColors;
    }
}

export enum ThemeMode {
    LIGHT = 'light',
    DARK = 'dark',
}

interface ThemeColorsDefault {
    black: string;
    white: string;
    blue: string;
    darkBlue: string;
    green: string;
    grey: string;
    lightGrey: string;
    yellow: string;
    lightBlue: string;
    transparentize: (color: string, amount: number) => string;
}

interface ThemeColorsPartial {
    primary: string;
    background: string;
    border: string;
    navbar: string;
    link: string;
    activeBackground: string;
    activeFont: string;
    warningBackground: string;
    label: string;
    n1000: string;
    n900: string;
    n800: string;
    n700: string;
    n600: string;
    n500: string;
    n400: string;
    n300: string;
    n200: string;
    n100: string;
    n90: string;
    n80: string;
    n70: string;
    n60: string;
    n50: string;
    n40: string;
    n30: string;
    n20: string;
    n10: string;
    n0: string;
}

interface ThemeColorsFeatures {
    features: {
        statuses: {
            toDo: string;
            inProgress: string;
            done: string;
        };
    };
}

interface ThemeColors extends ThemeColorsDefault, ThemeColorsPartial, ThemeColorsFeatures {}

export interface ThemeDefault {
    name: ThemeMode;
    colors: ThemeColorsDefault;
}

export interface ThemePartial {
    name: ThemeMode;
    colors: ThemeColorsPartial;
}

export type RGBType = { r?: number | null; g?: number | null; b?: number | null };
