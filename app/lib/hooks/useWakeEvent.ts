import { useEffect, useRef } from 'react';
import ms from 'ms';

const EVENT_TIMEOUT = ms('5 seconds');
const EVENT_THRESHOLD = ms('2 seconds');

const useWakeEvent = (callback: () => void): void => {
    const lastTimeRef = useRef<number>(Date.now());

    useEffect(() => {
        const wakeEventInterval = setInterval(() => {
            const now = Date.now();
            if (now > lastTimeRef.current + EVENT_TIMEOUT + EVENT_THRESHOLD) {
                callback();
            }
            lastTimeRef.current = now;
        }, EVENT_TIMEOUT);

        return () => clearInterval(wakeEventInterval);
    }, [callback]);
};

export default useWakeEvent;
