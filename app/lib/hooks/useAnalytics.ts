import { useCallback } from 'react';
import getConfig from 'next/config';

type SegmentAnalyticsProps = {
    event: string;
    properties: Record<string, unknown>;
};

type OnSegmentTrack = (eventObject: SegmentAnalyticsProps) => void;

type UseAnalyticsType = {
    trackEvent: OnSegmentTrack;
};

const useAnalytics = (): UseAnalyticsType => {
    // TODO Get instance license to identify requests

    const license = '';

    const trackEvent = useCallback<OnSegmentTrack>(
        (eventObject) => {
            const { publicRuntimeConfig } = getConfig();
            if (publicRuntimeConfig.segmentWriteKey && !publicRuntimeConfig.isDev) {
                const { event, properties: getProperties } = eventObject;

                const properties = {
                    ...getProperties,
                    addon_key: publicRuntimeConfig.appKey,
                };

                if (typeof window !== 'undefined') {
                    if (license) {
                        window.analytics?.identify(license);
                    }
                    return window.analytics?.track(event, properties);
                }
            }
            return eventObject;
        },
        [license],
    );

    return {
        trackEvent,
    };
};

export default useAnalytics;
