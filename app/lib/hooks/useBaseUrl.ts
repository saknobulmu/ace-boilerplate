import { useMemo } from 'react';
import { useRouter } from 'next/router';

const useBaseUrl = (): string => {
    const { query } = useRouter();

    const baseUrl = useMemo(() => {
        if (query && query.xdm_e !== undefined && query.cp !== undefined) {
            return `${query.xdm_e}${query.cp}`;
        }
        return typeof window !== 'undefined' ? `${window.location?.protocol}//${window.location?.host}` : '';
    }, [query]);

    return baseUrl;
};

export default useBaseUrl;
