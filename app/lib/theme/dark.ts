import { ThemeMode, ThemePartial } from 'lib/types/theme';

const baseColors = {
    n1000: '#FAFBFC',
    n900: '#FAFBFC',
    n800: '#F4F5F7',
    n700: '#EBECF0',
    n600: '#D9DDE3',
    n500: '#C7CED6',
    n400: '#B5BEC9',
    n300: '#A3AEBD',
    n200: '#929FB0',
    n100: '#8392A5',
    n90: '#718298',
    n80: '#65758B',
    n70: '#5A697C',
    n60: '#515F70',
    n50: '#475262',
    n40: '#3F4855',
    n30: '#373F49',
    n20: '#2F353D',
    n10: '#262B31',
    n0: '#202328',
};

const theme: ThemePartial = {
    name: ThemeMode.DARK,
    colors: {
        primary: '#000000',
        background: '#ffffff',
        border: '#ebecf0',
        navbar: '#fafbfc',
        label: '#6b778c',
        link: '#4c9aff',
        activeFont: '#0747a6',
        activeBackground: '#b3d4ff',
        warningBackground: baseColors.n20,
        ...baseColors,
    },
};

export default theme;
