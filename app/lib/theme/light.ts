import { ThemeMode, ThemePartial } from 'lib/types/theme';

export const THEME_NAME = 'light';

const baseColors = {
    n1000: '#000000',
    n900: '#091E42',
    n800: '#172B4D',
    n700: '#253858',
    n600: '#344563',
    n500: '#42526E',
    n400: '#505F79',
    n300: '#5E6C84',
    n200: '#6B778C',
    n100: '#7A869A',
    n90: '#8993A4',
    n80: '#97A0AF',
    n70: '#A5ADBA',
    n60: '#B3BAC5',
    n50: '#C1C7D0',
    n40: '#DFE1E6',
    n30: '#EBECF0',
    n20: '#F4F5F7',
    n10: '#FAFBFC',
    n0: '#FFFFFF',
};

const theme: ThemePartial = {
    name: ThemeMode.LIGHT,
    colors: {
        primary: '#172b4d',
        background: '#ffffff',
        border: '#ebecf0',
        navbar: '#fafbfc',
        label: '#6b778c',
        link: '#0052cc',
        activeFont: '#0052cc',
        activeBackground: '#b3d4ff',
        warningBackground: '#FFFAE6',
        ...baseColors,
    },
};

export default theme;
