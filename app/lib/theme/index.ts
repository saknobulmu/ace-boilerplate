import { transparentize } from 'polished';
import { ThemeDefault, ThemeMode } from 'lib/types/theme';
import { Theme } from '@emotion/react';
import lightTheme from './light';
import darkTheme from './dark';

const DARK_MODE_ENABLED_VAR = 'darkMode.enabled';

const defaults: ThemeDefault = {
    name: ThemeMode.LIGHT,
    colors: {
        black: '#000000',
        white: '#ffffff',
        blue: '#0052cc',
        darkBlue: '#091E42',
        green: '#36b37e',
        grey: '#dfe1e6',
        lightGrey: '#dddddd',
        yellow: '#ffab00',
        lightBlue: '#4c9aff',
        transparentize: (color: string, amount: number) => transparentize(amount, color),
    },
};

const features = {
    colors: {
        features: {
            statuses: {
                toDo: defaults.colors.grey,
                inProgress: defaults.colors.blue,
                done: defaults.colors.green,
            },
        },
    },
};

const light: Theme = {
    ...lightTheme,
    colors: {
        ...defaults.colors,
        ...lightTheme.colors,
        ...features.colors,
    },
};

const dark: Theme = {
    ...darkTheme,
    colors: {
        ...defaults.colors,
        ...darkTheme.colors,
        ...features.colors,
    },
};

// eslint-disable-next-line import/prefer-default-export
export { light, dark, DARK_MODE_ENABLED_VAR };
