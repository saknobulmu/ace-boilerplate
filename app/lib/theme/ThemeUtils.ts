import { Theme } from '@emotion/react';
import { RGBType, ThemeMode } from 'lib/types/theme';

const lightenDarkenColor = (color: string, amount: number): string => {
    let usePound = false;
    let col = color;
    if (col[0] === '#') {
        col = col.slice(1);
        usePound = true;
    }

    const num = parseInt(col, 16);
    // eslint-disable-next-line no-bitwise
    let r = (num >> 16) + amount;

    if (r > 255) {
        r = 255;
    } else if (r < 0) {
        r = 0;
    }

    // eslint-disable-next-line no-bitwise
    let b = ((num >> 8) & 0x00ff) + amount;

    if (b > 255) {
        b = 255;
    } else if (b < 0) {
        b = 0;
    }

    // eslint-disable-next-line no-bitwise
    let g = (num & 0x0000ff) + amount;

    if (g > 255) {
        g = 255;
    } else if (g < 0) {
        g = 0;
    }

    // eslint-disable-next-line no-bitwise
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
};

export const darkenColor = (color: string, amount: number): string => lightenDarkenColor(color, -Math.abs(amount));

export const lightenColor = (color: string, amount: number): string => lightenDarkenColor(color, Math.abs(amount));

export const getButtonBackgroundColor = (props: { theme: Theme }, amount: number): string => {
    const { theme } = props;
    const color = theme.colors.n0;
    return theme.name === ThemeMode.DARK ? lightenColor(color, amount) : darkenColor(color, amount);
};

export const getFontColorForInactiveButton = (theme: Theme): string =>
    theme.name === ThemeMode.LIGHT ? theme.colors.n500 : theme.colors.n900;

export const hexToRgb = (hex: string): RGBType => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if (result) {
        return { r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16) };
    }
    return { r: null, g: null, b: null };
};
