import {
    ComponentType,
    createContext,
    FocusEvent,
    FunctionComponent,
    MouseEventHandler,
    PropsWithChildren,
    ReactNode,
    useCallback,
    useContext,
    useMemo,
    useState,
} from 'react';
import Flag, { FlagGroup, ActionsType, AppearanceTypes } from '@atlaskit/flag';
import SuccessIcon from '@atlaskit/icon/glyph/check-circle';
import InfoIcon from '@atlaskit/icon/glyph/info';
import WarningIcon from '@atlaskit/icon/glyph/warning';
import ErrorIcon from '@atlaskit/icon/glyph/error';
import { CustomThemeButtonProps } from '@atlaskit/button';
import { G300, N500, Y200, R400 } from '@atlaskit/theme/colors';
import { noop, uniqueId } from 'lodash';

export enum FlagTypes {
    SUCCESS,
    ERROR,
    WARNING,
    INFO,
}

const flagIcons = {
    [FlagTypes.SUCCESS]: <SuccessIcon label="Success" primaryColor={G300} />,
    [FlagTypes.ERROR]: <ErrorIcon label="Error" primaryColor={R400} />,
    [FlagTypes.WARNING]: <WarningIcon label="Warning" primaryColor={Y200} />,
    [FlagTypes.INFO]: <InfoIcon label="Info" primaryColor={N500} />,
};

export type FlagType = {
    type: FlagTypes;
    actions?: ActionsType;
    appearance?: AppearanceTypes;
    description?: ReactNode;
    title: ReactNode;
    linkComponent?: ComponentType<CustomThemeButtonProps>;
    testId?: string;
    onBlur?: (e: FocusEvent<HTMLElement>) => void;
    onFocus?: (e: FocusEvent<HTMLElement>) => void;
    onMouseOut?: MouseEventHandler;
    onMouseOver?: MouseEventHandler;
    timeout?: number;
};

type FlagTypeWithId = FlagType & {
    id: string | number;
};

type FlagContextType = {
    addFlag: (flag: FlagType) => void;
};

const FlagContext = createContext<FlagContextType>({ addFlag: noop });

const FlagProvider: FunctionComponent<PropsWithChildren<unknown>> = ({ children }) => {
    const [flags, setFlags] = useState<FlagTypeWithId[]>([]);

    const dismissFlag = useCallback(
        (flagId: string | number) => setFlags((prevFlags) => prevFlags.filter(({ id }) => id !== flagId)),
        [],
    );

    const addFlag = useCallback(
        (flag: FlagType) => {
            const flagId = uniqueId('flag_');
            setFlags((prevFlags) => [{ ...flag, id: flagId }, ...prevFlags]);
            setTimeout(() => {
                dismissFlag(flagId);
            }, flag.timeout || 4000);
        },
        [dismissFlag],
    );

    const flagsToRender = useMemo(
        () => flags.map((flag) => <Flag {...flag} icon={flagIcons[flag.type]} key={flag.id} />),
        [flags],
    );

    const providerValue = useMemo(() => ({ addFlag }), [addFlag]);

    return (
        <FlagContext.Provider value={providerValue}>
            {children}
            <FlagGroup onDismissed={dismissFlag}>{flagsToRender}</FlagGroup>
        </FlagContext.Provider>
    );
};

export const useFlags = (): FlagContextType => useContext(FlagContext);

export default FlagProvider;
