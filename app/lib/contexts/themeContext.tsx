import noop from 'lodash/noop';
import { useMemo, useState, FunctionComponent, PropsWithChildren, createContext, useContext, useCallback } from 'react';
import { css, ThemeProvider, Global, Theme } from '@emotion/react';
import { ThemeMode } from 'lib/types/theme';
import { dark, light } from 'lib/theme';

export const DATE_PICKER_ID = 'themed-calendar-popup';

const GlobalStyle: FunctionComponent<{ theme: Theme }> = ({ theme }) => (
    <Global
        styles={css`
            body {
                margin: 0;
                overflow-y: hidden;
                background-color: ${theme.colors.n0};
                color: ${theme.colors.n900};
            }

            div[role='tooltip'].Tooltip {
                color: ${theme.colors.n0};
                background-color: ${theme.colors.n900};
            }
            div[role='dialog'] {
                background-color: ${theme.colors.n0};
                box-shadow: 0 0 6px 0 ${theme.colors.transparentize(theme.colors.n1000, 0.84)};
            }
            div[role='dialog'] > div > footer > div > div:nth-of-type(2) > button {
                color: ${theme.colors.n500} !important;
            }
            div[data-testid='dialogTutorialVideo'] {
                background-color: transparent;
                box-shadow: none;
            }
            h1,
            h2,
            h3,
            h4,
            div[role='dialog'] > div > div > div > h4 {
                color: ${theme.colors.n800};
            }
            div.react-select__menu {
                border: 1px solid ${theme.colors.transparentize(theme.colors.n40, 0.5)};
                border-width: 0 0 0 1px;
            }
            /* Full screen overlay when displaying modal confirmation dialog */
            div[data-focus-lock-disabled='false'] > div:first-of-type {
                background: ${theme.colors.transparentize(theme.colors.n900, 0.46)};
            }
            div[data-testid='spotlight--dialog'] > div {
                background: #6554c0;
            }
            div[role='dialog'] > div > div > div > p {
                color: ${theme.colors.n900};
            }
            /* DatePickers */
            div#${DATE_PICKER_ID} {
                /* Current Month title */
                div[aria-label='calendar'] > div > div:nth-of-type(2) {
                    color: ${theme.colors.n800};
                }
                /* The calendar background colour */
                > div,
                div[aria-label='calendar'] {
                    background-color: ${theme.colors.n0};
                }
                /* Months colours */
                thead > tr > th {
                    color: ${theme.colors.n200};
                }
                /* Dates hover */
                td > div:hover {
                    background-color: ${theme.colors.n30};
                    color: ${theme.colors.n800};
                }
                /* Buttons */
                > div > button,
                button {
                    > span {
                        color: ${theme.colors.n500} !important; /* Clear button */
                    }
                    > span > span {
                        color: ${theme.colors.n70}; /* Last/After month buttons */
                    }
                    background-color: ${theme.colors.n0};
                    &:hover {
                        background-color: ${theme.colors.transparentize(theme.colors.n900, 0.92)};
                        > span {
                            color: ${theme.colors.n800} !important;
                        }
                        > span > span {
                            color: ${theme.colors.n400};
                        }
                    }
                    &:active {
                        background-color: ${theme.colors.activeBackground};
                        > span {
                            color: ${theme.colors.activeFont} !important;
                        }
                        > span > span {
                            color: ${theme.colors.n40};
                        }
                    }
                }
            }
        `}
    />
);

type ThemeContextType = {
    toggleTheme: () => void;
};

const ThemeContext = createContext<ThemeContextType>({
    toggleTheme: noop,
});

const AppThemeProvider: FunctionComponent<PropsWithChildren<unknown>> = ({ children }) => {
    const [themeMode, setThemeMode] = useState(ThemeMode.LIGHT);

    const toggleTheme = useCallback(
        () => setThemeMode((current) => (current === ThemeMode.DARK ? ThemeMode.LIGHT : ThemeMode.DARK)),
        [],
    );

    const theme = useMemo(() => (themeMode === ThemeMode.LIGHT ? light : dark), [themeMode]);

    const providerValue = useMemo(
        () => ({
            toggleTheme,
        }),
        [toggleTheme],
    );

    return (
        <ThemeContext.Provider value={providerValue}>
            <GlobalStyle theme={theme} />
            <ThemeProvider theme={theme}>{children}</ThemeProvider>
        </ThemeContext.Provider>
    );
};

export const useThemeContext = (): ThemeContextType => useContext(ThemeContext);

export default AppThemeProvider;
