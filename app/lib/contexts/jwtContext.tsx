import React, {
    createContext,
    FunctionComponent,
    PropsWithChildren,
    useCallback,
    useContext,
    useEffect,
    useRef,
} from 'react';
import ms from 'ms';
import useWakeEvent from 'lib/hooks/useWakeEvent';

type JwtProviderProps = {
    initialToken: string;
};

type JwtContextType = () => string;

const JwtContext = createContext<JwtContextType>(() => '');

const JwtContextProvider: FunctionComponent<PropsWithChildren<JwtProviderProps>> = ({ initialToken, children }) => {
    const jwtRef = useRef(initialToken);

    const refreshToken = useCallback(async () => {
        if (window?.AP?.context?.getToken) {
            const token = await window.AP.context.getToken();

            if (jwtRef.current !== token) {
                jwtRef.current = token;
            }
        } else {
            // eslint-disable-next-line no-console
            console.warn('🟡 [useJwtToken.ts] window.AP not ready');
        }
    }, []);

    useWakeEvent(refreshToken);

    useEffect(() => {
        const intervalId = setInterval(async () => {
            await refreshToken();
        }, ms('10 minutes'));
        return () => {
            clearInterval(intervalId);
        };
    }, [refreshToken]);

    const getToken = useCallback(() => jwtRef.current, []);

    return <JwtContext.Provider value={getToken}>{children}</JwtContext.Provider>;
};

export const useJwtToken = (): JwtContextType => useContext(JwtContext);

export default JwtContextProvider;
