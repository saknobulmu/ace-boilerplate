import { createContext, FunctionComponent, PropsWithChildren, useContext, useMemo } from 'react';
import getConfig from 'next/config';
import useBaseUrl from 'lib/hooks/useBaseUrl';
import * as Sentry from '@sentry/nextjs';

const { publicRuntimeConfig } = getConfig();

type EnvironmentContextType = {
    appKey?: string;
    segmentWriteKey?: string;
    sentryDsn?: string;
    isDev: boolean;
};

const environmentContext = createContext<EnvironmentContextType>({
    isDev: true,
});

const EnvironmentContextProvider: FunctionComponent<PropsWithChildren<unknown>> = ({ children }) => {
    const baseUrl = useBaseUrl();

    Sentry.setTag('instance', baseUrl);

    const value = useMemo(
        () => ({
            appKey: publicRuntimeConfig.appKey,
            segmentWriteKey: publicRuntimeConfig.segmentWriteKey,
            isDev: publicRuntimeConfig.isDev,
            sentryDsn: publicRuntimeConfig.sentryDsn,
        }),
        [],
    );
    return <environmentContext.Provider value={value}>{children}</environmentContext.Provider>;
};

export const useEnvironmentVariables = (): EnvironmentContextType => useContext(environmentContext);

export default EnvironmentContextProvider;
