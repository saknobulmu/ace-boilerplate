import { gql, MutationTuple, QueryResult, useMutation, useQuery, useSubscription } from '@apollo/client';
import { Todo } from 'lib/types/todo';

const TODO_FIELDS = `
    id
    title
    done
    createdAt
`;

export type GetTodosQueryType = { todos: Todo[] };
export const getTodos = gql`
    query todos {
        todos {
            ${TODO_FIELDS}
        }
    }
`;

export const todoCreated = gql`
    subscription todoCreated {
        todoCreated {
            ${TODO_FIELDS}
        }
    }
`;

export const todoUpdated = gql`
    subscription todoUpdated {
        todoUpdated {
            ${TODO_FIELDS}
        }
    }
`;

export type CreateTodoInput = {
    title: string;
    done?: boolean;
};
export const createTodo = gql`
    mutation createTodo($input: TodoInput!) {
        createTodo(input: $input) {
            id
            title
            done
            createdAt
        }
    }
`;

export type UpdateTodoInput = {
    id: string;
    title?: string;
    done?: boolean;
};
export const updateTodo = gql`
    mutation updateTodo($input: UpdateTodoInput!) {
        updateTodo(input: $input) {
            id
            title
            done
            createdAt
        }
    }
`;

type CreateTodoMutationVariables = { input: CreateTodoInput };
export const useCreateTodoMutation = (): MutationTuple<Todo, CreateTodoMutationVariables> =>
    useMutation<Todo, CreateTodoMutationVariables>(createTodo);

type UpdateTodoMutationVariables = { input: UpdateTodoInput };
export const useUpdateTodoMutation = (): MutationTuple<Todo, UpdateTodoMutationVariables> =>
    useMutation<Todo, UpdateTodoMutationVariables>(updateTodo);

export const useRealtimeTodos = (): QueryResult<GetTodosQueryType> => {
    const { refetch, ...rest } = useQuery(getTodos);

    useSubscription(todoCreated, {
        onSubscriptionData: ({ subscriptionData, client }) => {
            if (!subscriptionData.data) {
                return;
            }

            const prev = client.readQuery({ query: getTodos });
            client.writeQuery({ query: getTodos, data: { todos: [...prev.todos, subscriptionData.data.todoCreated] } });
        },
    });

    useSubscription(todoUpdated);

    return { refetch, ...rest };
};
