import React, { FunctionComponent } from 'react';
import Head from 'next/head';
import { ApolloProvider, ApolloClient, InMemoryCache, NormalizedCacheObject, split, ApolloLink } from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';
import { BatchHttpLink } from '@apollo/client/link/batch-http';
import { setContext } from '@apollo/client/link/context';
import { getMainDefinition } from '@apollo/client/utilities';
import fetch from 'isomorphic-unfetch';
import { NextPage, NextPageContext } from 'next';
import { useJwtToken } from 'lib/contexts/jwtContext';

type JwtType = string | (() => string);

export type WithApolloProps = {
    apolloClient?: ApolloClient<NormalizedCacheObject>;
    apolloState?: NormalizedCacheObject;
    jwt?: string;
};

interface NextPageContextWithApollo extends NextPageContext {
    apolloClient: ApolloClient<NormalizedCacheObject>;
    apolloState: NormalizedCacheObject;
    query: {
        xdm_e: string;
        xdm_c: string;
        cp: string;
        lic: string;
        jwt: string;
    };
    ctx: AppContext;
}

type AppContext = NextPageContextWithApollo;

let globalApolloClient: ApolloClient<NormalizedCacheObject>;

function createWsLink(endpoint: string, jwt: JwtType): ApolloLink {
    const connectionParams = typeof jwt === 'string' ? { jwt } : () => ({ jwt: jwt() });

    return new WebSocketLink({
        uri: `${endpoint?.replace('http', 'ws')}/subscriptions`,
        options: {
            reconnect: true,
            connectionParams,
        },
    });
}

function createIsomorphLink(jwt: JwtType): ApolloLink {
    let endpoint;
    if (typeof window === 'undefined') {
        endpoint = 'http://api';
    } else {
        const { host, protocol } = window.location;
        endpoint = `${protocol}//${host}`;
    }

    const authLink = setContext((_, { headers }) => {
        const token = typeof jwt === 'string' ? jwt : jwt();

        return {
            headers: {
                ...headers,
                Authorization: `JWT ${token}`,
            },
        };
    });

    const httpLink = new BatchHttpLink({
        fetch,
        uri: `${endpoint}/graphql`,
        credentials: 'same-origin',
    });

    const apolloLink = authLink.concat(httpLink);

    if (typeof window === 'undefined') {
        return httpLink;
    }

    return split(
        ({ query }) => {
            const definition = getMainDefinition(query);
            return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
        },
        createWsLink(endpoint, jwt),
        apolloLink,
    );
}

/**
 * Creates and configures the ApolloClient
 */
function createApolloClient(jwt: JwtType, initialState = {}): ApolloClient<NormalizedCacheObject> {
    const ssrMode = typeof window === 'undefined';
    const cache = new InMemoryCache().restore(initialState);
    // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
    return new ApolloClient({
        ssrMode,
        cache,
        link: createIsomorphLink(jwt),
        defaultOptions: {
            watchQuery: {
                fetchPolicy: 'cache-and-network',
            },
        },
    });
}

/**
 * Always creates a new apollo client on the server
 * Creates or reuses apollo client in the browser.
 */
function initApolloClient(jwt: JwtType, initialState = {}) {
    // Make sure to create a new client for every server-side request so that data
    // isn't shared between connections (which would be bad)
    if (typeof window === 'undefined') {
        return createApolloClient(jwt, initialState);
    }
    // Reuse client on the client-side
    if (!globalApolloClient) {
        globalApolloClient = createApolloClient(jwt, initialState);
    }
    return globalApolloClient;
}

/**
 * Creates and provides the apolloContext
 * to a next.js PageTree. Use it by wrapping
 * your PageComponent via HOC pattern.
 */
export default function withApollo<P>(
    PageComponent: NextPage<P>,
    { ssr = true } = {},
): FunctionComponent<WithApolloProps> {
    const WithApollo = ({
        apolloClient: _apolloClient,
        apolloState,
        jwt: _jwtFromProps,
        ...pageProps
    }: WithApolloProps) => {
        const jwt = useJwtToken();

        return (
            <ApolloProvider client={initApolloClient(jwt, apolloState)}>
                <PageComponent {...(pageProps as P)} />
            </ApolloProvider>
        );
    };
    // Set the correct displayName in development
    if (process.env.NODE_ENV !== 'production') {
        const displayName = PageComponent.displayName || PageComponent.name || 'Component';
        if (displayName === 'App') {
            // eslint-disable-next-line no-console
            console.warn('This withApollo HOC only works with PageComponents.');
        }
        WithApollo.displayName = `withApollo(${displayName})`;
    }
    if (ssr || PageComponent.getInitialProps) {
        WithApollo.getInitialProps = async (ctx: NextPageContextWithApollo) => {
            const { AppTree } = ctx;
            const { jwt = '' } = ctx.query;
            // Initialize ApolloClient, add it to the ctx object so
            // we can use it in `PageComponent.getInitialProp`.
            // eslint-disable-next-line no-multi-assign
            const apolloClient = (ctx.apolloClient = initApolloClient(jwt));

            // Run wrapped getInitialProps methods
            let pageProps = { jwt };
            if (PageComponent.getInitialProps) {
                const initialProps = await PageComponent.getInitialProps(ctx);
                pageProps = { ...pageProps, ...initialProps };
            }
            // Only on the server:
            if (typeof window === 'undefined') {
                // When redirecting, the response is finished.
                // No point in continuing to render
                if (ctx.res && ctx.res.writableEnded) {
                    return pageProps;
                }
                // Only if ssr is enabled
                if (ssr) {
                    try {
                        // Run all GraphQL queries
                        const { getDataFromTree } = await import('@apollo/client/react/ssr');
                        await getDataFromTree(
                            <AppTree
                                pageProps={{
                                    ...pageProps,
                                    apolloClient,
                                }}
                            />,
                        );
                    } catch (error) {
                        // Prevent Apollo Client GraphQL errors from crashing SSR.
                        // Handle them in components via the data.error prop:
                        // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
                        // eslint-disable-next-line no-console
                        console.error('Error while running `getDataFromTree`', error);
                    }
                    // getDataFromTree does not call componentWillUnmount
                    // head side effect therefore need to be cleared manually
                    // @ts-ignore - it says prop rewind does not exist, but it does
                    Head.rewind();
                }
            }
            // Extract query data from the Apollo store
            const apolloState = apolloClient.cache.extract();
            return {
                ...pageProps,
                apolloState,
            };
        };
    }
    return WithApollo;
}
