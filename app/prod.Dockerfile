FROM node:14.15.0-alpine
ARG SENTRY_DSN_APP
ARG SENTRY_URL
ARG SENTRY_ORG
ARG SENTRY_PROJECT
ARG SENTRY_AUTH_TOKEN

WORKDIR /home/app

RUN npm install --global pm2

ADD ./package.json ./yarn.lock ./
RUN yarn --frozen-lockfile

ADD . .

RUN yarn build

EXPOSE 3000

CMD pm2-runtime npm -- start
