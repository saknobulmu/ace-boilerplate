import { FunctionComponent } from 'react';
import Page from '@atlaskit/page';
import withApollo from 'lib/graphql/withApollo';
import TodosList from 'components/pages/index/TodosList';
import Navigation from 'components/Navigation';

const Home: FunctionComponent<unknown> = () => (
    <Page navigation={<Navigation />}>
        <TodosList />
    </Page>
);

export default withApollo(Home);
