/* eslint-disable */
// @ts-nocheck
import Document, { Html, Head, Main, NextScript } from 'next/document';
import getConfig from 'next/config';
import * as snippet from '@segment/snippet';

const { publicRuntimeConfig } = getConfig();

export default class MyDocument extends Document {
    renderSnippet() {
        if (publicRuntimeConfig.segmentWriteKey) {
            const opts = {
                apiKey: publicRuntimeConfig.segmentWriteKey,
            };

            if (publicRuntimeConfig.isDev) {
                return <script dangerouslySetInnerHTML={{ __html: snippet.max(opts) }} />;
            } else {
                return <script dangerouslySetInnerHTML={{ __html: snippet.min(opts) }} />;
            }
        }

        return null;
    }

    render() {
        return (
            <Html>
                <Head>{this.renderSnippet()}</Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
