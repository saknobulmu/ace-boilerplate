import { AppProps } from 'next/app';
import Script from 'next/script';
import { FunctionComponent } from 'react';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import { WindowAP } from 'lib/types/window';
import JwtContextProvider from 'lib/contexts/jwtContext';
import { NextPageContext } from 'next';
import AppThemeProvider from 'lib/contexts/themeContext';
import EnvironmentContextProvider from 'lib/contexts/environmentContext';
import FlagProvider from 'lib/contexts/flagContext';
import '@atlaskit/css-reset/dist/bundle.css';

interface NextPageContextWithJwt extends NextPageContext {
    query: {
        jwt: string;
    };
}

declare global {
    interface Window {
        AP: WindowAP;
    }
}

interface AppPropsWithJwt extends AppProps {
    jwt: string;
}

const myCache = createCache({ key: 'app-style-cache' });

const App: FunctionComponent<AppPropsWithJwt> = ({ Component, pageProps, jwt }) => (
    <>
        <Script
            src="https://connect-cdn.atl-paas.net/all.js"
            type="text/javascript"
            data-options="sizeToParent:true;hideFooter:true;margin:false"
            strategy="beforeInteractive"
        />
        <CacheProvider value={myCache}>
            <EnvironmentContextProvider>
                <FlagProvider>
                    <JwtContextProvider initialToken={jwt}>
                        <AppThemeProvider>
                            <Component {...pageProps} />
                        </AppThemeProvider>
                    </JwtContextProvider>
                </FlagProvider>
            </EnvironmentContextProvider>
        </CacheProvider>
    </>
);

// @ts-ignore - Needed because we are expanding the page props
App.getInitialProps = ({ ctx }: { ctx: NextPageContextWithJwt }) => ({
    jwt: ctx.query.jwt,
});

export default App;
